<?php

use Illuminate\Http\Request;

use App\Menu;
use App\Transaction;
use App\TransactionDetail;

Route::get('/', function () {
    return redirect('/login');
})->middleware('auth');

Route::get('/login', 'LoginController@index')->name('login');
Route::post('/login', 'LoginController@proses');

Route::get('/logout', function(){
	\Auth::logout();
	toastr()->success('Logout Success!', 'Success');
    return redirect('/login');
});

Route::prefix('dashboard')->middleware(['auth'])->group(function() {

	Route::get('/', 'DashboardController@index')
		->middleware('permission:view dashboard');

});

Route::prefix('usersetting')->middleware(['auth'])->group(function() {

	Route::get('/', 'UserSettingController@index')
		->middleware('permission:view usersetting');

	Route::post('/update', 'UserSettingController@update')
		->middleware('permission:update usersetting');

});

Route::prefix('setting')->middleware(['auth'])->group(function() {

	Route::get('/', 'SettingController@index')
		->middleware('permission:view setting');

	Route::post('/update', 'SettingController@update')
		->middleware('permission:update setting');

});

Route::prefix('menu')->middleware(['auth'])->group(function() {

	Route::get('/', 'MenuController@index')
		->middleware('permission:view menu');

	Route::get('/add', 'MenuController@add')
		->middleware('permission:add menu');
	Route::post('/store', 'MenuController@store')
		->middleware('permission:add menu');

	Route::get('/{id}', 'MenuController@edit')
		->middleware('permission:update menu');
	Route::post('/update', 'MenuController@update')
		->middleware('permission:update menu');

	Route::get('/delete/{id}', 'MenuController@delete')
		->middleware('permission:delete menu');
	Route::get('/destroye/{id}', 'MenuController@destroye')
		->middleware('permission:delete menu');

});

Route::prefix('grupuser')->middleware(['auth'])->group(function() {

	Route::get('/', 'GrupUserController@index')
		->middleware('permission:view grupuser');

	Route::get('/add', 'GrupUserController@add')
		->middleware('permission:add grupuser');
	Route::post('/store', 'GrupUserController@store')
		->middleware('permission:add grupuser');

	Route::get('/{id}', 'GrupUserController@edit')
		->middleware('permission:update grupuser');
	Route::post('/update', 'GrupUserController@update')
		->middleware('permission:update grupuser');

	Route::get('/delete/{id}', 'GrupUserController@delete')
		->middleware('permission:delete grupuser');
	Route::get('/destroye/{id}', 'GrupUserController@destroye')
		->middleware('permission:delete grupuser');

});

Route::prefix('user')->middleware(['auth'])->group(function() {

	Route::get('/', 'UserController@index')
		->middleware('permission:view user');

	Route::get('/add', 'UserController@add')
		->middleware('permission:add user');
	Route::post('/store', 'UserController@store')
		->middleware('permission:add user');

	Route::get('/{id}', 'UserController@edit')
		->middleware('permission:update user');
	Route::post('/update', 'UserController@update')
		->middleware('permission:update user');

	Route::get('/delete/{id}', 'UserController@delete')
		->middleware('permission:delete user');
	Route::get('/destroye/{id}', 'UserController@destroye')
		->middleware('permission:delete user');

});

Route::prefix('authorization')->middleware(['auth'])->group(function() {

	Route::get('/', 'AuthorizationController@index')
		->middleware('permission:view authorization');

	Route::get('/{id}', 'AuthorizationController@edit')
		->middleware('permission:update authorization');
	Route::post('/update', 'AuthorizationController@update')
		->middleware('permission:update authorization');

});

Route::prefix('page')->middleware(['auth'])->group(function() {

	Route::get('/', 'PageController@index')
		->middleware('permission:view page');

	Route::get('/add', 'PageController@add')
		->middleware('permission:add page');
	Route::post('/store', 'PageController@store')
		->middleware('permission:add page');

	Route::get('/{id}', 'PageController@edit')
		->middleware('permission:update page');
	Route::post('/update', 'PageController@update')
		->middleware('permission:update page');

	Route::get('/delete/{id}', 'PageController@delete')
		->middleware('permission:delete page');
	Route::get('/destroye/{id}', 'PageController@destroye')
		->middleware('permission:delete page');

});

Route::prefix('banner')->middleware(['auth'])->group(function() {

	Route::get('/', 'BannerController@index')
		->middleware('permission:view banner');

	Route::get('/add', 'BannerController@add')
		->middleware('permission:add banner');
	Route::post('/store', 'BannerController@store')
		->middleware('permission:add banner');

	Route::get('/{id}', 'BannerController@edit')
		->middleware('permission:update banner');
	Route::post('/update', 'BannerController@update')
		->middleware('permission:update banner');

	Route::get('/delete/{id}', 'BannerController@delete')
		->middleware('permission:delete banner');
	Route::get('/destroye/{id}', 'BannerController@destroye')
		->middleware('permission:delete banner');

});

Route::prefix('catcontent')->middleware(['auth'])->group(function() {

	Route::get('/', 'CatContentController@index')
		->middleware('permission:view catcontent');

	Route::get('/add', 'CatContentController@add')
		->middleware('permission:add catcontent');
	Route::post('/store', 'CatContentController@store')
		->middleware('permission:add catcontent');

	Route::get('/{id}', 'CatContentController@edit')
		->middleware('permission:update catcontent');
	Route::post('/update', 'CatContentController@update')
		->middleware('permission:update catcontent');

	Route::get('/delete/{id}', 'CatContentController@delete')
		->middleware('permission:delete catcontent');
	Route::get('/destroye/{id}', 'CatContentController@destroye')
		->middleware('permission:delete catcontent');

});

Route::prefix('content')->middleware(['auth'])->group(function() {

	Route::get('/', 'ContentController@index')
		->middleware('permission:view content');

	Route::get('/add', 'ContentController@add')
		->middleware('permission:add content');
	Route::post('/store', 'ContentController@store')
		->middleware('permission:add content');

	Route::get('/{id}', 'ContentController@edit')
		->middleware('permission:update content');
	Route::post('/update', 'ContentController@update')
		->middleware('permission:update content');

	Route::get('/delete/{id}', 'ContentController@delete')
		->middleware('permission:delete content');
	Route::get('/destroye/{id}', 'ContentController@destroye')
		->middleware('permission:delete content');

});

Route::prefix('catgallery')->middleware(['auth'])->group(function() {

	Route::get('/', 'CatGalleryController@index')
		->middleware('permission:view catgallery');

	Route::get('/add', 'CatGalleryController@add')
		->middleware('permission:add catgallery');
	Route::post('/store', 'CatGalleryController@store')
		->middleware('permission:add catgallery');

	Route::get('/{id}', 'CatGalleryController@edit')
		->middleware('permission:update catgallery');
	Route::post('/update', 'CatGalleryController@update')
		->middleware('permission:update catgallery');

	Route::get('/delete/{id}', 'CatGalleryController@delete')
		->middleware('permission:delete catgallery');
	Route::get('/destroye/{id}', 'CatGalleryController@destroye')
		->middleware('permission:delete catgallery');

});

Route::prefix('gallery')->middleware(['auth'])->group(function() {

	Route::get('/', 'GalleryController@index')
		->middleware('permission:view gallery');

	Route::get('/add', 'GalleryController@add')
		->middleware('permission:add gallery');
	Route::post('/store', 'GalleryController@store')
		->middleware('permission:add gallery');

	Route::get('/{id}', 'GalleryController@edit')
		->middleware('permission:update gallery');
	Route::post('/update', 'GalleryController@update')
		->middleware('permission:update gallery');

	Route::get('/delete/{id}', 'GalleryController@delete')
		->middleware('permission:delete gallery');
	Route::get('/destroye/{id}', 'GalleryController@destroye')
		->middleware('permission:delete gallery');

});

Route::prefix('contactus')->middleware(['auth'])->group(function() {

	Route::get('/', 'ContactUsController@index')
		->middleware('permission:view contactus');

	Route::get('/{id}', 'ContactUsController@detail')
		->middleware('permission:view contactus');

	Route::get('/delete/{id}', 'ContactUsController@delete')
		->middleware('permission:delete contactus');
	Route::get('/destroye/{id}', 'ContactUsController@destroye')
		->middleware('permission:delete contactus');

});

Route::prefix('newsletter')->middleware(['auth'])->group(function() {

	Route::get('/', 'NewsLetterController@index')
		->middleware('permission:view newsletter');

	Route::get('/delete/{id}', 'NewsLetterController@delete')
		->middleware('permission:delete newsletter');
	Route::get('/destroye/{id}', 'NewsLetterController@destroye')
		->middleware('permission:delete newsletter');

});

Route::prefix('gudang')->middleware(['auth'])->group(function() {

	Route::get('/', 'GudangController@index')
		->middleware('permission:view gudang');

	Route::get('/add', 'GudangController@add')
		->middleware('permission:add gudang');
	Route::post('/store', 'GudangController@store')
		->middleware('permission:add gudang');

	Route::get('/{id}', 'GudangController@edit')
		->middleware('permission:update gudang');
	Route::post('/update', 'GudangController@update')
		->middleware('permission:update gudang');

	Route::get('/delete/{id}', 'GudangController@delete')
		->middleware('permission:delete gudang');
	Route::get('/destroye/{id}', 'GudangController@destroye')
		->middleware('permission:delete gudang');

});

Route::prefix('supplier')->middleware(['auth'])->group(function() {

	Route::get('/', 'SupplierController@index')
		->middleware('permission:view supplier');

	Route::get('/add', 'SupplierController@add')
		->middleware('permission:add supplier');
	Route::post('/store', 'SupplierController@store')
		->middleware('permission:add supplier');

	Route::get('/{id}', 'SupplierController@edit')
		->middleware('permission:update supplier');
	Route::post('/update', 'SupplierController@update')
		->middleware('permission:update supplier');

	Route::get('/delete/{id}', 'SupplierController@delete')
		->middleware('permission:delete supplier');
	Route::get('/destroye/{id}', 'SupplierController@destroye')
		->middleware('permission:delete supplier');

});

Route::prefix('additionalcost')->middleware(['auth'])->group(function() {

	Route::get('/', 'AdditionalCostController@index')
		->middleware('permission:view additionalcost');

	Route::get('/getAll', 'AdditionalCostController@getAll');

	Route::get('/add', 'AdditionalCostController@add')
		->middleware('permission:add additionalcost');
	Route::post('/store', 'AdditionalCostController@store')
		->middleware('permission:add additionalcost');

	Route::get('/{id}', 'AdditionalCostController@edit')
		->middleware('permission:update additionalcost');
	Route::post('/update', 'AdditionalCostController@update')
		->middleware('permission:update additionalcost');

	Route::get('/delete/{id}', 'AdditionalCostController@delete')
		->middleware('permission:delete additionalcost');
	Route::get('/destroye/{id}', 'AdditionalCostController@destroye')
		->middleware('permission:delete additionalcost');

});

Route::prefix('catproduct')->middleware(['auth'])->group(function() {

	Route::get('/', 'CatProductController@index')
		->middleware('permission:view catproduct');

	Route::get('/getcattegory', 'CatProductController@getcattegory')
		->middleware('permission:view catproduct');

	Route::get('/add', 'CatProductController@add')
		->middleware('permission:add catproduct');
	Route::post('/store', 'CatProductController@store')
		->middleware('permission:add catproduct');

	Route::get('/{id}', 'CatProductController@edit')
		->middleware('permission:update catproduct');
	Route::post('/update', 'CatProductController@update')
		->middleware('permission:update catproduct');

	Route::get('/delete/{id}', 'CatProductController@delete')
		->middleware('permission:delete catproduct');
	Route::get('/destroye/{id}', 'CatProductController@destroye')
		->middleware('permission:delete catproduct');

});

Route::prefix('product')->middleware(['auth'])->group(function() {

	Route::get('/', 'ProductController@index')
		->middleware('permission:view product');

	Route::get('/add', 'ProductController@add')
		->middleware('permission:add product');
	Route::post('/store', 'ProductController@store')
		->middleware('permission:add product');

	Route::get('/{id}', 'ProductController@edit')
		->middleware('permission:update product');
	Route::post('/update', 'ProductController@update')
		->middleware('permission:update product');

	Route::get('/delete/{id}', 'ProductController@delete')
		->middleware('permission:delete product');
	Route::get('/destroye/{id}', 'ProductController@destroye')
		->middleware('permission:delete product');

});

Route::prefix('sales')->middleware(['auth'])->group(function() {

	Route::get('/', 'SalesController@index')
		->middleware('permission:view sales');

	Route::get('/add', 'SalesController@add')
		->middleware('permission:add sales');
	Route::post('/store', 'SalesController@store')
		->middleware('permission:add sales');

	Route::get('/{id}', 'SalesController@edit')
		->middleware('permission:update sales');
	Route::post('/update', 'SalesController@update')
		->middleware('permission:update sales');

	Route::get('/delete/{id}', 'SalesController@delete')
		->middleware('permission:delete sales');
	Route::get('/destroye/{id}', 'SalesController@destroye')
		->middleware('permission:delete sales');

});

Route::prefix('customer')->middleware(['auth'])->group(function() {

	Route::get('/', 'CustomerController@index')
		->middleware('permission:view customer');

	Route::get('/add', 'CustomerController@add')
		->middleware('permission:add customer');
	Route::post('/store', 'CustomerController@store')
		->middleware('permission:add customer');

	Route::get('/{id}', 'CustomerController@edit')
		->middleware('permission:update customer');
	Route::post('/update', 'CustomerController@update')
		->middleware('permission:update customer');

	Route::get('/delete/{id}', 'CustomerController@delete')
		->middleware('permission:delete customer');
	Route::get('/destroye/{id}', 'CustomerController@destroye')
		->middleware('permission:delete customer');

});

Route::prefix('purchase')->middleware(['auth'])->group(function() {

	Route::get('/', 'PurchaseController@index')
		->middleware('permission:view purchase');

	Route::get('/detail/{id}', 'PurchaseController@detail')
		->middleware('permission:view purchase');
	Route::get('/print/{id}', 'PurchaseController@print')
		->middleware('permission:view purchase');
	Route::get('/undo/{id}', 'PurchaseController@undo')
		->middleware('permission:view purchase');

	Route::get('/add', 'PurchaseController@add')
		->middleware('permission:add purchase');
	Route::post('/store', 'PurchaseController@store')
		->middleware('permission:add purchase');

	Route::get('/{id}', 'PurchaseController@edit')
		->middleware('permission:update purchase');
	Route::post('/update', 'PurchaseController@update')
		->middleware('permission:update purchase');

	Route::get('/delete/{id}', 'PurchaseController@delete')
		->middleware('permission:delete purchase');
	Route::get('/destroye/{id}', 'PurchaseController@destroye')
		->middleware('permission:delete purchase');

});

Route::get('/transaction/detail/{id}', function(Request $request, $id){

	$transaction = Transaction::find($id);

	$detail = $transaction->data_detail()
		->whereRaw('receive < qty')
		->with('data_product')
		->get() ?? [];

	return response()->json($detail);
});

Route::prefix('goodsin')->middleware(['auth'])->group(function() {

	Route::get('/', 'GoodsInController@index')
		->middleware('permission:view goodsin');

	Route::get('/detail/{id}', 'GoodsInController@detail')
		->middleware('permission:view goodsin');

	Route::get('/add', 'GoodsInController@add')
		->middleware('permission:add goodsin');
	Route::post('/store', 'GoodsInController@store')
		->middleware('permission:add goodsin');

	Route::get('/{id}', 'GoodsInController@edit')
		->middleware('permission:update goodsin');
	Route::post('/update', 'GoodsInController@update')
		->middleware('permission:update goodsin');

	Route::get('/delete/{id}', 'GoodsInController@delete')
		->middleware('permission:delete goodsin');
	Route::get('/destroye/{id}', 'GoodsInController@destroye')
		->middleware('permission:delete goodsin');
});

Route::prefix('goodsout')->middleware(['auth'])->group(function() {

	Route::get('/', 'GoodsOutController@index')
		->middleware('permission:view goodsout');

	Route::get('/detail/{id}', 'GoodsOutController@detail')
		->middleware('permission:view goodsout');
	Route::get('/print/{id}', 'GoodsOutController@print')
		->middleware('permission:view goodsout');
	Route::get('/undo/{id}', 'GoodsOutController@undo')
		->middleware('permission:view goodsout');

	Route::get('/add', 'GoodsOutController@add')
		->middleware('permission:add goodsout');
	Route::post('/store', 'GoodsOutController@store')
		->middleware('permission:add goodsout');

	Route::get('/{id}', 'GoodsOutController@edit')
		->middleware('permission:update goodsout');
	Route::post('/update', 'GoodsOutController@update')
		->middleware('permission:update goodsout');

	Route::get('/delete/{id}', 'GoodsOutController@delete')
		->middleware('permission:delete goodsout');
	Route::get('/destroye/{id}', 'GoodsOutController@destroye')
		->middleware('permission:delete goodsout');
});

Route::prefix('purchasepayment')->middleware(['auth'])->group(function() {

	Route::get('/', 'PurchasePaymentController@index')
		->middleware('permission:view purchasepayment');

	Route::get('/detail/{id}', 'PurchasePaymentController@detail')
		->middleware('permission:view purchasepayment');

	Route::get('/add', 'PurchasePaymentController@add')
		->middleware('permission:add purchasepayment');
	Route::post('/store', 'PurchasePaymentController@store')
		->middleware('permission:add purchasepayment');

	Route::get('/{id}', 'PurchasePaymentController@edit')
		->middleware('permission:update purchasepayment');
	Route::post('/update', 'PurchasePaymentController@update')
		->middleware('permission:update purchasepayment');

	Route::get('/delete/{id}', 'PurchasePaymentController@delete')
		->middleware('permission:delete purchasepayment');
	Route::get('/destroye/{id}', 'PurchasePaymentController@destroye')
		->middleware('permission:delete purchasepayment');

});

Route::prefix('returnpurchase')->middleware(['auth'])->group(function() {

	Route::get('/', 'ReturnPurchaseController@index')
		->middleware('permission:view returnpurchase');

	Route::get('/detail/{id}', 'ReturnPurchaseController@detail')
		->middleware('permission:view returnpurchase');

	Route::post('/get/goods', 'ReturnPurchaseController@getGoods')
		->middleware('permission:add returnpurchase');

	Route::get('/add', 'ReturnPurchaseController@add')
		->middleware('permission:add returnpurchase');
	Route::post('/store', 'ReturnPurchaseController@store')
		->middleware('permission:add returnpurchase');

	Route::get('/{id}', 'ReturnPurchaseController@edit')
		->middleware('permission:update returnpurchase');
	Route::post('/update', 'ReturnPurchaseController@update')
		->middleware('permission:update returnpurchase');

	Route::get('/delete/{id}', 'ReturnPurchaseController@delete')
		->middleware('permission:delete returnpurchase');
	Route::get('/destroye/{id}', 'ReturnPurchaseController@destroye')
		->middleware('permission:delete returnpurchase');

});

Route::prefix('returselling')->middleware(['auth'])->group(function() {

	Route::get('/', 'ReturSellingController@index')
		->middleware('permission:view returselling');

	Route::get('/detail/{id}', 'ReturSellingController@detail')
		->middleware('permission:view returselling');

	Route::post('/get/goods', 'ReturSellingController@getGoods')
		->middleware('permission:add returselling');

	Route::get('/add', 'ReturSellingController@add')
		->middleware('permission:add returselling');
	Route::post('/store', 'ReturSellingController@store')
		->middleware('permission:add returselling');

	Route::get('/{id}', 'ReturSellingController@edit')
		->middleware('permission:update returselling');
	Route::post('/update', 'ReturSellingController@update')
		->middleware('permission:update returselling');

	Route::get('/delete/{id}', 'ReturSellingController@delete')
		->middleware('permission:delete returselling');
	Route::get('/destroye/{id}', 'ReturSellingController@destroye')
		->middleware('permission:delete returselling');

});

Route::prefix('sellingpayment')->middleware(['auth'])->group(function() {

	Route::get('/', 'SellingPaymentController@index')
		->middleware('permission:view sellingpayment');

	Route::get('/detail/{id}', 'SellingPaymentController@detail')
		->middleware('permission:view sellingpayment');

	Route::get('/add', 'SellingPaymentController@add')
		->middleware('permission:add sellingpayment');
	Route::post('/store', 'SellingPaymentController@store')
		->middleware('permission:add sellingpayment');

	Route::get('/{id}', 'SellingPaymentController@edit')
		->middleware('permission:update sellingpayment');
	Route::post('/update', 'SellingPaymentController@update')
		->middleware('permission:update sellingpayment');

	Route::get('/delete/{id}', 'SellingPaymentController@delete')
		->middleware('permission:delete sellingpayment');
	Route::get('/destroye/{id}', 'SellingPaymentController@destroye')
		->middleware('permission:delete sellingpayment');

});

Route::prefix('bookingorder')->middleware(['auth'])->group(function() {

	Route::get('/', 'BookingOrderController@index')
		->middleware('permission:view bookingorder');

	Route::get('/detail/{id}', 'BookingOrderController@detail')
		->middleware('permission:view bookingorder');

	Route::get('/detail_ajax/{id}', 'BookingOrderController@detail_ajax')
		->middleware('permission:view bookingorder');

	Route::get('/add', 'BookingOrderController@add')
		->middleware('permission:add bookingorder');
	Route::post('/store', 'BookingOrderController@store')
		->middleware('permission:add bookingorder');

	Route::get('/{id}', 'BookingOrderController@edit')
		->middleware('permission:update bookingorder');
	Route::post('/update', 'BookingOrderController@update')
		->middleware('permission:update bookingorder');

	Route::get('/delete/{id}', 'BookingOrderController@delete')
		->middleware('permission:delete bookingorder');
	Route::get('/destroye/{id}', 'BookingOrderController@destroye')
		->middleware('permission:delete bookingorder');

});

Route::prefix('selling')->middleware(['auth'])->group(function() {

	Route::get('/', 'SellingController@index')
		->middleware('permission:view selling');

	Route::get('/detail/{id}', 'SellingController@detail')
		->middleware('permission:view selling');

	Route::get('/history/{id}/{cust_id}', 'SellingController@history');

	Route::get('/add', 'SellingController@add')
		->middleware('permission:add selling');
	Route::post('/store', 'SellingController@store')
		->middleware('permission:add selling');

	Route::get('/{id}', 'SellingController@edit')
		->middleware('permission:update selling');
	Route::post('/update', 'SellingController@update')
		->middleware('permission:update selling');

	Route::get('/delete/{id}', 'SellingController@delete')
		->middleware('permission:delete selling');
	Route::get('/destroye/{id}', 'SellingController@destroye')
		->middleware('permission:delete selling');

});

Route::prefix('receipt')->middleware(['auth'])->group(function() {

	Route::get('/', 'ReceiptController@index')
		->middleware('permission:view receipt');

	Route::get('/detail/{id}', 'ReceiptController@detail')
		->middleware('permission:view receipt');
    Route::get('/print/{id}', 'ReceiptController@print')
        ->middleware('permission:view receipt');
    Route::get('/undo/{id}', 'ReceiptController@undo')
        ->middleware('permission:view receipt');

	Route::get('/add', 'ReceiptController@add')
		->middleware('permission:add receipt');
	Route::post('/store', 'ReceiptController@store')
		->middleware('permission:add receipt');

	Route::get('/{id}', 'ReceiptController@edit')
		->middleware('permission:update receipt');
	Route::post('/update', 'ReceiptController@update')
		->middleware('permission:update receipt');

	Route::get('/delete/{id}', 'ReceiptController@delete')
		->middleware('permission:delete receipt');
	Route::get('/destroye/{id}', 'ReceiptController@destroye')
		->middleware('permission:delete receipt');

});

Route::prefix('bank')->middleware(['auth'])->group(function() {

	Route::get('/', 'BankController@index')
		->middleware('permission:view bank');

	Route::get('/add', 'BankController@add')
		->middleware('permission:add bank');
	Route::post('/store', 'BankController@store')
		->middleware('permission:add bank');

	Route::get('/{id}', 'BankController@edit')
		->middleware('permission:update bank');
	Route::post('/update', 'BankController@update')
		->middleware('permission:update bank');

	Route::get('/delete/{id}', 'BankController@delete')
		->middleware('permission:delete bank');
	Route::get('/destroye/{id}', 'BankController@destroye')
		->middleware('permission:delete bank');

});

Route::prefix('kas')->middleware(['auth'])->group(function() {

	Route::get('/', 'KasController@index')
		->middleware('permission:view kas');

	Route::post('/add/balance', 'KasController@addBalance')
		->middleware('permission:update kas');

	Route::get('/add', 'KasController@add')
		->middleware('permission:add kas');
	Route::post('/store', 'KasController@store')
		->middleware('permission:add kas');

	Route::get('/{id}', 'KasController@edit')
		->middleware('permission:update kas');
	Route::post('/update', 'KasController@update')
		->middleware('permission:update kas');

	Route::get('/delete/{id}', 'KasController@delete')
		->middleware('permission:delete kas');
	Route::get('/destroye/{id}', 'KasController@destroye')
		->middleware('permission:delete kas');

});

Route::prefix('stock')->middleware(['auth'])->group(function() {

	Route::get('/', 'StockController@index')
		->middleware('permission:view stock');

	Route::get('/add', 'StockController@add')
		->middleware('permission:add stock');
	Route::post('/store', 'StockController@store')
		->middleware('permission:add stock');

	Route::get('/{id}', 'StockController@edit')
		->middleware('permission:update stock');
	Route::post('/update', 'StockController@update')
		->middleware('permission:update stock');

	Route::get('/delete/{id}', 'StockController@delete')
		->middleware('permission:delete stock');
	Route::get('/destroye/{id}', 'StockController@destroye')
		->middleware('permission:delete stock');

});

Route::prefix('laphutang')->middleware(['auth'])->group(function() {

	Route::get('/', 'LaporanHutangController@index')
		->middleware('permission:view laphutang');

});

Route::prefix('lappiutang')->middleware(['auth'])->group(function() {

	Route::get('/', 'LaporanPiutangController@index')
		->middleware('permission:view lappiutang');

});

Route::prefix('stockhistory')->middleware(['auth'])->group(function() {

	Route::get('/', 'StockHistoryController@index')
		->middleware('permission:view stockhistory');

});





Route::get('/{links}', function(){
	return view('errors.notfound');
})->middleware('auth');
