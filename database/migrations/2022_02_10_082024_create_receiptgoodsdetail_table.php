<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptgoodsdetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receiptgoodsdetail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("parent_id")->default(0);
            $table->bigInteger("product_id")->default(0);
            $table->double("qty")->default(0);
            $table->bigInteger("stock_id")->default(0);
            $table->timestamps();
            $table->dateTime("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receiptgoodsdetail');
    }
}
