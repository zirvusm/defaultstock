<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptGoodsTable extends Migration
{
    // receipt of goods
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receiptgoods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("transaction_id")->default(0);
            $table->date("date")->nullable();
            $table->string("type", 30)->comment("buy,sell");
            $table->integer("status")->default(0)->comment("0=pending|1=approve|2=reject");
            $table->timestamps();
            $table->dateTime("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goodsin');
    }
}
