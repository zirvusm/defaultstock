<?php

use App\Menu;
use App\Permission;
use App\Role;
use App\RolePermission;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_history', function (Blueprint $table) {
            $table->bigIncrements('id')->length(20);
            $table->bigInteger('gudang_id')->length(20)->nullable();
            $table->bigInteger('product_id')->length(20)->nullable();
            $table->bigInteger('supplier_id')->length(20)->nullable();
            $table->bigInteger('qty')->length(20)->nullable();
            $table->double('hpp')->length(20)->nullable();
            $table->timestamps();
            $table->timestamp("deleted_at")->nullable();
            $table->text('description')->nullable();
        });

        $data = array(
            [
                'name' => 'History',
                'route' => '#',
                'icon' => '<i class="fas fa-history"></i>',
                'parent_id' => '0',
                'is_menu' => '1',
                'order' => '6',
                'status' => '1',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Stock History',
                'route' => 'stockhistory',
                'icon' => '<i class="fas fa-history"></i>',
                'parent_id' => '66',
                'is_menu' => '1',
                'order' => '1',
                'status' => '1',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        );
        foreach ($data as $dataa){
            $data = new Menu;
            $data->parent_id = $dataa['parent_id'];
            $data->name = $dataa['name'];
            $data->route = $dataa['route'];
            $data->icon = $dataa['icon'];
            $data->order = $dataa['order'];
            $data->is_menu = $dataa['is_menu'];
            $data->status = $dataa['status'];
            $data->save();
        }

        $data_permission = array(
            [
                'name' => 'view stockhistory',
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        );

        foreach ($data_permission as $dataa){
            $data_permission = new Permission;
            $data_permission->name = $dataa['name'];
            $data_permission->guard_name = $dataa['guard_name'];
            $data_permission->save();
            $data_role = array(
                [
                    'permission_id' => $data_permission->id,
                    'role_id' => '1',
                ],
                [
                    'permission_id' => $data_permission->id,
                    'role_id' => '2',
                ],
            );

            foreach ($data_role as $dataa){
                $data_role = new RolePermission;
                $data_role->permission_id = $dataa['permission_id'];
                $data_role->role_id = $dataa['role_id'];
                $data_role->save();
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_history');
    }
}
