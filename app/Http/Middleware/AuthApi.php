<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use App\User;
use App\Customer;
use App\Settings;

class AuthApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $setting = Settings::get()->pluck('value', 'name')->toArray();

        if($setting['whitelist'] != '*' && $request->ip() != $setting['whitelist'])
        {
            return response()->json([
                'success' => false,
                'message' => 'Your IP is not whitelist!',
                'data' => [
                    'ip' => $request->ip()
                ]
            ], 400);
        }

        if($setting['secret_key'] != $request->secret_key)
        {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Credential!',
                'data' => []
            ], 400);
        }

        return $next($request);
    }
}
