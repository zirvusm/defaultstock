<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\CatProduct;
use App\Supplier;
use App\Gudang;


class CatProductController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = CatProduct::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('catproduct.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function getcattegory(Request $request)
    {
        $cat_product = CatProduct::select('id', 'code', 'name')
            ->get()->toArray();

        return response()->json($cat_product);
    }

    public function add(Request $request)
    {
        return view('catproduct.add');
    }

    public function store(Request $request)
    {
        $rule = [
            // 'supplier_id' => 'required|exists:supplier,id',
            'code' => 'required|unique:catproduct,code',
            'name' => 'required'
        ];

        // if($request->catproduct_id != 0)
        // {
        //     $rule['catproduct_id'] = 'required|exists:catproduct,id';
        // }

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();
        try {
            $data = new CatProduct;
            // $data->parent_id = $request->catproduct_id;
            // $data->supplier_id = $request->supplier_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = CatProduct::find($id);
        // $data_supplier = Supplier::where('status', 1)->get();

        if($data == null){
            return redirect()->back();
        }
        return view('catproduct.edit')
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rule = [
            // 'supplier_id' => 'required|exists:supplier,id',
            'code' => [
                'required',
                Rule::unique('catproduct', 'code')->ignore($request->id)
            ],
            'name' => 'required'
        ];

        // if($request->catproduct_id != 0)
        // {
        //     $rule['catproduct_id'] = 'required|exists:catproduct,id';
        // }
        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {
            
            $data = CatProduct::find($request->id);
            // $data->parent_id = $request->catproduct_id;
            // $data->supplier_id = $request->supplier_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = CatProduct::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else if($data->data_product()->count() > 0){
            toastr()->error('Data has been used elsewhere!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
