<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Supplier;
use App\Sales;
use App\Customer;
use App\Gudang;
use App\Product;
use App\Receipt;
use App\ReceiptDetail;
use App\Transaction;
use App\TransactionDetail;
use App\BookingOrder;
use App\BookingOrderDetail;
use App\Stock;
use App\Kas;
use App\AdditionalCost;
use App\Cost;
use App\Selling;
use App\SellingDetail;
use App\Settings;


class ReceiptController extends Controller
{

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Receipt::where('id', '>', 0);
        $data = HelperController::filter($request, $data);

        $data = $data->orderBy('date', 'desc')->orderBy('id', 'desc');
        $count = $data->count();
        $data = $data->paginate($take);

        return view('receipt.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function detail(Request $request, $id)
    {
        $data = Receipt::find($id);

        if($data == null){
            return redirect()->back();
        }

        return view('receipt.detail')
            ->with('data', $data);
    }

    public function print(Request $request, $id)
    {
        $data = Receipt::find($id);
        $setting = Settings::where('name', 'allsettings')->first();
        if($data->delivery_at == null){
            $data->delivery_at = now();
            $data->save();
        }
        $setting = json_decode($setting->value, true);

        if($data == null){
            return redirect()->back();
        }

        return view('receipt.print')
            ->with('setting', $setting)
            ->with('data', $data);
    }

    public function undo(Request $request, $id)
    {
        $data = Receipt::find($id);
        DB::beginTransaction();
        try {
            if($data->delivery_at != null){
                $data->delivery_at = null;
                $data->save();
                DB::commit();
                toastr()->success('Data Updated!', 'Success!');
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }
        return redirect('/receipt');
    }

    public function add(Request $request)
    {
        $data_sales = Sales::select('id', 'code', 'name', 'nik', 'address')
            ->where('status', 1)->get();

        $data_transaction = Transaction::orderBy('id', 'DESC')->where('type', 'sell')->where('status', 1)->where('is_retur', 0)->where('receipt', 0)->get();
        $data_receipt = Receipt::orderBy('id', 'DESC')->first();

        return view('receipt.add')
            ->with('data_transaction', $data_transaction)
            ->with('data_receipt', $data_receipt)
            ->with('data_sales', $data_sales);
    }

    public function store(Request $request)
    {
        $rule = [
            'sales_id' => 'required|exists:sales,id',
            'receipt' => 'required|unique:receipt,receipt',
            'date' => 'required',
            'ids.*' => 'required|exists:transaction,id',
            'dates.*' => 'required',
            'due_dates.*' => 'required',
            'grand_total.*' => 'required',
            'total_all' => 'required|numeric|min:0',
        ];

        Validator::validate($request->all(), $rule, [
            'ids.*' => 'Transaction not found!'
        ]);

        DB::beginTransaction();
        try {
            $data = new Receipt;
            $data->receipt = $request->receipt;
            $data->sales_id = $request->sales_id;
            $data->date = $request->date;
            $data->description = $request->description;
            $data->amount = $request->total_all;

            $data->save();

            foreach ($request->ids as $key => $value) {

                $detail = new ReceiptDetail();
                $detail->parent_id = $data->id;
                $detail->transaction_id = $request->ids[$key];
                $detail->save();
            }

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
            if(Auth::user()->can('update receipt')){
                return redirect('/receipt/'.$data->id);
            }else{
                return redirect('/receipt/detail/'.$data->id);
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        } catch (Exception $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Receipt::find($id);

        if($data == null){
            return redirect()->back();
        }
        if($data->status != 0){
            return redirect()->back();
        }

        $data_sales = Sales::select('id', 'code', 'name', 'nik', 'address')
        ->where('status', 1)->get();

        $data_transaction = Transaction::orderBy('id', 'DESC')->where('type', 'sell')->where('status', 1)->where('is_retur', 0)->where('receipt', 0)->get();
        $data_receipt = Receipt::orderBy('id', 'DESC')->first();

        return view('receipt.edit')
            ->with('data', $data)
            ->with('data_sales', $data_sales)
            ->with('data_transaction', $data_transaction)
            ->with('data_receipt', $data_receipt);
    }

    public function update(Request $request)
    {
        $receipt = Receipt::find($request->id);
        if($receipt == null){
            toastr()->error('Data not found!', 'Error!');
            return redirect()->back();
        }
        $success = true;

        $rule = [
            'sales_id' => 'required|exists:sales,id',
            'date' => 'required',
        ];

        DB::beginTransaction();

        try {
            if($receipt->status == 0)
            {
                $receipt->sales_id = $request->sales_id;
                $receipt->date = $request->date;
                $receipt->description = $request->description;
                $receipt->status = $request->status;
                if($request->status != 0)
                {
                    $receipt->audit_at = date('Y-m-d H:i:s');
                    $receipt->audit_by = Auth::user()->name;
                }

                foreach($receipt->data_detail as $key => $detail)
                {
                    $receiptdetail = Transaction::find($detail->transaction_id);
                    $receiptdetail->receipt = 1;
                    $receiptdetail->save();
                }

                if($request->status == 1)
                {
                    if(!Auth::user()->can('approve receipt'))
                    {
                        toastr()->error('You have not permission to Approve!', 'Error!');
                        return redirect()->back();
                    }

                }elseif ($request->status == 2) {
                    if(!Auth::user()->can('reject receipt'))
                    {
                        toastr()->error('You have not permission to Reject!', 'Error!');
                        return redirect()->back();
                    }
                }
                $receipt->save();
                DB::commit();
                toastr()->success('Data Updated!', 'Success!');
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect('/receipt');
    }

    public function destroye(Request $request, $id)
    {
        $data = Receipt::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            if($data->status != 0){
                toastr()->error('Data already Done!', 'Error!');
                return redirect()->back();
            }

            $receipt = Receipt::find($request->id);
            foreach($receipt->data_detail as $key => $detail)
                {
                    $receiptdetail = Transaction::find($detail->transaction_id);
                    $receiptdetail->receipt = 0;
                    $receiptdetail->save();
                }
            $data->data_detail()->delete();
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }



}
