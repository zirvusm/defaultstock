<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Gallery;
use App\CatGallery;


class GalleryController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Gallery::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('gallery.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $cat = CatGallery::get();

        return view('gallery.add')
            ->with('cat', $cat);
    }

    public function store(Request $request)
    {
        Validator::validate($request->all(), [
            'parent_id' => 'required|exists:catgallery,id',
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        DB::beginTransaction();
        try {
            
            $data = new Gallery;
            $data->parent_id = $request->parent_id;
            $data->name = $request->name;
            $data->description = $request->description;
            $data->save();

            $image_name = '/gallery/'.base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();
            \Storage::disk('uploads')->put($image_name, \File::get($request->image));
            $data->image = env('APP_URL').'/uploads'.$image_name;

            $data->image_name = base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();

            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Gallery::find($id);
        $cat = CatGallery::get();

        if($data == null){
            return redirect()->back();
        }

        return view('gallery.edit')
            ->with('data', $data)
            ->with('cat', $cat);
    }

    public function update(Request $request)
    {
        Validator::validate($request->all(), [
            'parent_id' => 'required|exists:catgallery,id',
            'id' => 'required|exists:gallery,id',
            'name' => 'required',
        ]);

        if($request->image != null)
        {
            $rule['image'] = 'required|image|mimes:jpeg,png,jpg,gif,svg';
        }

        DB::beginTransaction();

        try {
            
            $data = Gallery::find($request->id);
            $data->parent_id = $request->parent_id;
            $data->name = $request->name;
            $data->description = $request->description;

            if($request->image != null)
            {
                $temp = '/gallery/'.$data->image_name;
                \Storage::disk('uploads')->delete($temp);

                $image_name = '/gallery/'.base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();
                \Storage::disk('uploads')->put($image_name, \File::get($request->image));

                $data->image = env('APP_URL').'/uploads'.$image_name;
                $data->image_name = base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();
            }

            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Gallery::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            $temp = '/gallery/'.$data->image_name;
            \Storage::disk('uploads')->delete($temp);

            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
