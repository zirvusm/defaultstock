<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Bank;


class BankController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Bank::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('bank.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        return view('bank.add');
    }

    public function store(Request $request)
    {
        $rule = [
            'name' => 'required',
            'account_name' => 'required',
            'account_number' => 'required|unique:bank,account_number',
            'balance' => 'numeric',
            'status' => 'required|in:0,1'
        ];

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();
        try {
            
            $data = new Bank;
            $data->name = $request->name;
            $data->account_name = $request->account_name;
            $data->account_number = $request->account_number;
            $data->balance = $request->balance;
            $data->status = $request->status;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        // return redirect()->back()->withInput();
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Bank::find($id);

        if($data == null){
            return redirect()->back();
        }
        return view('bank.edit')
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rule = [
            'name' => 'required',
            'account_name' => 'required',
            'account_number' => [
                'required',
                Rule::unique('bank', 'account_number')->ignore($request->id)
            ],
            'balance' => 'numeric',
            'status' => 'required|in:0,1',
        ];

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {
            
            $data = Bank::find($request->id);
            $data->name = $request->name;
            $data->account_name = $request->account_name;
            $data->account_number = $request->account_number;
            $data->balance = $request->balance;
            $data->status = $request->status;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Bank::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }elseif($data->data_purchase->count() > 0){
            toastr()->error('Data has been used elsewhere!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
