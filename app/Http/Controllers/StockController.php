<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Stock;
use App\Stock_history;
use App\Supplier;
use App\Gudang;
use App\Product;


class StockController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Stock::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('stock.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $data_gudang = Gudang::select('id', 'code', 'name')
            ->where('status', 1)->get();
        $data_supplier = Supplier::get();

        $data_product = Product::select('id', 'code', 'name', 'unit')->get();

        return view('stock.add')
            ->with('data_gudang', $data_gudang)
            ->with('data_supplier', $data_supplier)
            ->with('data_product', $data_product);
    }

    public function store(Request $request)
    {
        $rule = [
            'supplier_id' => 'required|exists:supplier,id',
            'gudang_id' => 'required|exists:gudang,id',
            'product_id' => 'required|exists:product,id',
            'qty' => 'required|numeric|min:0',
            'hpp' => 'required|numeric|min:0',
        ];

        Validator::validate($request->all(), $rule);

        $cek = Stock::where('gudang_id', $request->gudang_id)
            ->where('product_id', $request->product_id)->get()->count();

        if($cek > 0){
            toastr()->error('Stok sudah ada!', 'Error!');
            return redirect()->back();
        }

        DB::beginTransaction();
        try {
            $data = new Stock;
            $data->gudang_id = $request->gudang_id;
            $data->supplier_id = $request->supplier_id;
            $data->product_id = $request->product_id;
            $data->qty = $request->qty;
            $data->hpp = $request->hpp;
            $data->description = $request->description;
            $data->save();
            
            $data_history = new Stock_history;
            $data_history->gudang_id = $request->gudang_id;
            $data_history->supplier_id = $request->supplier_id;
            $data_history->product_id = $request->product_id;
            $data_history->qty = $request->qty;
            $data_history->hpp = $request->hpp;
            $data_history->description = "Create Stock";
            $data_history->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Stock::find($id);

        if($data == null){
            return redirect()->back();
        }

        $data_gudang = Gudang::select('id', 'code', 'name')
            ->where('status', 1)->get();
            
        $data_supplier = Supplier::get();

        $data_product = Product::select('id', 'code', 'name', 'unit')->get();

        return view('stock.edit')
            ->with('data', $data)
            ->with('data_gudang', $data_gudang)
            ->with('data_supplier', $data_supplier)
            ->with('data_product', $data_product);
    }

    public function update(Request $request)
    {
        $rule = [
            'supplier_id' => 'required|exists:supplier,id',
            'gudang_id' => 'required|exists:gudang,id',
            'product_id' => 'required|exists:product,id',
            'qty' => 'required|numeric|min:0',
            'hpp' => 'required|numeric|min:0',
        ];

        Validator::validate($request->all(), $rule);

        $cek = Stock::where('gudang_id', $request->gudang_id)
            ->where('product_id', $request->product_id)
            ->where('id', '!=', $request->id)
            ->get()->count();

        if($cek > 0){
            toastr()->error('Stok sudah ada!', 'Error!');
            return redirect()->back();
        }

        DB::beginTransaction();

        try {
            
            $data = Stock::find($request->id);
            $data_history = new Stock_history;
            $data_history->description = "Update Stock";
            if($data->supplier_id != $request->supplier_id){
                $data_history->supplier_id = $request->supplier_id;
                $data_history->description .= " || Update supplier_id from ".$data->supplier_id." to ".$request->supplier_id;
            }
            $data->supplier_id = $request->supplier_id;

            if($data->gudang_id != $request->gudang_id){
                $data_history->gudang_id = $request->gudang_id;
                $data_history->description .= " || Update gudang_id from ".$data->gudang_id." to ".$request->gudang_id;
            }
            $data->gudang_id = $request->gudang_id;

            if($data->product_id != $request->product_id){
                $data_history->product_id = $request->product_id;
                $data_history->description .= " || Update product_id from ".$data->product_id." to ".$request->product_id;
            }
            $data->product_id = $request->product_id;

            if($data->qty != $request->qty){
                $data_history->qty = $request->qty;
                $data_history->description .= " || Update qty from ".$data->qty." to ".$request->qty;
            }
            $data->qty = $request->qty;

            if($data->hpp != $request->hpp){
                $data_history->hpp = $request->hpp;
                $data_history->description .= " || Update hpp from ".$data->hpp." to ".$request->hpp;
            }
            $data->hpp = $request->hpp;
            $data->description = $request->description;
            $data->save();

            $data_history->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Stock::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            if($data->qty != 0){
                toastr()->error('Stock Belum Habis!', 'Error!');
                return redirect()->back();
            }
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
