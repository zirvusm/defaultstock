<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Settings;

class DashboardController extends Controller
{
    
    public function index(Request $request)
    {
        $setting = Settings::where('name', 'allsettings')->first();
        $data = json_decode($setting->value, true);


        return view('dashboard.view')
        	->with('data', $data);
    }

}
