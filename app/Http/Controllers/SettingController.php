<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\User;
use \DB;
use \Auth;
use App\Settings;


class SettingController extends Controller
{
    
    public function index(Request $request){

        $setting = Settings::where('name', 'allsettings')->first();
        $data = json_decode($setting->value, true);

        return view('setting.view')
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rules = [
			'full_name' => 'required',
            'short_name' => 'required',
            'email' => 'required',
            'address' => 'required'
        ];

        if($request->logo != null)
        {
            $rule['logo'] = 'required|image|mimes:jpeg,png,jpg,gif,svg';
        }

        if($request->favicon != null)
        {
            $rule['favicon'] = 'required|image|mimes:jpeg,png,jpg,gif,svg,ico';
        }

        $validator = Validator::validate($request->all(), $rules);

        $data = [
            'full_name' => $request->full_name,
            'short_name' => $request->short_name,
            'email' => $request->email,
            'address' => $request->address,
            'hp' => $request->hp,
            'wa' => $request->wa,
            'fb' => $request->fb,
            'ig' => $request->ig,
            'map_link' => $request->map_link,
            'iframe' => $request->iframe
        ];

        $setting = Settings::where('name', 'allsettings')->first();
        $temp = json_decode($setting->value, true);

        if($request->logo != null)
        {
            $logo_name = '/logo.'.$request->logo->getClientOriginalExtension();
            \Storage::disk('uploads')->put($logo_name, \File::get($request->logo));
            $data['logo'] = env('APP_URL').'/uploads'.$logo_name;
        }else{
            $data['logo'] = $temp['logo'];
        }
        if($request->favicon != null)
        {
            $favicon_name = '/favicon.'.$request->favicon->getClientOriginalExtension();
            \Storage::disk('uploads')->put($favicon_name, \File::get($request->favicon));
            $data['favicon'] = env('APP_URL').'/uploads'.$favicon_name;
        }else{
            $data['favicon'] = $temp['favicon'];
        }

        $setting->value = json_encode($data);
        $setting->save();


        toastr()->success('User Updated!', 'Success!');

        return redirect()->back();
    }

}
