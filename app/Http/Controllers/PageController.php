<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Page;


class PageController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Page::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('page.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        return view('page.add');
    }

    public function store(Request $request)
    {
        Validator::validate($request->all(), [
            'prefix' => 'required|unique:page,prefix',
        ]);
        DB::beginTransaction();
        try {
            
            $data = new Page;
            $data->name = $request->name;
            if(env('USE_LANG'))
            {
                $data->lang = $request->lang;
            }
            $data->prefix = $request->prefix;
            $data->content = $request->content;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Page::find($id);
        if($data == null){
            return redirect()->back();
        }

        return view('page.edit')
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        Validator::validate($request->all(), [
        ]);

        DB::beginTransaction();

        try {
            $data = Page::find($request->id);
            if(env('USE_LANG'))
            {
                $data->lang = $request->lang;
            }
            $data->content = $request->content;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Page::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }


}
