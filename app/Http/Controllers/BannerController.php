<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Banner;


class BannerController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Banner::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('banner.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        return view('banner.add');
    }

    public function store(Request $request)
    {
        Validator::validate($request->all(), [
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        DB::beginTransaction();
        try {
            
            $data = new Banner;
            $data->name = $request->name;
            $data->save();

            $image_name = '/banner/'.base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();
            \Storage::disk('uploads')->put($image_name, \File::get($request->image));
            $data->image = env('APP_URL').'/uploads'.$image_name;
            $data->image_name = base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Banner::find($id);
        if($data == null){
            return redirect()->back();
        }
        return view('banner.edit')
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rule = [
            'name' => 'required'
        ];
        if($request->image != null)
        {
            $rule['image'] = 'required|image|mimes:jpeg,png,jpg,gif,svg';
        }
        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {
            
            $data = Banner::find($request->id);
            $data->name = $request->name;
            if($request->image != null)
            {
                $temp = '/banner/'.$data->image_name;
                \Storage::disk('uploads')->delete($temp);

                $image_name = '/banner/'.base64_encode($request->name.$request->id).'.'.$request->image->getClientOriginalExtension();
                \Storage::disk('uploads')->put($image_name, \File::get($request->image));
                $data->image = env('APP_URL').'/uploads'.$image_name;
                $data->image_name = base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();
            }
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Banner::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            $temp = '/banner/'.$data->image_name;
            \Storage::disk('uploads')->delete($temp);

            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
