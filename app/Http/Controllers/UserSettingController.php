<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\User;
use \DB;
use \Auth;


class UserSettingController extends Controller
{
    
    public function index(Request $request){
        return view('usersetting.view');
    }

    public function update(Request $request)
    {
        $rules = [
			'password' => 'required_with:cpassword|same:cpassword',
            'pin' => 'required_with:cpin|same:cpin',
        ];
        $validator = Validator::validate($request->all(), $rules);

        Auth::user()->img = $request->img;
        if($request->password != null)
        {
            Auth::user()->password = bcrypt($request->password);
        }
        if($request->pin != null)
        {
            Auth::user()->pin = md5($request->pin);
        }
        Auth::user()->save();

        toastr()->success('User Updated!', 'Success!');

        return redirect()->back();
    }

}
