<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Transaction;
use App\TransactionDetail;
use App\Supplier;


class LaporanHutangController extends Controller
{
    
    public function index(Request $request)
    {
        $supplier_id = $request->supplier_id ?? 0;
        $jto = $request->jto ?? "";

        $now = date('Y-m-d');

        $data = Transaction::where('type', 'buy')
            ->where('pay_off', 0)
            ->where('status', 1);

        if($supplier_id > 0){
            $data = $data->where('supplier_id', $supplier_id);
        }

        if($jto == 0 && $jto != ""){
            $data = $data->where('due_date', '>=', $now);
        }elseif ($jto == 1 && $jto != "") {
            $data = $data->where('due_date', '<', $now);
        }

        $data = $data->orderBy('supplier_id', 'asc')
            ->orderBy('due_date', 'asc')
            ->get();

        $data_supplier = Supplier::get();

        return view('laphutang.view')
            ->with('data_supplier', $data_supplier)
            ->with('supplier_id', $supplier_id)
            ->with('jto', $jto)
            ->with('data', $data);
    }

}
