<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Gudang;


class GudangController extends Controller
{

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Gudang::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('gudang.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $data_parent = Gudang::where('status', 1)->get();
        $data_gudang = Gudang::select('code')->orderBy('id', 'DESC')->first();

        return view('gudang.add')
            ->with('data_gudang', $data_gudang)
            ->with('data_parent', $data_parent);
    }

    public function store(Request $request)
    {
        $rule = [
            'name' => 'required',
            'code' => 'required|unique:gudang,code',
            'status' => 'required|in:0,1'
        ];

        if($request->parent_id != 0)
        {
            $rule['parent_id'] = 'required|exists:gudang,id';
        }

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();
        try {

            $data = new Gudang;
            $data->parent_id = $request->parent_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->status = $request->status;
            $data->address = $request->address;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        // return redirect()->back()->withInput();
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Gudang::find($id);

        $data_parent = Gudang::where('status', 1)
            ->where('id', '!=', $id)->get();

        if($data == null){
            return redirect()->back();
        }
        return view('gudang.edit')
            ->with('data_parent', $data_parent)
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rule = [
            'name' => 'required',
            'status' => 'required|in:0,1',
            'code' => [
                'required',
                Rule::unique('gudang', 'code')->ignore($request->id)
            ],
        ];

        if($request->parent_id != 0)
        {
            $rule['parent_id'] = 'required|exists:gudang,id';
        }

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {

            $data = Gudang::find($request->id);
            $data->parent_id = $request->parent_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->status = $request->status;
            $data->address = $request->address;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Gudang::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else if($data->data_child()->count() > 0){
            toastr()->error('Data has been used elsewhere!', 'Error!');
            return redirect()->back();
        }else if($data->data_purchase()->count() > 0){
            toastr()->error('Data has been used elsewhere!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
