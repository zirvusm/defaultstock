<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Kas;
use App\KasLog;
use App\Transaction;
use App\TransactionDetail;
use App\Supplier;
use App\Gudang;
use App\Stock;
use App\Customer;
use App\Stock_history;

class ReturSellingController extends Controller
{
    public $gr = "selling";

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Transaction::where('type', 'sell')->where('is_retur', 1);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->orderBy('date', 'DESC')->orderBy('id', 'DESC');
        $data = $data->paginate($take);

        return view('returselling.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $data_customer = Customer::get();
        $data_gudang = Gudang::get();

        $data_old = Transaction::where('is_retur', 1)->orderBy('id', 'DESC')->first();

        return view('returselling.add')
            ->with('gr', $this->gr)
            ->with('data_customer', $data_customer)
            ->with('data_gudang', $data_gudang)
            ->with('data_old', $data_old);
    }

    public function getGoods(Request $request)
    {
        $customer_id = $request->customer_id;
        $gudang_id = $request->gudang_id;

        $data_goodsin = [];
        $data = TransactionDetail::whereHas('data_parent', function($query) use($customer_id, $gudang_id){
            $query->where('customer_id', $customer_id)
                ->where('gudang_id', $gudang_id)
                ->where('type', 'sell')
                ->where('goods_out', 1)
                ->where('status', 1);
        })->get();

        if(isset($request->arr)) $arr = json_decode($request->arr);
        else $arr = [];

        foreach ($data as $key => $value) {
            if(!isset($data_goodsin[$value->product_id])){
                $data_goodsin[$value->product_id] = [
                    "invoice" => $value->data_parent->invoice,
                    "product_id" => $value->product_id,
                    "code" => $value->data_product->code,
                    "name" => $value->data_product->name,
                    "qty" => 0,
                    "amount" => 0,
                    "in_arr" => (in_array($value->product_id, $arr)),
                ];
            }
            if($value->data_parent->is_retur == 0) $data_goodsin[$value->product_id]['qty'] += $value->qty;
            else $data_goodsin[$value->product_id]['qty'] -= $value->qty;

            $data_goodsin[$value->product_id]['amount'] = $value->amount;
        }

        $view = view('tables.goodsin')->with('data_goodsin', $data_goodsin)->render();

        return response()->json([
            "view" => $view
        ]);
    }

    public function store(Request $request)
    {
        $rule = [
            'customer_id' => 'required|exists:customer,id',
            'gudang_id' => 'required|exists:gudang,id',
            'invoice' => 'required|unique:transaction,invoice',
            'date' => 'required',
            'ids.*' => 'required|exists:product,id',
            'qtys.*' => 'required|numeric|min:1',
            'amounts.*' => 'required|numeric|min:0',
            'total_allfix' => 'required|numeric|min:0',
        ];
        Validator::validate($request->all(), $rule);

        DB::beginTransaction();
        try {
            $data = new Transaction;

            $data->customer_id = $request->customer_id;
            $data->gudang_id = $request->gudang_id;
            $data->invoice = $request->invoice;
            $data->date = $request->date;
            $data->amount = $request->total_allfix;
            $data->type = "sell";
            $data->is_retur = 1;
            $data->description = $request->description;
            $data->save();

            $total = 0;

            if($request->ids != null)
            {
                foreach ($request->ids as $key => $value) {
                    $detail = new TransactionDetail;
                    $detail->parent_id = $data->id;
                    $detail->product_id = $value;
                    $detail->qty = $request->qtys[$key];
                    $detail->amount = $request->amounts[$key];
                    $detail->save();

                    $total += $detail->qty * $detail->amount;
                }
            }

            $data->save();
            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Transaction::find($id);
        if($data == null){
            return redirect()->back();
        }
        if($data->status != 0 && (!Auth::user()->can('approve returselling') || !Auth::user()->can('reject returselling'))){
            toastr()->error('You dont have permission!', 'Error!');
            return redirect()->back();
        }

        $data_customer = Customer::get();
        $data_gudang = Gudang::get();

        return view('returselling.edit')
            ->with('gr', $this->gr)
            ->with('data_customer', $data_customer)
            ->with('data_gudang', $data_gudang)
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rule = [
            'id' => 'required|exists:transaction,id',
            'date' => 'required',
            'ids.*' => 'required|exists:product,id',
            'qtys.*' => 'required|numeric|min:1',
            'amounts.*' => 'required|numeric|min:0',
            'total_allfix' => 'required|numeric|min:0',
        ];

        if(Auth::user()->can('approve returselling') || Auth::user()->can('reject returselling'))
        {
            $rule['status'] = 'required|in:0,1,2';
        }

        Validator::validate($request->all(), $rule, [
            'ids.*' => 'product not found!',
            'qtys.*' => 'invalid qty!',
            'amounts.*' => 'invalid amount!',
        ]);

        DB::beginTransaction();
        $data = Transaction::find($request->id);

        if($data->type != "sell" || $data->is_retur != 1){
            toastr()->error("Data not Found!", 'Error!');
            return redirect()->back();
        }

        try {
            $data->date = $request->date;
            $data->type = "sell";
            $data->is_retur = 1;
            $data->description = $request->description;
            $data->amount = $request->total_allfix;
            $data->save();

            $total = 0;
            if(($data->status == 1 && $request->status == 0) || ($data->status == 1 && $request->status == 2)){
                if($request->status == 0 && !Auth::user()->can('approve returselling')){
                    toastr()->error('You dont have permission!', 'Error!');
                    return redirect()->back();
                }
                if($request->status == 2 && !Auth::user()->can('reject returselling')){
                    toastr()->error('You dont have permission!', 'Error!');
                    return redirect()->back();
                }
                foreach ($data->data_detail as $key => $detail) {
                    $stock = Stock::where('gudang_id', $data->gudang_id)
                        ->where('product_id', $detail->product_id)
                        ->first();
                    if($stock == null){
                        toastr()->error('Something went wrong!', 'Error!');
                        return redirect()->back();
                    }
                    $stock->qty -= $detail->qty;
                    $stock->save();
                }
            }
            TransactionDetail::where('parent_id', $data->id)->delete();
            if($request->ids != null)
            {
                foreach ($request->ids as $key => $value) {
                    $detail = new TransactionDetail;
                    $detail->parent_id = $data->id;
                    $detail->product_id = $value;
                    $detail->qty = $request->qtys[$key];
                    $detail->amount = $request->amounts[$key];
                    $detail->save();

                    $total += $detail->qty * $detail->amount;

                    if(($data->status == 0 && $request->status == 1) || ($data->status == 2 && $request->status == 1)){
                        if(!Auth::user()->can('approve returselling')){
                            toastr()->error('You dont have permission!', 'Error!');
                            return redirect()->back();
                        }
                        $stock = Stock::where('gudang_id', $data->gudang_id)
                            ->where('product_id', $detail->product_id)
                            ->first();

                        if($stock == null){
                            toastr()->error('Something went wrong!', 'Error!');
                            return redirect()->back();
                        }

                        $stock->qty += $detail->qty;

                        $data_history = new Stock_history;
                        $data_history->description = "ReturnSelling Stock";
                        $data_history->gudang_id = $data->gudang_id;
                        $data_history->description .= " || ReturnSelling gudang_id ".$data->gudang_id;
                        $data_history->product_id = $detail->product_id;
                        $data_history->description .= " || ReturnSelling product_id ".$detail->product_id;
                        $data_history->supplier_id = $data->supplier_id;
                        $data_history->description .= " || ReturnSelling supplier_id ".$data->supplier_id;
                        $data_history->qty = $detail->qty;
                        $temp = 0;
                        $temp = $stock->qty - $detail->qty;
                        $data_history->description .= " || ReturnSelling qty from ".$stock->qty." to ".$temp;
                        $data_history->save();

                        $stock->save();
                    }
                }
            }

            $data->status = $request->status;
            $data->save();
            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect('/returselling');
    }

    public function destroye(Request $request, $id)
    {
        $data = Transaction::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            DB::beginTransaction();
            try {
                if($data->status == 1)
                {
                    if(!Auth::user()->can('reject returselling')){
                        toastr()->error('You have not permission!', 'Error!');
                        return redirect()->back();
                    }
                    foreach ($data->data_detail as $key => $detail) {
                        $stock = Stock::where('gudang_id', $data->gudang_id)
                            ->where('product_id', $detail->product_id)
                            ->first();
                        if($stock == null){
                            toastr()->error('Something went wrong!', 'Error!');
                            return redirect()->back();
                        }
                        $stock->qty -= $detail->qty;
                        $stock->save();
                    }
                }

                $data->delete();
                DB::commit();
                toastr()->success('Data Deleted!', 'Success!');
            } catch (QueryException $e) {
                toastr()->error($e->getMessage(), 'Error!');
            }

            return redirect()->back();
        }
    }

}
