<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\GrupUser;


class GrupUserController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = GrupUser::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('grupuser.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        return view('grupuser.add');
    }

    public function store(Request $request)
    {
        Validator::validate($request->all(), [
            'name' => 'required|unique:roles,name'
        ]);

        DB::beginTransaction();

        try {
            
            $data = new GrupUser;
            $data->name = $request->name;
            $data->guard_name = 'web';
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = GrupUser::find($id);
        if($data == null){
            return redirect()->back();
        }
        return view('grupuser.edit')
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        Validator::validate($request->all(), [
            'id' => 'required|exists:roles,id',
            'name' => [
                'required',
                Rule::unique('roles')->ignore($request->id, 'id')
            ],
        ]);

        DB::beginTransaction();

        try {
            
            $data = GrupUser::find($request->id);
            $data->name = $request->name;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = GrupUser::find($id);
        
        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
