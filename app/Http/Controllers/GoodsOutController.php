<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Supplier;
use App\Gudang;
use App\Product;
use App\Transaction;
use App\TransactionDetail;
use App\Stock;
use App\AdditionalCost;
use App\Cost;
use App\Kas;
use App\Settings;
use App\ReceiptGoods;
use App\ReceiptGoodsDetail;
use App\Stock_history;

class GoodsOutController extends Controller
{
    public $gr = "selling";

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = ReceiptGoods::where('type', 'sell');

        $data = HelperController::filter($request, $data);

        $data = $data->orderBy('date', 'desc')->orderBy('id', 'desc');

        $count = $data->count();
        $data = $data->paginate($take);

        return view('goodsout.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('gr', $this->gr)
            ->with('data', $data);
    }

    public function detail(Request $request, $id)
    {
        $data = ReceiptGoods::where('type', 'sell')->find($id);

        if($data == null){
            return redirect()->back();
        }

        return view('goodsout.detail')
            ->with('gr', $this->gr)
            ->with('data', $data);
    }

    public function print(Request $request, $id)
    {
        $data = ReceiptGoods::where('type', 'sell')->find($id);
        $setting = Settings::where('name', 'allsettings')->first();
        if($data->data_transaction->delivery_at == null){
            $data->data_transaction->delivery_at = now();
            $data->data_transaction->save();
        }
        $setting = json_decode($setting->value, true);

        if($data == null){
            return redirect()->back();
        }

        return view('goodsout.print')
            ->with('gr', $this->gr)
            ->with('setting', $setting)
            ->with('data', $data->data_transaction);
    }

    public function undo(Request $request, $id)
    {
        $data = Transaction::where('type', 'sell')->find($id);
        DB::beginTransaction();
        try {
            if($data->delivery_at != null){
                $data->delivery_at = null;
                $data->save();
                DB::commit();
                toastr()->success('Data Updated!', 'Success!');
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }
        return redirect('/goodsout');
    }

    public function add(Request $request)
    {
        $transactions = Transaction::where('type', 'sell')
            ->where('status', 1)->where('is_retur', 0)
            ->whereHas('data_detail', function($query){
                $query->whereRaw('receive < qty');
            })
            ->get();

        return view('goodsout.add')
            ->with('gr', $this->gr)
            ->with('transactions', $transactions);
    }

    public function store(Request $request)
    {
        Validator::validate($request->all(), [
            'transaction_id' => 'required|exists:transaction,id'
        ]);

        DB::beginTransaction();

        try {
            $transaction = Transaction::find($request->transaction_id);
            
            $data = new ReceiptGoods;
            $data->transaction_id = $request->transaction_id;
            $data->date = $request->date;
            $data->type = "sell";
            $data->status = 1;
            $data->description = $request->description;
            $data->save();

            foreach($request->ids ?? [] as $key => $value)
            {
                $transaction_detail = $transaction->data_detail()
                    ->where('product_id', $request->ids[$key])
                    ->whereRaw("qty > receive && (qty - receive) >= ?", [$request->qtys[$key]])
                    ->first();

                $detail = new ReceiptGoodsDetail;
                $detail->parent_id = $data->id;
                $detail->stock_id = $request->stockids[$key];
                $detail->product_id = $request->ids[$key];
                $detail->qty = $request->qtys[$key];

                $stock = Stock::find($detail->stock_id);

                if($stock == null)
                {
                    toastr()->error('Something went Wrong!', 'Error!');
                    return redirect()->back();
                }
                $stock->qty -= $detail->qty;


                $transaction_detail->receive += $detail->qty;
                $transaction_detail->save();
                $detail->save();
                $stock->save();
            }

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Transaction::where('type', 'sell')->find($id);
        $data_gudang = Gudang::where('status', 1)->get();

        if($data == null){
            return redirect()->back();
        }
        if($data->goods_out != 0){
            return redirect()->back();
        }

        return view('goodsout.edit')
            ->with('data', $data)
            ->with('data_gudang', $data_gudang)
            ->with('gr', $this->gr);
    }

    public function update(Request $request)
    {
        $data = Transaction::where('type', 'sell')
            ->where('status', 1)
            ->find($request->id);
        if($data == null){
            toastr()->error('Data not found!', 'Error!');
            return redirect()->back();
        }

        $rule = [
            'gudang_id' => 'required|exists:gudang,id',
            'goods_out' => 'required|in:0,1,2',
        ];

        Validator::validate($request->all(), $rule);
        DB::beginTransaction();

        try {
            if($data->goods_out == 0)
            {
                $data->gudang_id = $request->gudang_id;
                $data->goods_out = $request->goods_out;

                if($request->goods_out != 0)
                {
                    $data->audit_at = date('Y-m-d H:i:s');
                    $data->audit_by = Auth::user()->name;
                }

                if($request->goods_out == 1)
                {
                    if(!Auth::user()->can('approve goodsout'))
                    {
                        toastr()->error('You have not permission to Approve!', 'Error!');
                        return redirect()->back();
                    }
                    foreach ($data->data_detail as $key => $detail) {
                        $stock = Stock::where('gudang_id', $data->gudang_id)
                            ->where('product_id', $detail->product_id)
                            ->first();

                        if($stock == null)
                        {
                            toastr()->error('Something went Wrong!', 'Error!');
                            return redirect()->back();
                        }

                        $stock->gudang_id = $data->gudang_id;
                        $stock->product_id = $detail->product_id;
                        $stock->qty -= $detail->qty;
                        $data->supplier_id = $stock->supplier_id;

                        
                        $data_history = new Stock_history;
                        $data_history->description = "GoodOut Stock";
                        $data_history->gudang_id = $data->gudang_id;
                        $data_history->supplier_id = $stock->supplier_id;
                        $data_history->product_id = $detail->product_id;
                        $data_history->qty = $detail->qty;
                        $temp = 0;
                        $temp = $stock->qty - $detail->qty;
                        $data_history->description .= " || Update qty from ".$stock->qty." to ".$temp;
                        $data_history->save();

                        $stock->save();
                    }

                }elseif ($request->goods_out == 2) {
                    if(!Auth::user()->can('reject goodsout'))
                    {
                        toastr()->error('You have not permission to Reject!', 'Error!');
                        return redirect()->back();
                    }
                }

                $data->save();
                DB::commit();
                toastr()->success('Data Updated!', 'Success!');
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect('/goodsout');
    }

    public function destroye(Request $request, $id)
    {
        $data = ReceiptGoods::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }
        DB::beginTransaction();

        try{
            foreach($data->data_detail as $key => $detail)
            {
                $transaction_detail = $data->data_transaction->data_detail()
                    ->where('product_id', $detail->product_id)
                    ->whereRaw("(receive - ?) >= 0", [$detail->qty])
                    ->first();

                $stock = Stock::find($detail->stock_id);

                if($stock == null)
                {
                    toastr()->error('Something went Wrong!', 'Error!');
                    return redirect()->back();
                }
                $stock->qty += $detail->qty;


                $transaction_detail->receive -= $detail->qty;
                $transaction_detail->save();
                $stock->save();

                $detail->delete();
            }
            $data->delete();
            DB::commit();      
            toastr()->success("Success", 'Success!');     
        }catch(QueryException $e){
            toastr()->error($e->getMessage(), 'Error!');
        }
        return redirect()->back();
    }

}
