<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\User;
use \DB;
use \Auth;


class LoginController extends Controller
{
    
    public function index(Request $request){
    	if(Auth::check()) return redirect('/dashboard');
        return view('login')->render();
    }

	public function proses(Request $request)
	{
        $rules = [
            'email' => 'required|exists:users,email',
            'password' => 'required'
        ];
        $validator = Validator::validate($request->all(), $rules);

        if(Auth::attempt(['email' => $request->email,'password' => $request->password])){
            return redirect('/dashboard');
        }else{
        	toastr()->error('Invalid Credential', 'Error!');
            return redirect('/login');
        }

	}


}
