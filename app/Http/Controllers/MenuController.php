<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;


class MenuController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Menu::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('menu.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $parent = Menu::select('id', 'name')
            ->where('status', 1)
            ->get()->pluck('name', 'id')->toArray();

        return view('menu.add')
            ->with('parent', $parent);
    }

    public function store(Request $request)
    {
        Validator::validate($request->all(), [
            'parent_id' => 'required',
            'name' => 'required',
            'route' => [
                'required',
                Rule::unique('menu')->ignore('#', 'route')
            ],
            'icon' => 'required',
            'order' => 'required|numeric|min:0',
            'is_menu' => 'required|in:0,1',
            'status' => 'required|in:0,1'
        ]);
        DB::beginTransaction();
        try {
            
            $data = new Menu;
            $data->parent_id = $request->parent_id;
            $data->name = $request->name;
            $data->route = $request->route;
            $data->icon = $request->icon;
            $data->order = $request->order;
            $data->is_menu = $request->is_menu;
            $data->status = $request->status;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Menu::find($id);
        if($data == null){
            return redirect()->back();
        }
        $parent = Menu::select('id', 'name')
            ->where('status', 1)
            ->get()->pluck('name', 'id')->toArray();

        return view('menu.edit')
            ->with('data', $data)
            ->with('parent', $parent);
    }

    public function update(Request $request)
    {
        Validator::validate($request->all(), [
            'id' => 'required|exists:menu,id',
            'parent_id' => 'required',
            'name' => 'required',
            'icon' => 'required',
            'order' => 'required|numeric|min:0',
            'is_menu' => 'required|in:0,1',
            'status' => 'required|in:0,1'
        ]);

        DB::beginTransaction();

        try {
            
            $data = Menu::find($request->id);
            $data->parent_id = $request->parent_id;
            $data->name = $request->name;
            $data->icon = $request->icon;
            $data->order = $request->order;
            $data->is_menu = $request->is_menu;
            $data->status = $request->status;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Menu::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
