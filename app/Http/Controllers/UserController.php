<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\GrupUser;


class UserController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = User::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('user.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $roles = GrupUser::get()->pluck('name', 'id')->toArray();

        return view('user.add')
            ->with('roles', $roles);
    }

    public function store(Request $request)
    {
        Validator::validate($request->all(), [
            'roles' => 'required|exists:roles,id',
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|same:cpassword',
            'cpassword' => 'required|same:password',
        ]);

        DB::beginTransaction();

        try {
            
            $data = new User;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->img = $request->img;
            $data->password = bcrypt($request->password);
            $data->save();

            $data->roles()->detach();
            $data->syncRoles([$request->roles]);
            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = User::find($id);
        if($data == null){
            return redirect()->back();
        }

        $roles = GrupUser::get()->pluck('name', 'id')->toArray();

        return view('user.edit')
            ->with('roles', $roles)
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        Validator::validate($request->all(), [
            'roles' => 'required|exists:roles,id',
            'name' => 'required',
            'password' => 'required_with:cpassword|same:cpassword'
        ]);

        DB::beginTransaction();

        try {
            
            $data = User::find($request->id);
            $data->name = $request->name;
            $data->email = $request->email;
            $data->img = $request->img;
            if($request->password != null)
            {
                $data->password = bcrypt($request->password);
            }
            $data->save();
            $data->syncRoles([$request->roles]);
            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = User::find($id);
        DB::beginTransaction();
        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            try {
                $data->roles()->detach();
                $data->delete();
                DB::commit();
                toastr()->success('Data Deleted!', 'Success!');
            } catch (QueryException $e) {
                toastr()->error($e->getMessage(), 'Error!');
            }
            
            return redirect()->back();
        }
    }

}
