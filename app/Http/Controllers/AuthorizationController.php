<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\Role;
use App\Permission;
use App\User;
use App\Menu;
use App\GrupUser;


class AuthorizationController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = GrupUser::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('authorization.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function edit(Request $request, $id)
    {
        $data = Role::find($id);
        if($data == null){
            return redirect()->back();
        }
        $menu_action = HelperController::$menu_action;
        $menus = Menu::where('status', 1)->where('route', '!=', '#')->get();

        return view('authorization.edit')
            ->with('menu_action', $menu_action)
            ->with('menus', $menus)
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        Validator::validate($request->all(), [
            'id' => 'required|exists:roles,id',
            'name' => 'required|exists:roles,name',
        ]);

        DB::beginTransaction();

        try {
            $role = Role::findByName($request->name);
            $data = $request->all();
            unset($data['id']);
            unset($data['_token']);

            $sync = [];
            foreach ($data as $per => $value) {
                $per = str_replace('_', ' ', $per);
                $cek = Permission::where('name', $per)->first();

                if($cek == null){
                    $cek = new Permission;
                    $cek->name = $per;
                    $cek->save();
                }
                $sync[] = $cek->id;
            }
            // dd($sync);
            $role->syncPermissions($sync);

            toastr()->success('Data Updated!', 'Success!');
            DB::commit();

        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

}
