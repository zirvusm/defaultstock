<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\CatProduct;
use App\Product;
use App\Supplier;
use App\Gudang;


class ProductController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Product::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('product.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        // $data_supplier = Supplier::where('status', 1)->get();
        $data_category = CatProduct::get();
        // $data_gudang = Gudang::where('status', 1)->get();

        return view('product.add')
            // ->with('data_supplier', $data_supplier)
            ->with('data_category', $data_category);
    }

    public function store(Request $request)
    {
        $rule = [
            // 'supplier_id' => 'required|exists:supplier,id',
            // 'gudang_id' => 'required|exists:gudang,id',
            'category_id' => 'required|exists:catproduct,id',
            'code' => 'required|unique:product,code',
            'name' => 'required',
            'unit' => 'required',
            // 'selling_price' => 'required|numeric|min:0',
        ];

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();
        try {
            $data = new Product;
            // $data->supplier_id = $request->supplier_id;
            // $data->gudang_id = $request->gudang_id;
            $data->category_id = $request->category_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->unit = $request->unit;
            // $data->selling_price = $request->selling_price;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Product::find($id);

        if($data == null){
            return redirect()->back();
        }

        // $data_supplier = Supplier::where('status', 1)->get();
        $data_category = CatProduct::get();
        // $data_gudang = Gudang::where('status', 1)->get();

        return view('product.edit')
            ->with('data', $data)
            // ->with('data_supplier', $data_supplier)
            ->with('data_category', $data_category);
    }

    public function update(Request $request)
    {
        $rule = [
            // 'supplier_id' => 'required|exists:supplier,id',
            // 'gudang_id' => 'required|exists:gudang,id',
            'category_id' => 'required|exists:catproduct,id',
            'code' => [
                'required',
                Rule::unique('product', 'code')->ignore($request->id)
            ],
            'name' => 'required',
            'unit' => 'required',
            // 'selling_price' => 'required|numeric|min:0',
        ];

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {
            
            $data = Product::find($request->id);
            // $data->supplier_id = $request->supplier_id;
            // $data->gudang_id = $request->gudang_id;
            $data->category_id = $request->category_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->unit = $request->unit;
            // $data->selling_price = $request->selling_price;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Product::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }elseif($data->data_stock->count() > 0){
            toastr()->error('Data has been used elsewhere!', 'Error!');
            return redirect()->back();
        }elseif($data->data_purchase_detail->count() > 0){
            toastr()->error('Data has been used elsewhere!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
