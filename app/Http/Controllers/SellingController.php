<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Supplier;
use App\Sales;
use App\Customer;
use App\Gudang;
use App\Product;
use App\Selling;
use App\SellingDetail;
use App\Transaction;
use App\TransactionDetail;
use App\BookingOrder;
use App\BookingOrderDetail;
use App\Stock;
use App\Stock_history;
use App\Kas;
use App\AdditionalCost;
use App\Cost;


class SellingController extends Controller
{
    public $gr = "selling";

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $filter_jt = $request->filter_jt ?? '';

        $data = Transaction::where('type', 'sell')
            ->where('is_retur', 0);
        $data = HelperController::filter($request, $data);

        if($filter_jt == "N") $data = $data->where('due_date', '>', date('Y-m-d'));
        elseif($filter_jt == "Y") $data = $data->where('due_date', '<=', date('Y-m-d'));

        $data = $data->orderBy('date', 'desc')->orderBy('id', 'desc');
        $count = $data->count();
        $data = $data->paginate($take);

        return view('selling.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('filter_jt', $filter_jt)
            ->with('value', $value)
            ->with('count', $count)
            ->with('gr', $this->gr)
            ->with('data', $data);
    }

    public function history(Request $request, $id, $cust_id)
    {
        $data = TransactionDetail::where('stock_id', $id)
            ->whereHas('data_parent', function($query) use ($cust_id){
                // if($cust_id != "undefined") $query->where('customer_id', $cust_id);
            })
            ->first();

        return response()->json($data);
    }

    public function detail(Request $request, $id)
    {
        $data = Transaction::where('type', 'sell')->find($id);

        if($data == null){
            return redirect()->back();
        }

        return view('selling.detail')
            ->with('gr', $this->gr)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $data_customer = Customer::select('id', 'code', 'name', 'nik', 'address', 'sales_id')
            ->where('status', 1)->get();
        $data_sales = Sales::select('id', 'code', 'name', 'nik', 'address')
            ->where('status', 1)->get();

        $data_stock = Stock::get();
        $data_booking = BookingOrder::where('status', 1)
            ->whereDoesntHave('data_selling')->get();
        $data_selling = Transaction::select('invoice')->where('type', 'sell')
            ->where('is_retur', 0)->orderBy('id', 'DESC')->first();

        $data_kas = Kas::where('type', 'kas/bank')->get();

        return view('selling.add')
            ->with('data_customer', $data_customer)
            ->with('data_kas', $data_kas)
            ->with('data_sales', $data_sales)
            ->with('data_stock', $data_stock)
            ->with('data_selling', $data_selling)
            ->with('data_booking', $data_booking);
    }

    public function store(Request $request)
    {
        $rule = [
            'kas_id' => 'required|exists:kas,id',
            'customer_id' => 'required|exists:customer,id',
            'sales_id' => 'required|exists:sales,id',
            'invoice' => 'required|unique:selling,invoice',
            'date' => 'required',
            'ids.*' => 'required|exists:stock,id',
            'qtys.*' => 'required|numeric|min:0',
            'prices.*' => 'required|numeric|min:0',
            'id_costs.*' => 'required|exists:additional_cost,id',
            'amounts.*' => 'required|numeric|min:0',
            'grand_totalfix' => 'required|numeric|min:0',
        ];

        if($request->booking_id != 0)
        {
            $rule['booking_id'] = 'required|exists:booking_order,id';
        }

        Validator::validate($request->all(), $rule, [
            'ids.*' => 'stock not found!',
            'id_costs.*' => 'cost not found!',
        ]);

        DB::beginTransaction();
        try {
            $data = new Transaction;
            $data->kas_id = $request->kas_id;
            $data->customer_id = $request->customer_id;
            $data->sales_id = $request->sales_id;
            $data->booking_id = $request->booking_id;
            $data->amount = $request->grand_totalfix;
            $data->invoice = $request->invoice;
            if($request->booking_invoice != "Tidak Ada"){
                $data->po_invoice = $request->booking_invoice;
                $data->is_po = 1;
            }else{
                $data->po_invoice = $request->booking_invoice;
                $data->is_po = 0;
            }
            $data->date = $request->date;
            $data->due_date = date('Y-m-d H:i:s', strtotime($request->date . ' +90 day'));
            $data->type = "sell";
            $data->description = $request->description;

            $data->save();

            $selling = new Selling;
            $selling->id = $data->id;
            $selling->customer_id = $request->customer_id;
            $selling->sales_id = $request->sales_id;
            $selling->booking_id = $request->booking_id;
            $selling->invoice = $request->invoice;
            $selling->date = $request->date;
            $selling->description = $request->description;

            $selling->save();
            $total = 0;

            foreach ($request->ids as $key => $value) {

                $stock = Stock::find($value);
                $detail = new TransactionDetail;
                $detail->parent_id = $data->id;
                $detail->product_id = $stock->data_product->id;
                $detail->qty = $request->qtys[$key];
                $detail->stock_id = $stock->id;
                $detail->amount = $request->prices[$key];
                $detail->save();

                $sellingdetail = new SellingDetail;
                $sellingdetail->selling_id = $data->id;
                $sellingdetail->product_id = $stock->data_product->id;
                $sellingdetail->qty = $request->qtys[$key];
                $sellingdetail->selling_price = $request->prices[$key];
                $sellingdetail->save();

                $total += $detail->qty * $detail->amount;
            }

            if($request->id_costs != null)
            {
                foreach ($request->id_costs as $key => $value) {
                    $additional_cost = AdditionalCost::find($value);
                    $cost = new Cost;
                    $cost->add_id = $value;
                    $cost->code = $additional_cost->code;
                    $cost->name = $additional_cost->name;
                    $cost->type = $additional_cost->type;
                    $cost->amount = $request->amounts[$key];
                    $cost->gr = $this->gr;
                    $cost->parent_id = $data->id;

                    if($cost->type == 0){
                        $cost->total = $cost->amount;
                    }else{
                        $cost->total = $total * $cost->amount / 100;
                    }

                    $cost->save();
                }
            }

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
            if(Auth::user()->can('update selling')){
                return redirect('/selling/'.$data->id);
            }else{
                return redirect('/selling/detail/'.$data->id);
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        } catch (Exception $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Transaction::where('type', 'sell')->find($id);

        if($data == null){
            return redirect()->back();
        }
        if($data->status != 0){
            return redirect()->back();
        }
        if($data->pay_off == 1){
            toastr()->error('This transaction already done!', 'Error!');
            return redirect()->back();
        }

        $data_kas = Kas::select('id', 'name', 'account_number', 'amount', 'main')
            ->where('type', 'kas/bank')->get();

        $data_stock = Stock::get();

        $data_selling = Selling::orderBy('id', 'DESC')->first();

        return view('selling.edit')
            ->with('data', $data)
            ->with('data_kas', $data_kas)
            ->with('data_stock', $data_stock)
            ->with('data_selling', $data_selling)
            ->with('gr', $this->gr);
    }

    public function update(Request $request)
    {
        $selling = Transaction::where('type', 'sell')->find($request->id);
        $sellingTrue = Selling::find($request->id);
        if($selling == null){
            toastr()->error('Data not found!', 'Error!');
            return redirect()->back();
        }
        if($sellingTrue == null){
            toastr()->error('Data not found!', 'Error!');
            return redirect()->back();
        }
        if($selling->pay_off == 1){
            toastr()->error('Transaction already Paid!', 'Error!');
            return redirect()->back();
        }
        $success = true;

        $rule = [
            'kas_id' => 'required|exists:kas,id',
            'date' => 'required',
            'qtys.*' => 'required|numeric|min:0',
            'prices.*' => 'required|numeric|min:0',
            'id_costs.*' => 'required|exists:additional_cost,id',
            'amounts.*' => 'required|numeric|min:0',
            'grand_totalfix' => 'required|numeric|min:0',
        ];

        if($request->ids != null) $rule['ids.*'] = 'required|exists:product,id';

        if($selling->po_invoice != null){
            $rule['customer_id'] = 'required|exists:customer,id';
            $rule['sales_id'] = 'required|exists:sales,id';
        }

        Validator::validate($request->all(), $rule, [
            'ids.*' => 'stock not found!',
            'id_costs.*' => 'cost not found!',
        ]);

        DB::beginTransaction();

        try {
            if($selling->status == 0)
            {
                if($selling->po_invoice != null){
                    $selling->customer_id = $request->customer_id;
                    $selling->sales_id = $request->sales_id;
                    $sellingTrue->customer_id = $request->customer_id;
                    $sellingTrue->sales_id = $request->sales_id;
                }

                $selling->kas_id = $request->kas_id;
                $selling->date = $request->date;
                $sellingTrue->date = $request->date;
                $selling->amount = $request->grand_totalfix;
                $selling->due_date = date('Y-m-d H:i:s', strtotime($request->date . ' +90 day'));
                $selling->description = $request->description;
                $sellingTrue->description = $request->description;
                $selling->status = $request->status;
                $sellingTrue->status = $request->status;

                if($request->status != 0)
                {
                    $selling->audit_at = date('Y-m-d H:i:s');
                    $selling->audit_by = Auth::user()->name;
                    $sellingTrue->audit_at = date('Y-m-d H:i:s');
                    $sellingTrue->audit_by = Auth::user()->name;
                }
                $total = 0;
                $grand_total = 0;

                TransactionDetail::where('parent_id', $selling->id)->delete();
                SellingDetail::where('selling_id', $sellingTrue->id)->delete();
                if($request->ids != null)
                {
                   foreach ($request->ids as $key => $value) {
                        $stock = Stock::find($value);

                        $selling_detail = new TransactionDetail;
                        $selling_detail->parent_id = $selling->id;
                        $selling_detail->product_id = $value;
                        $selling_detail->stock_id = $stock->id;
                        $selling_detail->qty = $request->qtys[$key];
                        $selling_detail->amount = $request->prices[$key];
                        $selling_detail->save();
                        $sellingTrue_detail = new SellingDetail;
                        $sellingTrue_detail->selling_id = $sellingTrue->id;
                        $sellingTrue_detail->product_id = $value;
                        $sellingTrue_detail->qty = $request->qtys[$key];
                        $sellingTrue_detail->selling_price = $request->prices[$key];
                        $sellingTrue_detail->save();

                        $total += $selling_detail->qty * $selling_detail->amount;
                        $grand_total += $total;
                    }
                }

                Cost::where('parent_id', $selling->id)->where('gr', $this->gr)->delete();
                if($request->id_costs != null)
                {
                    foreach ($request->id_costs as $key => $value) {
                        $additional_cost = AdditionalCost::find($value);

                        $cost = new Cost;
                        $cost->add_id = $value;
                        $cost->code = $additional_cost->code;
                        $cost->name = $additional_cost->name;
                        $cost->type = $additional_cost->type;
                        $cost->amount = $request->amounts[$key];
                        $cost->gr = $this->gr;
                        $cost->parent_id = $selling->id;

                        if($cost->type == 0){
                            $cost->total = $cost->amount;
                        }else{
                            $cost->total = $total * $cost->amount / 100;
                        }
                        $grand_total += $cost->total;

                        $cost->save();
                    }
                }

                if($request->status == 1)
                {
                    if(!Auth::user()->can('approve selling'))
                    {
                        toastr()->error('You have not permission to Approve!', 'Error!');
                        return redirect()->back();
                    }
                    $kas = Kas::where('account_number', '1')->first();
                    $kas->amount += $grand_total;
                    $kas->save();

                }elseif ($request->status == 2) {
                    if(!Auth::user()->can('reject selling'))
                    {
                        toastr()->error('You have not permission to Reject!', 'Error!');
                        return redirect()->back();
                    }
                }

                $selling->save();
                $sellingTrue->save();
                DB::commit();
                toastr()->success('Data Updated!', 'Success!');
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect('/selling');
    }

    public function destroye(Request $request, $id)
    {
        $data = Selling::find($id);
        $transaction = Transaction::find($id);

        if($data == null || $transaction == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            if($data->status != 0){
                toastr()->error('Data already Done!', 'Error!');
                return redirect()->back();
            }
            if($transaction->status != 0){
                toastr()->error('Data already Done!', 'Error!');
                return redirect()->back();
            }
            $transaction->data_detail()->delete();
            $data->data_detail()->delete();
            $transaction->data_cost()->where('gr', $this->gr)->delete();
            $data->delete();
            $transaction->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
