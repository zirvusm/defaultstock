<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\CatGallery;


class CatGalleryController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = CatGallery::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('catgallery.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        return view('catgallery.add');
    }

    public function store(Request $request)
    {
        Validator::validate($request->all(), [
            'name' => 'required|unique:catgallery,name'
        ]);

        DB::beginTransaction();
        try {
            
            $data = new CatGallery;
            $data->name = $request->name;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = CatGallery::find($id);
        if($data == null){
            return redirect()->back();
        }
        return view('catgallery.edit')
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rule = [
            'name' => [
                'required',
                Rule::unique('catgallery', 'name')->ignore($request->id)
            ]
        ];
        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {
            
            $data = CatGallery::find($request->id);
            $data->name = $request->name;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = CatGallery::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
