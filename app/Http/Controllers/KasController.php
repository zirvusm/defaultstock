<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Kas;
use App\KasLog;


class KasController extends Controller
{
    public $types = [
        "kas/bank" => "Kas/Bank",
        "piutang" => "Akun Piutang",
        "hutang" => "Akun Hutang",
    ];

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Kas::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->orderBy('urut');
        $data = $data->paginate($take);

        return view('kas.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        return view('kas.add')
            ->with('types', $this->types);
    }

    public function store(Request $request)
    {
        $rule = [
            'code' => 'required|unique:kas,code',
            'name' => 'required',
            'account_number' => 'required|unique:kas,account_number',
            'type' => 'required|in:'.implode(",", array_keys($this->types)),
            'amount' => 'required|numeric',
        ];

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();
        try {
            $old = Kas::where('type', $request->type)->first();
            $data = new Kas;

            $data->code = $request->code;
            $data->account_number = $request->account_number;
            $data->name = $request->name;
            $data->type = $request->type;
            $data->amount = $request->amount;
            $data->description = $request->description;
            $data->save();

            $log = new KasLog;
            $log->kas_id = $data->id;
            $log->amount = $request->amount;
            $log->description = $request->description;
            $log->before_balance = 0;
            $log->after_balance = $request->amount;
            $log->date = date('Y-m-d');
            $log->status = 1;
            $log->created_by = Auth::user()->name;
            $log->save();

            $data->urut = 1 + $old->urut;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        // return redirect()->back()->withInput();
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Kas::find($id);

        if($data == null){
            return redirect()->back();
        }
        return view('kas.edit')
            ->with('types', $this->types)
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $data = Kas::find($request->id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }

        $rule = [
            'code' => [
                'required',
                Rule::unique('gudang', 'code')->ignore($request->id)
            ],
            'name' => 'required',
            'amount' => 'required|numeric',
        ];

        if($data->main == 0)
        {
            $rule['account_number'] = [
                'required',
                Rule::unique('kas', 'account_number')->ignore($request->id)
            ];
            $rule['type'] = 'required|in:'.implode(",", array_keys($this->types));
        }

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {
            $old = Kas::where('type', $request->type)->first();

            if($data->main == 0)
            {
                $data->account_number = $request->account_number;
                $data->type = $request->type;
            }
            $data->code = $request->code;
            $data->name = $request->name;
            $sebelum = 0;
            $sebelum = $data->amount;
            $data->amount = $request->amount;
            $data->description = $request->description;
            $data->urut = $data->id + $old->urut;
            $data->save();

            $log = new KasLog;
            $log->kas_id = $request->id;
            $log->amount = $request->amount;
            $log->description = $request->description;
            $log->before_balance = $sebelum;
            $log->after_balance = $request->amount;
            $log->date = date('Y-m-d');
            $log->status = 1;
            $log->created_by = Auth::user()->name;
            $log->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function addBalance(Request $request)
    {
        $rule = [
            'id' => 'required|exists:kas,id',
            'amount' => 'required|numeric|min:0',
            'description' => 'required',
        ];
        Validator::validate($request->all(), $rule);

        $kas = Kas::find($request->id);

        if($kas->type != "kas/bank")
        {
            toastr()->error("Data ini tidak bisa di update", 'Error!');
            return redirect()->back();
        }

        if($request->amount <= 0)
        {
            toastr()->error("Jumlah tidak bisa kurang dari 0", 'Error!');
            return redirect()->back();
        }

        DB::beginTransaction();

        try {
            $kas->amount += $request->amount;
            $kas->save();

            $log = new KasLog;
            $log->kas_id = $request->id;
            $log->amount = $request->amount;
            $log->description = $request->description;
            $log->before_balance = $kas->amount - $request->amount;
            $log->after_balance = $kas->amount;
            $log->date = date('Y-m-d');
            $log->status = 1;
            $log->created_by = Auth::user()->name;
            $log->save();

            DB::commit();
            toastr()->success('Saldo telah di tambahkan', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Kas::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }elseif($data->main){
            toastr()->error('Data cant be deleted!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
