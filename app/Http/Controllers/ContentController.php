<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Content;
use App\CatContent;


class ContentController extends Controller
{
    
    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Content::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('content.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $cat = CatContent::get();

        return view('content.add')
            ->with('cat', $cat);
    }

    public function store(Request $request)
    {
        Validator::validate($request->all(), [
            'parent_id' => 'required|exists:catcontent,id',
            'name' => 'required|unique:content,name',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'date' => 'required|date_format:Y-m-d',
            'simple' => 'required',
            'description' => 'required',
        ]);

        DB::beginTransaction();
        try {
            
            $data = new Content;
            $data->parent_id = $request->parent_id;
            $data->name = $request->name;
            if(env('USE_LANG'))
            {
                $data->lang = $request->lang;
            }
            $data->date = $request->date;
            $data->simple = $request->simple;
            $data->description = $request->description;
            $data->save();

            $image_name = '/content/'.base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();
            \Storage::disk('uploads')->put($image_name, \File::get($request->image));
            $data->image = env('APP_URL').'/uploads'.$image_name;
            $data->image_name = base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Content::find($id);
        $cat = CatContent::get();

        if($data == null){
            return redirect()->back();
        }
        return view('content.edit')
            ->with('cat', $cat)
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rule = [
            'parent_id' => 'required|exists:catcontent,id',
            'name' => [
                'required',
                Rule::unique('content', 'name')->ignore($request->id)
            ],
            'date' => 'required|date_format:Y-m-d',
            'simple' => 'required',
            'description' => 'required',
            'status' => 'required',
        ];
        if($request->image != null)
        {
            $rule['image'] = 'required|image|mimes:jpeg,png,jpg,gif,svg';
        }
        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {
            
            $data = Content::find($request->id);
            $data->parent_id = $request->parent_id;
            $data->name = $request->name;
            if(env('USE_LANG'))
            {
                $data->lang = $request->lang;
            }
            $data->date = $request->date;
            $data->simple = $request->simple;
            $data->description = $request->description;
            $data->status = $request->status;
            if($request->image != null)
            {
                $temp = '/content/'.$data->image_name;
                \Storage::disk('uploads')->delete($temp);

                $image_name = '/content/'.base64_encode($request->name.$request->id).'.'.$request->image->getClientOriginalExtension();
                \Storage::disk('uploads')->put($image_name, \File::get($request->image));
                $data->image = env('APP_URL').'/uploads'.$image_name;
                $data->image_name = base64_encode($request->name.$data->id).'.'.$request->image->getClientOriginalExtension();
            }
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Content::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            $temp = '/content/'.$data->image_name;
            \Storage::disk('uploads')->delete($temp);

            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
