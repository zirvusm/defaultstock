<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Supplier;
use App\Gudang;
use App\Product;
use App\Transaction;
use App\TransactionDetail;
use App\Stock;
use App\AdditionalCost;
use App\Cost;
use App\Kas;
use App\Settings;


class PurchaseController extends Controller
{
    public $gr = "purchase";

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $filter_jt = $request->filter_jt ?? '';

        $data = Transaction::where('type', 'buy')
            ->where('is_retur', 0);
        $data = HelperController::filter($request, $data);

        if($filter_jt == "N") $data = $data->where('due_date', '>', date('Y-m-d'));
        elseif($filter_jt == "Y") $data = $data->where('due_date', '<=', date('Y-m-d'));

        $data = $data->orderBy('date', 'desc')->orderBy('id', 'desc');
        $count = $data->count();
        $data = $data->paginate($take);

        return view('purchase.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('filter_jt', $filter_jt)
            ->with('value', $value)
            ->with('count', $count)
            ->with('gr', $this->gr)
            ->with('data', $data);
    }

    public function detail(Request $request, $id)
    {
        $data = Transaction::where('type', 'buy')->find($id);

        if($data == null){
            return redirect()->back();
        }

        return view('purchase.detail')
            ->with('gr', $this->gr)
            ->with('data', $data);
    }

    public function print(Request $request, $id)
    {
        $data = Transaction::where('type', 'buy')->find($id);
        $setting = Settings::where('name', 'allsettings')->first();
        if($data->delivery_at == null){
            $data->delivery_at = now();
            $data->save();
        }
        $setting = json_decode($setting->value, true);
        if($data == null){
            return redirect()->back();
        }

        return view('purchase.print')
            ->with('gr', $this->gr)
            ->with('setting', $setting)
            ->with('data', $data);
    }

    public function undo(Request $request, $id)
    {
        $data = Transaction::where('type', 'buy')->find($id);
        DB::beginTransaction();
        try {
            if($data->delivery_at != null){
                $data->delivery_at = null;
                $data->save();
                DB::commit();
                toastr()->success('Data Updated!', 'Success!');
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }
        return redirect('/purchase');
    }

    public function add(Request $request)
    {
        $data_kas = Kas::select('id', 'code', 'name', 'account_number', 'amount', 'main')
            ->where('type', 'kas/bank')->get();
        $data_supplier = Supplier::select('id', 'code', 'name', 'address')->where('status', 1)->get();
        $data_gudang = Gudang::where('status', 1)->get();
        $data_product = Product::get();
        $data_purchase = Transaction::select('invoice')->where('type', 'buy')
            ->where('is_retur', 0)->orderBy('id', 'DESC')->first();

        return view('purchase.add')
            ->with('data_kas', $data_kas)
            ->with('data_supplier', $data_supplier)
            ->with('data_gudang', $data_gudang)
            ->with('data_product', $data_product)
            ->with('data_purchase', $data_purchase);
    }

    public function store(Request $request)
    {
        $rule = [
            'kas_id' => 'required|exists:kas,id',
            'supplier_id' => 'required|exists:supplier,id',
            'gudang_id' => 'required|exists:gudang,id',
            'invoice' => 'required|unique:transaction,invoice',
            'date' => 'required',
            'ids.*' => 'required|exists:product,id',
            'qtys.*' => 'required|numeric|min:0',
            'prices.*' => 'required|numeric|min:0',
            'id_costs.*' => 'required|exists:additional_cost,id',
            'amounts.*' => 'required|numeric|min:0',
            'grand_totalfix' => 'required|numeric|min:0',
        ];

        Validator::validate($request->all(), $rule, [
            'ids.*' => 'product not found!',
            'id_costs.*' => 'cost not found!',
        ]);

        DB::beginTransaction();
        try {
            $purchase = new Transaction;
            $purchase->kas_id = $request->kas_id;
            $purchase->supplier_id = $request->supplier_id;
            $purchase->gudang_id = $request->gudang_id;
            $purchase->invoice = $request->invoice;
            $purchase->amount = $request->grand_totalfix;
            $purchase->date = $request->date;
            $purchase->due_date = date('Y-m-d H:i:s', strtotime($request->date . ' +90 day'));
            $purchase->type = "buy";
            $purchase->description = $request->description;

            $purchase->save();
            $total = 0;

            foreach ($request->ids as $key => $value) {
                $stock = Stock::where('gudang_id', $purchase->gudang_id)
                    ->where('product_id', $value)
                    ->first();

                $purchase_detail = new TransactionDetail;
                $purchase_detail->parent_id = $purchase->id;
                $purchase_detail->product_id = $value;
                $purchase_detail->stock_id = $stock->id;
                $purchase_detail->qty = $request->qtys[$key];
                $purchase_detail->amount = $request->prices[$key];
                $purchase_detail->save();

                $total += $purchase_detail->qty * $purchase_detail->amount;
            }
            if($request->id_costs != null)
            {
                foreach ($request->id_costs as $key => $value) {
                    $additional_cost = AdditionalCost::find($value);

                    $cost = new Cost;
                    $cost->add_id = $value;
                    $cost->code = $additional_cost->code;
                    $cost->name = $additional_cost->name;
                    $cost->type = $additional_cost->type;
                    $cost->amount = $request->amounts[$key];
                    $cost->gr = $this->gr;
                    $cost->parent_id = $purchase->id;

                    if($cost->type == 0){
                        $cost->total = $cost->amount;
                    }else{
                        $cost->total = $total * $cost->amount / 100;
                    }

                    $cost->save();
                }
            }

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
            if(Auth::user()->can('update purchase')){
                return redirect('/purchase/'.$purchase->id);
            }else{
                return redirect('/purchase/detail/'.$purchase->id);
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Transaction::where('type', 'buy')->find($id);

        if($data == null){
            return redirect()->back();
        }
        if($data->status != 0){
            return redirect()->back();
        }
        if($data->pay_off == 1){
            toastr()->error('This transaction already done!', 'Error!');
            return redirect()->back();
        }

        $data_kas = Kas::select('id', 'code', 'name', 'account_number', 'amount', 'main')
            ->where('type', 'kas/bank')->get();
        $data_supplier = Supplier::select('id', 'code', 'name', 'address')->where('status', 1)->get();
        $data_gudang = Gudang::where('status', 1)->get();
        $data_product = Product::get();

        return view('purchase.edit')
            ->with('data', $data)
            ->with('data_kas', $data_kas)
            ->with('data_supplier', $data_supplier)
            ->with('data_gudang', $data_gudang)
            ->with('gr', $this->gr)
            ->with('data_product', $data_product);
    }

    public function update(Request $request)
    {
        $purchase = Transaction::where('type', 'buy')->find($request->id);
        if($purchase == null){
            toastr()->error('Data not found!', 'Error!');
            return redirect()->back();
        }
        if($purchase->pay_off == 1){
            toastr()->error('Transaction already Paid!', 'Error!');
            return redirect()->back();
        }

        $rule = [
            'kas_id' => 'required|exists:kas,id',
            'supplier_id' => 'required|exists:supplier,id',
            'gudang_id' => 'required|exists:gudang,id',
            'date' => 'required',
            'qtys.*' => 'required|numeric|min:0',
            'prices.*' => 'required|numeric|min:0',
            'id_costs.*' => 'required|exists:additional_cost,id',
            'amounts.*' => 'required|numeric|min:0',
            'grand_totalfix' => 'required|numeric|min:0',
        ];

        if($request->ids != null)
        {
            $rule['ids.*'] = 'required|exists:product,id';
        }

        Validator::validate($request->all(), $rule, [
            'ids.*' => 'product not found!',
            'id_costs.*' => 'cost not found!',
        ]);

        DB::beginTransaction();

        try {
            if($purchase->status == 0)
            {
                $purchase->kas_id = $request->kas_id;
                $purchase->supplier_id = $request->supplier_id;
                $purchase->gudang_id = $request->gudang_id;
                $purchase->amount = $request->grand_totalfix;
                $purchase->date = $request->date;
                $purchase->due_date = date('Y-m-d H:i:s', strtotime($request->date . ' +90 day'));
                $purchase->description = $request->description;
                $purchase->status = $request->status;

                if($request->status != 0)
                {
                    $purchase->audit_at = date('Y-m-d H:i:s');
                    $purchase->audit_by = Auth::user()->name;
                }
                $total = 0;
                $grand_total = 0;

                TransactionDetail::where('parent_id', $purchase->id)->delete();
                if($request->ids != null)
                {
                   foreach ($request->ids as $key => $value) {
                        $stock = Stock::where('gudang_id', $purchase->gudang_id)
                            ->where('product_id', $value)
                            ->first();

                        $purchase_detail = new TransactionDetail;
                        $purchase_detail->parent_id = $purchase->id;
                        $purchase_detail->product_id = $value;
                        $purchase_detail->stock_id = $stock->id;
                        $purchase_detail->qty = $request->qtys[$key];
                        $purchase_detail->amount = $request->prices[$key];
                        $purchase_detail->save();

                        $total += $purchase_detail->qty * $purchase_detail->amount;
                        $grand_total += $total;
                    }
                }

                Cost::where('parent_id', $purchase->id)->where('gr', $this->gr)->delete();
                if($request->id_costs != null)
                {
                    foreach ($request->id_costs as $key => $value) {
                        $additional_cost = AdditionalCost::find($value);

                        $cost = new Cost;
                        $cost->add_id = $value;
                        $cost->code = $additional_cost->code;
                        $cost->name = $additional_cost->name;
                        $cost->type = $additional_cost->type;
                        $cost->amount = $request->amounts[$key];
                        $cost->gr = $this->gr;
                        $cost->parent_id = $purchase->id;

                        if($cost->type == 0){
                            $cost->total = $cost->amount;
                        }else{
                            $cost->total = $total * $cost->amount / 100;
                        }
                        $grand_total += $cost->total;

                        $cost->save();
                    }
                }


                if($request->status == 1)
                {
                    if(!Auth::user()->can('approve purchase'))
                    {
                        toastr()->error('You have not permission to Approve!', 'Error!');
                        return redirect()->back();
                    }

                    $kas = Kas::where('account_number', '2')->first();
                    $kas->amount += $grand_total;
                    $kas->save();

                    // if($request->ids != null)
                    // {
                    //     foreach ($request->ids as $key => $value) {
                    //         $stock = Stock::where('gudang_id', $purchase->gudang_id)
                    //             ->where('product_id', $value)
                    //             ->first();

                    //         if($stock == null)
                    //         {
                    //             $stock = new Stock;
                    //         }

                    //         $stock->gudang_id = $purchase->gudang_id;
                    //         $stock->product_id = $value;
                    //         $stock->qty += $request->qtys[$key];

                    //         $stock->save();
                    //     }
                    // }

                }elseif ($request->status == 2) {
                    if(!Auth::user()->can('reject purchase'))
                    {
                        toastr()->error('You have not permission to Reject!', 'Error!');
                        return redirect()->back();
                    }
                }

                $purchase->save();
                DB::commit();
                toastr()->success('Data Updated!', 'Success!');
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect('/purchase');
    }

    public function destroye(Request $request, $id)
    {
        $data = Transaction::where('type', 'buy')->find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            if($data->status != 0){
                toastr()->error('Data already Done!', 'Error!');
                return redirect()->back();
            }
            Cost::where('parent_id', $id)->where('gr', $this->gr)->delete();
            $data->data_detail()->delete();
            $data->data_cost()->where('gr', $this->gr)->delete();
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
