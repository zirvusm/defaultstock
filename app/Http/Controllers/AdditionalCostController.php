<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\AdditionalCost;


class AdditionalCostController extends Controller
{

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = AdditionalCost::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('additionalcost.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function getAll(Request $request)
    {
        $data = AdditionalCost::select('id', 'code', 'name', 'type', 'amount')
            ->get();

        return response()->json($data);
    }

    public function add(Request $request)
    {
        return view('additionalcost.add');
    }

    public function store(Request $request)
    {
        $rule = [
            'name' => 'required',
            'code' => 'required|unique:additional_cost,code',
            'type' => 'required|in:0,1',
            'amount' => 'required|numeric|min:0'
        ];

        if($request->type == 1)
        {
            $rule['amount'] = 'required|numeric|min:0|max:100';
        }

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();
        try {

            $data = new AdditionalCost;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->type = $request->type;
            $data->amount = $request->amount;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        // return redirect()->back()->withInput();
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = AdditionalCost::find($id);

        if($data == null){
            return redirect()->back();
        }
        return view('additionalcost.edit')
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rule = [
            'name' => 'required',
            'code' => [
                'required',
                Rule::unique('additional_cost', 'code')->ignore($request->id)
            ],
            'type' => 'required|in:0,1',
            'amount' => 'required|numeric|min:0'
        ];
        if($request->type == 1)
        {
            $rule['amount'] = 'required|numeric|min:0|max:100';
        }

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {

            $data = AdditionalCost::find($request->id);
            $data->code = $request->code;
            $data->name = $request->name;
            $data->type = $request->type;
            $data->amount = $request->amount;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = AdditionalCost::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }elseif($data->data_purchase->count() > 0){
            toastr()->error('Data has been used elsewhere!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
