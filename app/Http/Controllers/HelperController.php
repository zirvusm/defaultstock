<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;


class HelperController extends Controller
{
    static $status = [
        0 => 'Non Active',
        1 => 'Active'
    ];

    static $status_customer = [
        0 => '<font color="red">Suspend</font>',
        1 => '<font color="green">Active</font>',
        // 2 => 'Black List'
    ];

    static $status_transfer = [
        0 => 'Pending',
        1 => 'Done',
        2 => 'Reject'
    ];

    static $bool = [
        0 => 'False',
        1 => 'True'
    ];

    static $menu_action = [
        'view' => 'View',
        'add' => 'Add',
        'update' => 'Update',
        'delete' => 'Delete',
        'approve' => 'Approve',
        'reject' => 'Reject',
    ];

    static $digits = [2,3,4];

	static $take = [5,10,50,100,250,500,1000,2000,5000];

	static function filter($request, $builder)
	{
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $cek = explode('.', $request->filter);
        if(count($cek) > 1){
        	$field = array_pop($cek);
        	$string = implode('.', $cek);

        	$builder->whereHas($string, function($query) use($field, $value){
 				$query->where($field, 'LIKE', '%'.$value.'%');
 			});
        }else{
        	$builder->where($filter, 'LIKE', '%'.$value.'%');
        }


		return $builder;
	}

}
