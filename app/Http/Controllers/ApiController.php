<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Settings;
use App\Page;
use App\Banner;


class ApiController extends Controller
{


    public function page(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'prefix' => 'required|exists:page,prefix'
        ]);

        if($validate->fails())
        {
            return response()->json([
                'success' => false,
                'message' => 'Please check your input!',
                'data' => $validate->errors()
            ], 400);
        }

        $success = true;
        $message = 'Success!';
        $http_code = 200;

        $data = Page::where('prefix', $request->prefix)->first();

        return response()->json([
            'success' => $success,
            'message' => $message,
            'data' => $data,
        ], $http_code);
    }


    public function banner(Request $request)
    {
        $validate = Validator::make($request->all(), [
        ]);

        if($validate->fails())
        {
            return response()->json([
                'success' => false,
                'message' => 'Please check your input!',
                'data' => $validate->errors()
            ], 400);
        }

        $success = true;
        $message = 'Success!';
        $http_code = 200;

        $data = Banner::get();

        return response()->json([
            'success' => $success,
            'message' => $message,
            'data' => $data,
        ], $http_code);
    }
}
