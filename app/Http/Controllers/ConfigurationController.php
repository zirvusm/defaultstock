<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\User;
use \DB;
use \Auth;


class ConfigurationController extends Controller
{
    
    public function index(Request $request){
        return view('configuration.view');
    }

    public function update(Request $request)
    {
        $rules = [
			'pin' => 'required_with:cpin|same:cpin'
        ];
        $validator = Validator::validate($request->all(), $rules);
        

    }

}
