<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Customer;
use App\Sales;


class CustomerController extends Controller
{

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = Customer::where('id', '>', 0);
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->paginate($take);

        return view('customer.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $data_sales = Sales::get();
        $data_customer = Customer::select('code')->orderBy('id', 'DESC')->first();
        return view('customer.add')
            ->with('data_customer', $data_customer)
            ->with('data_sales', $data_sales);
    }

    public function store(Request $request)
    {
        $rule = [
            'code' => 'required|unique:customer,code',
            'name' => 'required',
            'status' => 'required|in:0,1'
        ];

        if($request->sales_id != 0)
        {
            $rule['sales_id'] = 'required|exists:sales,id';
        }

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();
        try {

            $data = new Customer;
            $data->sales_id = $request->sales_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->nik = $request->nik;
            $data->bdate = $request->bdate;
            $data->phone_number = $request->phone_number;
            $data->email = $request->email;
            $data->address = $request->address;
            $data->status = $request->status;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = Customer::find($id);

        if($data == null){
            return redirect()->back();
        }

        $data_sales = Sales::get();

        return view('customer.edit')
            ->with('data_sales', $data_sales)
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $rule = [
            'code' => [
                'required',
                Rule::unique('customer', 'code')->ignore($request->id)
            ],
            'name' => 'required',
            'status' => 'required|in:0,1'
        ];

        if($request->sales_id != 0)
        {
            $rule['sales_id'] = 'required|exists:sales,id';
        }

        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        try {

            $data = Customer::find($request->id);
            $data->sales_id = $request->sales_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->nik = $request->nik;
            $data->bdate = $request->bdate;
            $data->phone_number = $request->phone_number;
            $data->email = $request->email;
            $data->address = $request->address;
            $data->status = $request->status;
            $data->description = $request->description;
            $data->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = Customer::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
