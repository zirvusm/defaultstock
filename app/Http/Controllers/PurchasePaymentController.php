<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Kas;
use App\KasLog;
use App\Transaction;
use App\TransactionDetail;


class PurchasePaymentController extends Controller
{
    public $gr = "purchase";

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = KasLog::where('type', 'buy');
        $data = HelperController::filter($request, $data);
        $count = $data->count();
        $data = $data->orderBy('date', 'DESC')->orderBy('id', 'DESC');
        $data = $data->paginate($take);

        return view('purchasepayment.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('data', $data);
    }

    public function add(Request $request)
    {
        $data_purchase = Transaction::where('type', 'buy')
            ->where('status', 1)->where('is_retur', 0)->where('pay_off', 0)->get();

        return view('purchasepayment.add')
            ->with('gr', $this->gr)
            ->with('data_purchase', $data_purchase);
    }

    public function store(Request $request)
    {
        $rule = [
            'purchase_id' => 'required|exists:transaction,id',
            'date' => 'required',
            'amount' => 'required|numeric|min:0',
        ];

        Validator::validate($request->all(), $rule);

        $purchase = Transaction::find($request->purchase_id);
        // $kas = Kas::find($purchase->kas_id);
        // $hutang = Kas::where('account_number', 2)->first();

        $total = $purchase->data_detail()->sum(\DB::raw('qty * amount'));
        $total_cost = $purchase->data_cost->where('gr', $this->gr)->sum('total');
        $grand_total = $total + $total_cost;
        $total_paid = $purchase->data_payment()->where('status', 1)->sum('amount');
        $total_unpaid = $grand_total - $total_paid;

        if($request->amount > $total_unpaid){
            toastr()->error("Telah melebihi jumlah yang harus di bayar!", 'Error!');
            return redirect()->back();
        }

        DB::beginTransaction();
        try {
            $data = new KasLog;
            
            $data->kas_id = $purchase->kas_id;
            $data->transaction_id = $purchase->id;
            $data->type = "buy";
            $data->amount = $request->amount;
            $data->before_balance = 0;
            $data->after_balance = 0;
            $data->date = $request->date;
            $data->description = $request->description;
            $data->created_by = Auth::user()->name;

            // $kas->amount -= $request->amount;
            // $hutang->amount -= $request->amount;

            // if($kas->amount < 0){
            //     toastr()->error("Jumlah kas tidak mencukupi!", 'Error!');
            //     return redirect()->back();
            // }

            // if($total_unpaid - $request->amount == 0){
            //     $purchase->pay_off = 1;
            //     $purchase->save();
            // }else{
            //     $purchase->pay_off = 0;
            //     $purchase->save();
            // }

            $data->save();
            // $kas->save();
            // $hutang->save();
            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = KasLog::find($id);
        if($data == null){
            return redirect()->back();
        }

        $purchase = Transaction::find($data->transaction_id);
        if($purchase == null){
            return redirect()->back();
        }

        $total = $purchase->data_detail()->sum(\DB::raw('qty * amount'));
        $total_cost = $purchase->data_cost->where('gr', $this->gr)->sum('total');
        $grand_total = $total + $total_cost;

        $amount = $data->amount;
        if($data->status == 0) $amount = 0;

        $total_paid = $purchase->data_payment()->where('status', 1)->sum('amount') - $amount;
        $total_unpaid = $grand_total - $total_paid;

        return view('purchasepayment.edit')
            ->with('gr', $this->gr)
            ->with('total', $total)
            ->with('total_cost', $total_cost)
            ->with('grand_total', $grand_total)
            ->with('total_paid', $total_paid)
            ->with('total_unpaid', $total_unpaid)
            ->with('data', $data);
    }

    public function update(Request $request)
    {
        $data = KasLog::find($request->id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }

        $rule = [
            'date' => 'required',
            'amount' => 'required|numeric|min:0',
        ];
        if(Auth::user()->can('approve purchasepayment') || Auth::user()->can('reject purchasepayment'))
        {
            $rule['status'] = 'required|in:0,1';
        }
        Validator::validate($request->all(), $rule);

        DB::beginTransaction();

        $purchase = Transaction::find($data->transaction_id);
        $kas = Kas::find($purchase->kas_id);
        $hutang = Kas::where('account_number', 2)->first();

        $total = $purchase->data_detail()->sum(\DB::raw('qty * amount'));
        $total_cost = $purchase->data_cost->where('gr', $this->gr)->sum('total');
        $grand_total = $total + $total_cost;

        $total_paid = $purchase->data_payment()->where('status', 1)->where('id', '!=', $data->id)->sum('amount');
        $total_unpaid = $grand_total - $total_paid;

        if($request->amount > $total_unpaid){
            toastr()->error("Telah melebihi jumlah yang harus di bayar!", 'Error!');
            return redirect()->back();
        }

        try {
            $data->date = $request->date;
            $data->description = $request->description;
            $data->updated_by = Auth::user()->name;

            if($data->status == 0 && $request->status == 1){
                if(!Auth::user()->can('approve purchasepayment')){
                    toastr()->error('You have not permission!', 'Error!');
                    return redirect()->back();
                }
                $data->status = $request->status;
                $data->audit_by = Auth::user()->name;
                $data->audit_at = date('Y-m-d H:i:s');

                $kas->amount -= $request->amount;
                $hutang->amount -= $request->amount;
                if($kas->amount < 0){
                    toastr()->error("Jumlah kas tidak mencukupi!", 'Error!');
                    return redirect()->back();
                }
                if($total_unpaid - $request->amount == 0){
                    $purchase->pay_off = 1;
                    $purchase->save();
                }else{
                    $purchase->pay_off = 0;
                    $purchase->save();
                }
            }elseif($data->status == 1 && $request->status == 0){
                if(!Auth::user()->can('reject purchasepayment')){
                    toastr()->error('You have not permission!', 'Error!');
                    return redirect()->back();
                }
                $data->status = $request->status;
                $data->audit_by = Auth::user()->name;
                $data->audit_at = date('Y-m-d H:i:s');

                $kas->amount += $data->amount;
                $hutang->amount += $data->amount;

                if($total_unpaid == 0){
                    $purchase->pay_off = 1;
                    $purchase->save();
                }else{
                    $purchase->pay_off = 0;
                    $purchase->save();
                }
            }elseif($data->status == 1 && $request->status == 1){
                if(!Auth::user()->can('approve purchasepayment')){
                    toastr()->error('You have not permission!', 'Error!');
                    return redirect()->back();
                }
                $data->status = $request->status;
                $data->audit_by = Auth::user()->name;
                $data->audit_at = date('Y-m-d H:i:s');

                $kas->amount += $data->amount;
                $hutang->amount += $data->amount;

                $kas->amount -= $request->amount;
                $hutang->amount -= $request->amount;
                if($kas->amount < 0){
                    toastr()->error("Jumlah kas tidak mencukupi!", 'Error!');
                    return redirect()->back();
                }
                if($total_unpaid - $request->amount == 0){
                    $purchase->pay_off = 1;
                    $purchase->save();
                }else{
                    $purchase->pay_off = 0;
                    $purchase->save();
                }
            }
            $data->amount = $request->amount;

            $data->save();
            $kas->save();
            $hutang->save();
            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect('/purchasepayment');
    }

    public function addBalance(Request $request)
    {
        $rule = [
            'id' => 'required|exists:kas,id',
            'amount' => 'required|numeric|min:0',
        ];
        Validator::validate($request->all(), $rule);

        $kas = Kas::find($request->id);

        if($kas->type != "kas/bank")
        {
            toastr()->error("Data ini tidak bisa di update", 'Error!');
            return redirect()->back();   
        }

        if($request->amount <= 0)
        {
            toastr()->error("Jumlah tidak bisa kurang dari 0", 'Error!');
            return redirect()->back();
        }

        DB::beginTransaction();

        try {
            $kas->amount += $request->amount;
            $kas->save();

            $log = new KasLog;
            $log->kas_id = $request->id;
            $log->amount = $request->amount;
            $log->before_balance = $kas->amount - $request->amount;
            $log->after_balance = $kas->amount;
            $log->datetime = date('Y-m-d H:i:s');
            $log->created_by = Auth::user()->name;
            $log->save();

            DB::commit();
            toastr()->success('Saldo telah di tambahkan', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function destroye(Request $request, $id)
    {
        $data = KasLog::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            DB::beginTransaction();
            try {
                if($data->status == 1)
                {
                    if(!Auth::user()->can('reject purchasepayment')){
                        toastr()->error('You have not permission!', 'Error!');
                        return redirect()->back();
                    }
                    
                    $purchase = Transaction::find($data->transaction_id);

                    $kas = Kas::find($purchase->kas_id);
                    $hutang = Kas::where('account_number', 2)->first();

                    $total = $purchase->data_detail()->sum(\DB::raw('qty * amount'));
                    $total_cost = $purchase->data_cost->where('gr', $this->gr)->sum('total');
                    $grand_total = $total + $total_cost;

                    $total_paid = $purchase->data_payment->sum('amount') - $data->amount;
                    $total_unpaid = $grand_total - $total_paid;

                    $kas->amount += $data->amount;
                    $hutang->amount += $data->amount;

                    if($total_unpaid > 0){
                        $purchase->pay_off = 0;
                        $purchase->save();
                    }else{
                        $purchase->pay_off = 1;
                        $purchase->save();
                    }
                    $kas->save();
                    $hutang->save();
                }
                    
                $data->delete();
                DB::commit();
                toastr()->success('Data Deleted!', 'Success!');
            } catch (QueryException $e) {
                toastr()->error($e->getMessage(), 'Error!');
            }
                
            return redirect()->back();
        }
    }

}
