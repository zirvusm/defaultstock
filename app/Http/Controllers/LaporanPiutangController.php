<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Transaction;
use App\TransactionDetail;
use App\Customer;
use App\Sales;


class LaporanPiutangController extends Controller
{
    
    public function index(Request $request)
    {
        $customer_id = $request->customer_id ?? 0;
        $sales_id = $request->sales_id ?? 0;
        $jto = $request->jto ?? "";

        $now = date('Y-m-d');

        $data = Transaction::where('type', 'sell')
            ->where('pay_off', 0)
            ->where('status', 1);

        if($customer_id > 0){
            $data = $data->where('customer_id', $customer_id);
        }
        if($sales_id > 0){
            $data = $data->where('sales_id', $sales_id);
        }

        if($jto == 0 && $jto != ""){
            $data = $data->where('due_date', '>=', $now);
        }elseif ($jto == 1 && $jto != "") {
            $data = $data->where('due_date', '<', $now);
        }

        $data = $data->orderBy('customer_id', 'asc')
            ->orderBy('due_date', 'asc')
            ->get();

        $data_customer = Customer::get();
        $data_sales = Sales::get();

        return view('lappiutang.view')
            ->with('data_customer', $data_customer)
            ->with('customer_id', $customer_id)
            ->with('data_sales', $data_sales)
            ->with('sales_id', $sales_id)
            ->with('jto', $jto)
            ->with('data', $data);
    }

}
