<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use \DB;
use \Auth;
use App\User;
use App\Menu;
use App\Supplier;
use App\Sales;
use App\Customer;
use App\Gudang;
use App\Product;
use App\BookingOrder;
use App\BookingOrderDetail;
use App\Stock;
use App\AdditionalCost;
use App\Cost;


class BookingOrderController extends Controller
{
    public $gr = "bookingorder";

    public function index(Request $request)
    {
        Validator::validate($request->all(), [
            'page' => 'numeric',
            'take' => 'numeric|in:'.implode(',', HelperController::$take)
        ]);

        $page = $request->page ?? 1;
        $take = $request->take ?? 10;
        $filter = $request->filter ?? 'id';
        $value = $request->value ?? '';

        $data = BookingOrder::where('id', '>', 0);

        $data = HelperController::filter($request, $data);

        $data = $data->orderBy('date', 'desc')->orderBy('id', 'desc');

        $count = $data->count();
        $data = $data->paginate($take);

        return view('bookingorder.view')
            ->with('page', $page)
            ->with('take', $take)
            ->with('filter', $filter)
            ->with('value', $value)
            ->with('count', $count)
            ->with('gr', $this->gr)
            ->with('data', $data);
    }

    public function detail(Request $request, $id)
    {
        $data = BookingOrder::find($id);

        if($data == null){
            return redirect()->back();
        }

        return view('bookingorder.detail')
            ->with('gr', $this->gr)
            ->with('data', $data);
    }

    public function detail_ajax(Request $request, $id)
    {
        $data = BookingOrder::find($id);

        if($data != null){
            $view = view('bookingorder.detail_ajax')
                ->with('data', $data)->render();

            $view_cost = view('bookingorder.detail_ajax_cost')
                ->with('data', $data)->with('gr', $this->gr)->render();
        }else{
            $view = "";
        }
        return response()->json([
            'view' => $view,
            'view_cost' => $view_cost,
            'description' => $data->description ?? ""
        ]);
    }

    public function add(Request $request)
    {
        $data_customer = Customer::select('id', 'code', 'name', 'nik', 'address', 'sales_id')
            ->where('status', 1)->get();
        $data_sales = Sales::select('id', 'code', 'name', 'nik', 'address')
            ->where('status', 1)->get();

        $data_stock = Stock::get();

        $data_bookingorder = BookingOrder::orderBy('id', 'DESC')->first();

        return view('bookingorder.add')
            ->with('data_customer', $data_customer)
            ->with('data_sales', $data_sales)
            ->with('data_stock', $data_stock)
            ->with('data_bookingorder', $data_bookingorder);
    }

    public function store(Request $request)
    {
        $rule = [
            'customer_id' => 'required|exists:customer,id',
            'sales_id' => 'required|exists:sales,id',
            'invoice' => 'required|unique:booking_order,invoice',
            'date' => 'required',
            'ids.*' => 'required|exists:stock,id',
            'qtys.*' => 'required|numeric|min:0',
            'prices.*' => 'required|numeric|min:0',
            'id_costs.*' => 'required|exists:additional_cost,id',
            'amounts.*' => 'required|numeric|min:0',
        ];

        Validator::validate($request->all(), $rule, [
            'ids.*' => 'stock not found!',
            'id_costs.*' => 'cost not found!',
        ]);

        DB::beginTransaction();
        try {
            $bookingorder = new BookingOrder;
            $bookingorder->customer_id = $request->customer_id;
            $bookingorder->sales_id = $request->sales_id;
            $bookingorder->invoice = $request->invoice;
            $bookingorder->date = $request->date;
            $bookingorder->description = $request->description;

            $bookingorder->save();
            $total = 0;

            foreach ($request->ids as $key => $value) {

                $stock = Stock::find($value);

                $bookingorder_detail = new BookingOrderDetail;
                $bookingorder_detail->stock_id = $value;
                $bookingorder_detail->booking_id = $bookingorder->id;
                $bookingorder_detail->product_id = $stock->data_product->id;
                $bookingorder_detail->qty = $request->qtys[$key];
                $bookingorder_detail->selling_price = $request->prices[$key];
                $bookingorder_detail->save();

                $total += $bookingorder_detail->qty * $bookingorder_detail->selling_price;
            }
            if($request->id_costs != null)
            {
                foreach ($request->id_costs as $key => $value) {
                    $additional_cost = AdditionalCost::find($value);

                    $cost = new Cost;
                    $cost->add_id = $value;
                    $cost->code = $additional_cost->code;
                    $cost->name = $additional_cost->name;
                    $cost->type = $additional_cost->type;
                    $cost->amount = $request->amounts[$key];
                    $cost->gr = $this->gr;
                    $cost->parent_id = $bookingorder->id;

                    if($cost->type == 0){
                        $cost->total = $cost->amount;
                    }else{
                        $cost->total = $total * $cost->amount / 100;
                    }

                    $cost->save();
                }
            }

            DB::commit();
            toastr()->success('Success add new record!', 'Success!');
            if(Auth::user()->can('update bookingorder')){
                return redirect('/bookingorder/'.$bookingorder->id);
            }else{
                return redirect('/bookingorder/detail/'.$bookingorder->id);
            }
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data = BookingOrder::find($id);

        if($data == null){
            return redirect()->back();
        }

        if($data->status != 0){
            toastr()->error('This transaction already done!', 'Error!');
            return redirect()->back();
        }
        $data_customer = Customer::select('id', 'code', 'name', 'nik', 'address', 'sales_id')
            ->where('status', 1)->get();
        $data_sales = Sales::select('id', 'code', 'name', 'nik', 'address')
            ->where('status', 1)->get();

        $data_stock = Stock::get();

        $data_bookingorder = BookingOrder::orderBy('id', 'DESC')->first();

        return view('bookingorder.edit')
            ->with('data', $data)
            ->with('data_customer', $data_customer)
            ->with('data_sales', $data_sales)
            ->with('data_bookingorder', $data_bookingorder)
            ->with('gr', $this->gr)
            ->with('data_stock', $data_stock);
    }

    public function update(Request $request)
    {
        $bookingorder = BookingOrder::find($request->id);
        if($bookingorder == null){
            toastr()->error('Data not found!', 'Error!');
            return redirect()->back();
        }
        if($bookingorder->status == 1){
            toastr()->error('Data already Approved!', 'Error!');
            return redirect()->back();
        }
        if($bookingorder->status == 2){
            toastr()->error('Data already Canceled!', 'Error!');
            return redirect()->back();
        }

        $rule = [
            'customer_id' => 'required|exists:customer,id',
            'sales_id' => 'required|exists:sales,id',
            'date' => 'required',
            'qtys.*' => 'required|numeric|min:0',
            'prices.*' => 'required|numeric|min:0',
            'id_costs.*' => 'required|exists:additional_cost,id',
            'amounts.*' => 'required|numeric|min:0',
        ];

        if($request->ids != null)
        {
            $rule['ids.*'] = 'required|exists:stock,id';
        }

        Validator::validate($request->all(), $rule, [
            'ids.*' => 'stock not found!',
            'id_costs.*' => 'cost not found!',
        ]);

        DB::beginTransaction();

        try {

            $bookingorder->customer_id = $request->customer_id;
            $bookingorder->sales_id = $request->sales_id;
            $bookingorder->date = $request->date;
            $bookingorder->description = $request->description;
            $bookingorder->status = $request->status;

            if($request->status != 0)
            {
                $bookingorder->audit_at = date('Y-m-d H:i:s');
                $bookingorder->audit_by = Auth::user()->name;
            }
            $total = 0;

            BookingOrderDetail::where('booking_id', $bookingorder->id)->delete();
            if($request->ids != null)
            {
                foreach ($request->ids as $key => $value) {

                    $stock = Stock::find($value);

                    $bookingorder_detail = new BookingOrderDetail;
                    $bookingorder_detail->stock_id = $value;
                    $bookingorder_detail->booking_id = $bookingorder->id;
                    $bookingorder_detail->product_id = $stock->data_product->id;
                    $bookingorder_detail->qty = $request->qtys[$key];
                    $bookingorder_detail->selling_price = $request->prices[$key];
                    $bookingorder_detail->save();

                    $total += $bookingorder_detail->qty * $bookingorder_detail->selling_price;
                }
            }

            Cost::where('parent_id', $bookingorder->id)->where('gr', $this->gr)->delete();
            if($request->id_costs != null)
            {
                foreach ($request->id_costs as $key => $value) {
                    $additional_cost = AdditionalCost::find($value);

                    $cost = new Cost;
                    $cost->add_id = $value;
                    $cost->code = $additional_cost->code;
                    $cost->name = $additional_cost->name;
                    $cost->type = $additional_cost->type;
                    $cost->amount = $request->amounts[$key];
                    $cost->gr = $this->gr;
                    $cost->parent_id = $bookingorder->id;

                    if($cost->type == 0){
                        $cost->total = $cost->amount;
                    }else{
                        $cost->total = $total * $cost->amount / 100;
                    }

                    $cost->save();
                }
            }

            if($request->status == 1)
            {
                if(!Auth::user()->can('approve bookingorder'))
                {
                    toastr()->error('You have not permission to Approve!', 'Error!');
                    return redirect()->back();
                }

            }elseif ($request->status == 2) {
                if(!Auth::user()->can('reject bookingorder'))
                {
                    toastr()->error('You have not permission to Reject!', 'Error!');
                    return redirect()->back();
                }
            }

            $bookingorder->save();

            DB::commit();
            toastr()->success('Data Updated!', 'Success!');
        } catch (QueryException $e) {
            toastr()->error($e->getMessage(), 'Error!');
        }

        return redirect('/bookingorder');
    }

    public function destroye(Request $request, $id)
    {
        $data = BookingOrder::find($id);

        if($data == null){
            toastr()->error('Data not Found!', 'Error!');
            return redirect()->back();
        }else{
            if($data->status != 0){
                toastr()->error('Data already Done!', 'Error!');
                return redirect()->back();
            }
            $data->data_detail()->delete();
            $data->data_cost()->where('gr', $this->gr)->delete();
            $data->delete();
            toastr()->success('Data Deleted!', 'Success!');
            return redirect()->back();
        }
    }

}
