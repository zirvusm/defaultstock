<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingOrderDetail extends Model
{
    protected $table = 'booking_order_detail';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_parent()
    {
        return $this->hasOne('App\BookingOrder', 'id', 'booking_id');
    }

    public function data_stock()
    {
        return $this->hasOne('App\Stock', 'id', 'stock_id');
    }

    public function data_product()
    {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

}
