<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KasLog extends Model
{
    protected $table = 'kas_log';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_kas()
    {
        return $this->hasOne('App\Kas', 'id', 'kas_id');
    }

    public function data_transaction()
    {
        return $this->hasOne('App\Transaction', 'id', 'transaction_id');
    }

}
