<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingOrder extends Model
{
    protected $table = 'booking_order';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_sales()
    {
        return $this->hasOne('App\Sales', 'id', 'sales_id');
    }

    public function data_customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }

    public function data_detail()
    {
        return $this->hasMany('App\BookingOrderDetail', 'booking_id', 'id');
    }

    public function data_selling()
    {
        return $this->hasMany('App\Selling', 'booking_id', 'id');
    }

    public function data_cost()
    {
        return $this->hasMany('App\Cost', 'parent_id', 'id');
    }

    public function data_transaction()
    {
    	return $this->hasOne('App\Transaction', 'id', 'booking_id');
    }
}
