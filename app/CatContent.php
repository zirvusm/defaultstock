<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatContent extends Model
{
    protected $table = 'catcontent';
    public $timestamps = false;

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

}
