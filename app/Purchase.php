<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table = 'purchase';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_supplier()
    {
    	return $this->hasOne('App\Supplier', 'id', 'supplier_id');
    }

    public function data_gudang()
    {
    	return $this->hasOne('App\Gudang', 'id', 'gudang_id');
    }

    public function data_detail()
    {
        return $this->hasMany('App\PurchaseDetail', 'purchase_id', 'id');
    }

    public function data_cost()
    {
        return $this->hasMany('App\Cost', 'parent_id', 'id');
    }

}
