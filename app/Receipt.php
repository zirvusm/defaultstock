<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $table = 'receipt';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_detail()
    {
        return $this->hasMany('App\ReceiptDetail', 'parent_id', 'id');
    }

    public function data_sales()
    {
        return $this->hasOne('App\Sales', 'id', 'sales_id');
    }
}
