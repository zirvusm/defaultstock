<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banner';
    public $timestamps = false;

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

}

