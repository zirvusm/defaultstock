<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    protected $table = 'cost';
    public $timestamps = false;

    protected $hidden = [
        // 'id',
    ];

    public function data_parent()
    {
    	return $this->hasOne('App\AdditionalCost', 'id', 'add_id');
    }

}
