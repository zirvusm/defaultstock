<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatProduct extends Model
{
    protected $table = 'catproduct';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_product()
    {
    	return $this->hasMany('App\Product', 'category_id', 'id');
    }

}
