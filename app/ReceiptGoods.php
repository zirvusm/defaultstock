<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptGoods extends Model
{
    protected $table = 'receiptgoods';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_transaction()
    {
        return $this->hasOne("App\Transaction", "id", "transaction_id");
    }

    public function data_detail()
    {
        return $this->hasMany("App\ReceiptGoodsDetail", "parent_id", "id");
    }

}
