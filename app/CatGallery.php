<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatGallery extends Model
{
    protected $table = 'catgallery';
    public $timestamps = false;

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

}
