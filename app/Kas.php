<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kas extends Model
{
    protected $table = 'kas';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_transaction()
    {
        return $this->hasMany('App\Transaction', 'kas_id', 'id');
    }

    public function data_payment()
    {
        return $this->hasMany('App\Payment', 'kas_id', 'id');
    }

}
