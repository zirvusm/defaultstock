<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    protected $table = 'purchase_detail';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_parent()
    {
        return $this->hasOne('App\Purchase', 'id', 'purchase_id');
    }

    public function data_product()
    {
    	return $this->hasOne('App\Product', 'id', 'product_id');
    }

}
