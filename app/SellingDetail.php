<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellingDetail extends Model
{
    protected $table = 'selling_detail';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_parent()
    {
        return $this->hasOne('App\Selling', 'id', 'selling_id');
    }

    public function data_stock()
    {
        return $this->hasOne('App\Stock', 'id', 'stock_id');
    }

    public function data_product()
    {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

}
