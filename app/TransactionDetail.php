<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $table = 'transaction_detail';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_parent()
    {
        return $this->hasOne('App\Transaction', 'id', 'parent_id');
    }

    public function data_product()
    {
    	return $this->hasOne('App\Product', 'id', 'product_id');
    }

}
