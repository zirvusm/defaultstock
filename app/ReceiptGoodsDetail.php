<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptGoodsDetail extends Model
{
    protected $table = 'receiptgoodsdetail';

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_parent()
    {
        return $this->hasOne("App\ReceiptGoods", "id", "parent_id");
    }

    public function data_product()
    {
        return $this->hasOne("App\Product", "id", "product_id");
    }

}
