<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalCost extends Model
{
    protected $table = 'additional_cost';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

}
