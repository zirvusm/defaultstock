<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stock';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_gudang()
    {
        return $this->hasOne('App\Gudang', 'id', 'gudang_id');
    }

    public function data_supplier()
    {
        return $this->hasOne('App\Supplier', 'id', 'supplier_id');
    }

    public function data_product()
    {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

    public function data_transaction()
    {
        return $this->hasMany('App\TransactionDetail', 'stock_id', 'id');
    }

}
