<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_category()
    {
        return $this->hasOne('App\CatProduct', 'id', 'category_id');
    }

    public function data_stock()
    {
        return $this->hasMany('App\Stock', 'product_id', 'id');
    }

    public function data_purchase_detail()
    {
        return $this->hasMany('App\PurchaseDetail', 'product_id', 'id');
    }

    public function data_transaction()
    {
        return $this->hasMany('App\TransactionDetail', 'product_id', 'id');
    }

}
