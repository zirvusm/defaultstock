<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //type      (buy)       (sell)
    //status    0(pending)  1(approve)  2(reject)
    //pay_off   0(pending)  1(done)
    //is_po     0(false)    1(true)
    //is_retur  0(false)    1(true)

    protected $table = 'transaction';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_kas()
    {
    	return $this->hasOne('App\Kas', 'id', 'kas_id');
    }

    public function data_payment()
    {
        return $this->hasMany('App\KasLog', 'transaction_id', 'id');
    }

    public function data_customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }

    public function data_sales()
    {
        return $this->hasOne('App\Sales', 'id', 'sales_id');
    }

    public function data_supplier()
    {
        return $this->hasOne('App\Supplier', 'id', 'supplier_id');
    }

    public function data_gudang()
    {
    	return $this->hasOne('App\Gudang', 'id', 'gudang_id');
    }

    public function data_detail()
    {
        return $this->hasMany('App\TransactionDetail', 'parent_id', 'id');
    }

    public function data_cost()
    {
        return $this->hasMany('App\Cost', 'parent_id', 'id');
    }

    public function data_booking()
    {
        return $this->hasOne('App\BookingOrder', 'booking_id', 'id');
    }

}
