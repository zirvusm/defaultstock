<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'sales';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_customer()
    {
    	return $this->hasMany('App\Customer', 'sales_id', 'id');
    }
}
