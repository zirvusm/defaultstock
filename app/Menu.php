<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    public function data_parent()
    {
    	return $this->hasOne('App\Menu', 'id', 'parent_id');
    }

    public function data_child()
    {
    	return $this->hasMany('App\Menu', 'parent_id', 'id');
    }
}
