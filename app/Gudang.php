<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gudang extends Model
{
    protected $table = 'gudang';
    // public $timestamps = false;

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_parent()
    {
    	return $this->hasOne('App\Gudang', 'id', 'parent_id');
    }

    public function data_child()
    {
        return $this->hasMany('App\Gudang', 'parent_id', 'id');
    }

    public function data_purchase()
    {
        return $this->hasMany('App\Purchase', 'gudang_id', 'id');
    }

}
