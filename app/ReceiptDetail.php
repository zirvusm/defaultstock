<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptDetail extends Model
{
    protected $table = 'receipt_detail';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_parent()
    {
        return $this->hasOne('App\Receipt', 'id', 'parent_id');
    }

    public function data_transaction()
    {
        return $this->hasOne('App\Transaction', 'id', 'transaction_id');
    }

}
