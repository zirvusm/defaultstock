<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payment';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function data_kas()
    {
        return $this->hasOne('App\Kas', 'id', 'kas_id');
    }

    public function data_transaction()
    {
        return $this->hasOne('App\Transaction', 'id', 'transaction_id');
    }

}
