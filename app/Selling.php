<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Selling extends Model
{
    protected $table = 'selling';
    // public $timestamps = false;

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    public function data_sales()
    {
        return $this->hasOne('App\Sales', 'id', 'sales_id');
    }

    public function data_customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }

    public function data_booking()
    {
        return $this->hasOne('App\BookingOrder', 'id', 'booking_id');
    }

    public function data_detail()
    {
        return $this->hasMany('App\SellingDetail', 'selling_id', 'id');
    }

    public function data_cost()
    {
        return $this->hasMany('App\Cost', 'parent_id', 'id');
    }


}
