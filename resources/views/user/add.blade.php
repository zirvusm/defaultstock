@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Add User</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/user/store')}}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="roles" class="col-sm-2 col-form-label">Role</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('roles'))?'is-invalid':''}}" id="roles" name="roles">
                            @foreach($roles as $key => $role)
                                <option value="{{$key}}">{{$role}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('roles'))
                            <span id="roles-error" class="error invalid-feedback">{{$errors->first('roles')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" id="name" name="name" id="name">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control {{($errors->has('email'))?'is-invalid':''}}" id="email" name="email" id="email">
                        @if($errors->has('email'))
                            <span id="email-error" class="error invalid-feedback">{{$errors->first('email')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="img" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('img'))?'is-invalid':''}}" id="img" name="img" id="img">
                        @if($errors->has('img'))
                            <span id="img-error" class="error invalid-feedback">{{$errors->first('img')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control {{($errors->has('password'))?'is-invalid':''}}" id="password" name="password" id="password">
                        @if($errors->has('password'))
                            <span id="password-error" class="error invalid-feedback">{{$errors->first('password')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cpassword" class="col-sm-2 col-form-label">Confirmation Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control {{($errors->has('cpassword'))?'is-invalid':''}}" id="cpassword" name="cpassword" id="cpassword">
                        @if($errors->has('cpassword'))
                            <span id="cpassword-error" class="error invalid-feedback">{{$errors->first('cpassword')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/user')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop