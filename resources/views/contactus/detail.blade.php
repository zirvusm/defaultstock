@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">View Message</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" id="name" value="{{$data->name}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control {{($errors->has('email'))?'is-invalid':''}}" name="email" id="email" value="{{$data->email}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Message</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="4" name="message" id="message" readonly="">{{$data->message}}</textarea>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="float-right">
                <a href="{{URL::to('/contactus')}}" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')

@stop