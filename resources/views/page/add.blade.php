@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Add Page</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/page/store')}}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" id="name">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                @if(env('USE_LANG'))
                    <div class="form-group row">
                        <label for="lang" class="col-sm-2 col-form-label">Lang</label>
                        <div class="col-sm-10">
                            <select class="form-control {{($errors->has('lang'))?'is-invalid':''}}" name="lang" id="lang">
                                <option value="en">En</option>
                                <option value="id">ID</option>
                                <option value="cn">Cn</option>
                            </select>
                            @if($errors->has('lang'))
                                <span id="lang-error" class="error invalid-feedback">{{$errors->first('lang')}}</span>
                            @endif
                        </div>
                    </div>
                @endif
                <div class="form-group row">
                    <label for="prefix" class="col-sm-2 col-form-label">Prefix</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('prefix'))?'is-invalid':''}}" name="prefix" id="prefix">
                        @if($errors->has('prefix'))
                            <span id="prefix-error" class="error invalid-feedback">{{$errors->first('prefix')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" name="description" rows="4"></textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="content" class="col-sm-12 col-form-label">Content</label>
                    <div class="col-sm-12">
                        <textarea class="form-control texteditor {{($errors->has('content'))?'is-invalid':''}}" name="content" name="content" rows="20"></textarea>
                        @if($errors->has('content'))
                            <span id="content-error" class="error invalid-feedback">{{$errors->first('content')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/page')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop