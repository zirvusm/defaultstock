@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Edit Gallery</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/gallery/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label for="parent_id" class="col-sm-2 col-form-label">Category</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('parent_id'))?'is-invalid':''}}" id="parent_id" name="parent_id">
                            @foreach($cat as $key => $value)
                                <option value="{{$value->id}}" @if($data->parent_id == $value->id) selected="" @endif>{{$value->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('parent_id'))
                            <span id="parent_id-error" class="error invalid-feedback">{{$errors->first('parent_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" id="name" name="name" id="name" value="{{$data->name}}">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" id="description" name="description" rows="4">{{$data->description}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="image" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" accept=".jpg,.jpeg,.png,.gif,.svg" class="custom-file-input" name="image" id="image" value="">
                                <label class="custom-file-label" for="image" id="textImage">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Upload</span>
                            </div>
                        </div>
                        @if($errors->has('image'))
                            <span id="image-error" class="error invalid-feedback">{{$errors->first('image')}}</span>
                        @endif
                    </div>
                </div>
                <img src="{{$data->image}}" id="preview" width="30%">
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/gallery')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        $('#image').change(function(){
            $('#textImage').html(this.value);
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop