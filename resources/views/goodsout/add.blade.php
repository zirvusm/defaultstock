@extends('app')
@extends('modals.receiptgoods')

@section('module_name')
    <h1 class="m-0 text-dark">Tambah Keluar Barang</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/goodsout/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="transaction_id" class="col-sm-2 col-form-label">Nomor Faktur</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="transaction_id" id="transaction_id" value="">
                        <input type="text" class="form-control {{($errors->has('transaction_id'))?'is-invalid':''}}" name="transaction_name" id="transaction_name" readonly="" value="">

                        <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#transactionModal"><i class="fa fa-folder-open"></i></button>

                        @if($errors->has('transaction_id'))
                            <span id="transaction_id-error" class="error invalid-feedback">{{$errors->first('transaction_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{date('Y-m-d')}}">
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="4" id="description" name="description"></textarea>
                    </div>
                </div>
            </div>

            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Qty</th>
                        <th>Action</th>
                    </thead>
                    <tbody id="tbl_products">
                    </tbody>
                </table>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/goodsout')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function removeProducts(index)
        {
            $("#tr_product_"+index).remove();
        }

        $("#transaction_id").change(function(){
            $.ajax({
                url: "{{URL::to('transaction/detail')}}/"+$("#transaction_id").val(),
                type: "GET",
                success: function(response){
                    $("#tbl_products").html("");

                    let ht = '';
                    console.log(response);

                    response.forEach(function(detail, index){
                        ht += '<tr id="tr_product_'+index+'">';
                        ht += '<td>';
                        ht += '<input type="hidden" name="ids[]" value="'+detail.data_product.id+'">';
                        ht += '<input type="hidden" name="stockids[]" value="'+detail.stock_id+'">';
                        ht += detail.data_product.code;
                        ht += '</td>';
                        ht += '<td>'+detail.data_product.name+'</td>';
                        ht += '<td><input type="number" step="1" min="1" max="'+(detail.qty-detail.receive)+'" class="form-control" name="qtys[]" value="'+(detail.qty-detail.receive)+'"></td>';
                        ht += '<td>';
                        ht += '<button type="button" class="btn btn-danger" onclick="removeProducts('+index+')"><i class="fas fa-trash-alt"></i></button>';
                        ht += '</td>';
                        ht += '</tr>';
                    });

                    $("#tbl_products").html(ht);
                },
            });
        });

    </script>

@stop
