@extends('app')
@extends('modals.purchase')

@section('module_name')
    <h1 class="m-0 text-dark">Tambah Pembayaran</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/purchasepayment/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="purchase_id" class="col-sm-2 col-form-label">Nomor Faktur</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="purchase_id" id="purchase_id" value="{{old('purchase_id')}}">
                        <input type="text" class="form-control {{($errors->has('purchase_id'))?'is-invalid':''}}" name="purchase_name" id="purchase_name" readonly="" value="{{old('purchase_name')}}">

                        <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#purchaseModal"><i class="fa fa-folder-open"></i></button>

                        @if($errors->has('purchase_id'))
                            <span id="purchase_id-error" class="error invalid-feedback">{{$errors->first('purchase_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal Bayar</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{date('Y-m-d')}}">
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="amount" class="col-sm-2 col-form-label">Jumlah Bayar</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control {{($errors->has('amount'))?'is-invalid':''}}" name="amount" id="amount" value="{{old('amount') ?? 0}}" min="0" max="0">
                        @if($errors->has('amount'))
                            <span id="amount-error" class="error invalid-feedback">{{$errors->first('amount')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" id="description" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
                
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/purchasepayment')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop