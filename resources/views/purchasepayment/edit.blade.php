@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Ubah Pembayaran</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/purchasepayment/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label for="purchase_id" class="col-sm-2 col-form-label">Nomor Faktur</label>
                    <div class="input-group col-sm-10">
                        <input type="text" class="form-control" readonly="" value="{{$data->data_transaction->invoice}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Grand Total</label>
                    <div class="input-group col-sm-10">
                        <input type="text" class="form-control" readonly="" value="{{number_format($grand_total)}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Total di Bayar</label>
                    <div class="input-group col-sm-10">
                        <input type="text" class="form-control" readonly="" value="{{number_format($total_paid)}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Sisa</label>
                    <div class="input-group col-sm-10">
                        <input type="text" class="form-control" readonly="" value="{{number_format($total_unpaid)}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal Bayar</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{$data->date}}" @if($data->status == 1) readonly="" @endif>
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="amount" class="col-sm-2 col-form-label">Jumlah Bayar</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control {{($errors->has('amount'))?'is-invalid':''}}" name="amount" id="amount" value="{{$data->amount ?? 0}}" min="0" max="{{$total_unpaid}}" @if($data->status == 1) readonly="" @endif>
                        @if($errors->has('amount'))
                            <span id="amount-error" class="error invalid-feedback">{{$errors->first('amount')}}</span>
                        @endif
                    </div>
                </div>
                @if(Auth::user()->can('approve purchasepayment') || Auth::user()->can('reject purchasepayment'))
                    <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control {{($errors->has('status'))?'is-invalid':''}}" id="status" name="status" @if($data->status == 1) disabled @endif>
                                <option value="0">Pending</option>
                                <option value="1" @if($data->status == 1) selected="" @endif>Approve</option>
                            </select>
                            @if($errors->has('status'))
                                <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                            @endif
                        </div>
                    </div>
                @endif
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" id="description" rows="4" @if($data->status == 1) readonly="" @endif>{{$data->description}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/purchasepayment')}}" class="btn btn-default">Back</a>
                    @if($data->status != 1)
                        <button type="submit" class="btn btn-warning">Update</button>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
