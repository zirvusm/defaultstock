@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Pembayaran Pembelian</h1>
@stop


@section('content')

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

	<div class="card">

        <div class="card-header border-0">
            <h3 class="card-title">Pembayaran Pembelian</h3>
            <div class="card-tools">
                @can('add purchasepayment')
                    <a href="{{URL::to('/purchasepayment/add')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></a>
                @endcan
                <a href="{{URL::to('/purchasepayment')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <select class="form-control" name="filter" id="filter">
                        <option value="">--Pilih Filter--</option>
                        <option value="data_kas.name" @if($filter == 'data_kas.name') selected="" @endif>Kas</option>
                        <option value="date" @if($filter == 'date') selected="" @endif>Tanggal</option>
                        <option value="data_transaction.invoice" @if($filter == 'data_transaction.invoice') selected="" @endif>Nomor Faktur</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="value" placeholder="Cari . . ." name="value" id="value" value="{{$value}}">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-default" onclick="searchData()"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>#</th>
                        <th>Kas</th>
                        <th>Supplier</th>
                        <th>Invoice</th>
                        <th>Tanggal</th>
                        <th>Jumlah di Bayar</th>
                        <th>Status</th>
                        <th>Di Sejutui</th>
                        <th width="180px">
                            Action
                        </th>
        			</tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
        					<tr>
        						<td>{{($key+1) + (($page - 1) * $take)}}</td>
                                <td>{{$dt->data_kas->name}}</td>
                                <td>{{$dt->data_transaction->data_supplier->name}}</td>
                                <td>{{$dt->data_transaction->invoice}}</td>
                                <td>{{$dt->date}}</td>
                                <td>
                                    @if($dt->status == 1)
                                        <font color="lime">{{number_format($dt->amount)}}</font>
                                    @else
                                        {{number_format($dt->amount)}}
                                    @endif
                                </td>
                                <td>
                                    @if($dt->status == 0)
                                        <font color="orange">Menunggu</font>
                                    @elseif($dt->status == 1)
                                        <font color="success">
                                            Di Setujui
                                        </font>
                                    @elseif($dt->status == 2)
                                        <font color="red">Di Batalkan</font>
                                    @endif
                                </td>
                                <td align="center">
                                    @if($dt->status == 1)
                                        {{$dt->audit_by}}<br>{{$dt->audit_at}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    <div class="row">
                                        @can('update purchasepayment')
                                            @can('reject purchasepayment')
                                                <div class="col-sm-4">
                                                    <a href="{{URL::to('/purchasepayment/'.$dt->id)}}" class="btn btn-warning">
                                                        <i class="fas fa-pencil-alt"></i>
                                                    </a>
                                                </div>
                                            @endcan
                                        @endcan
                                        @can('delete purchasepayment')
                                            @if($dt->status == 1)
                                                @can('reject purchasepayment')
                                                    <div class="col-sm-4">
                                                        <button type="button" class="btn btn-danger" onclick="beforeDelete('{{URL::to('/purchasepayment/destroye/'.$dt->id)}}')">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </button>
                                                    </div>
                                                @endcan
                                            @else
                                                <div class="col-sm-4">
                                                    <button type="button" class="btn btn-danger" onclick="beforeDelete('{{URL::to('/purchasepayment/destroye/'.$dt->id)}}')">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </div>
                                            @endif
                                        @endcan
                                    </div>
                                </td>
        					</tr>
        				@endforeach
        			@endif
        		</tbody>
        	</table>
        </div>

        <div class="card-footer">
        	<div class="float-left">
                <select class="form-control" onchange="searchData()" id="take_data" name="take_data">
                    @foreach(HelperController::$take as $key => $jumlah)
                        <option value="{{$jumlah}}" @if($take == $jumlah) selected="" @endif>{{$jumlah}}</option>
                    @endforeach
                </select>
                <label class="label-control">Of : {{$count}} Data</label>
        	</div>
        	<div class="float-right">
        		{!!$data->appends(['take' => $take, 'filter' => $filter, 'value' => $value])->links()!!}
        	</div>
        </div>

	</div>
@stop
