@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Ubah Bank</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/bank/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" id="name" value="{{$data->name}}">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="account_name" class="col-sm-2 col-form-label">Nama Akun</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('account_name'))?'is-invalid':''}}" name="account_name" id="account_name" value="{{$data->account_name}}">
                        @if($errors->has('account_name'))
                            <span id="account_name-error" class="error invalid-feedback">{{$errors->first('account_name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="account_number" class="col-sm-2 col-form-label">Nomor Akun</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('account_number'))?'is-invalid':''}}" name="account_number" id="account_number" value="{{$data->account_number}}">
                        @if($errors->has('account_number'))
                            <span id="account_number-error" class="error invalid-feedback">{{$errors->first('account_number')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="balance" class="col-sm-2 col-form-label">Saldo</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control {{($errors->has('balance'))?'is-invalid':''}}" name="balance" id="balance" value="{{$data->balance}}">
                        @if($errors->has('balance'))
                            <span id="balance-error" class="error invalid-feedback">{{$errors->first('balance')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('status'))?'is-invalid':''}}" id="status" name="status">
                            <option value="1">Aktif</option>
                            <option value="0" @if($data->status == 0) selected="" @endif>Tidak Aktif</option>
                        </select>
                        @if($errors->has('status'))
                            <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" id="description" name="description" rows="4">{{$data->description}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/bank')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop