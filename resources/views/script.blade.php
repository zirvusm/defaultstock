@extends('modals.profile')

@php
	if(count($errors) > 0)
		toastr()->error($errors->first(), 'Error!');
@endphp

<script type="text/javascript">

	$(document).ready(function(){
		$('.texteditor').summernote({
			height: 500
		});
	});

	hideBlink = () => {
		setTimeout(function(){
			if($("#notif").data('blink'))
			{
				$("#notif").hide();
			}
			showBlink();
		}, 500);
	}

	showBlink = () => {
		setTimeout(function(){ 
			if($("#notif").data('blink'))
			{
				$("#notif").show();
			}
			hideBlink();
		}, 500);
	}

	urlParam = function(name){
		let results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		let res = '';
		try {
			res = results[1] || 0
		}
		catch(err) {
			res = '';
		}
		return res;
	}

	$('#btn_profile_setting').click(function(){
		$('#profile_setting').modal('show');
	});

	searchData = () => {
		let url = window.location.origin + window.location.pathname;
		let page = urlParam('page') ? urlParam('page') : 1;
		let take = $('#take_data').val();
		let filter = $('#filter').val();
		let value = $('#value').val();

		let query = '';

		if(page != ''){
			if(query != '') query += '&';
			query += 'page='+page;
		}
		if(take != ''){
			if(query != '') query += '&';
			query += 'take='+take;
		}
		if(filter != '' && filter != undefined){
			if(query != '') query += '&';
			query += 'filter='+filter;
		}
		if(value != '' && value != undefined){
			if(query != '') query += '&';
			query += 'value='+value;
		}

		location.replace(url+'?'+query);
	}

	beforeDelete = (url) => 
	{
		if(confirm('Are you sure to delete this data?')){
			location.replace(url);
		}
	}

	getModal = (url, modal) => 
	{
		$('#for_inject').html('');
        $.ajax({
            url: url,
            type:'GET',
            success: function(data)
            {
                $('#for_inject').html(data);
                $(modal).modal('show');
            },
            error: function(request, status, error)
            {
            	console.log(url);
            	console.log(request.responseText)
            }
        });
	}

	$('.select_action').change(function(){
		let id = $(this).data('id');
		let menu = $(this).data('menu');
		let modal = $(this).data('tag');
		let action = $(this).val();

		if(action != '')
		{
			switch(action) {
				case 'detail':
					location.replace('/'+menu+'/detail/'+id);
					break;
				case 'update':
					location.replace('/'+menu+'/'+id);
					break;
				case 'trans_history':
					getModal('/'+menu+'/transfer/'+id, modal);
					break;
				case 'bet_history':
					getModal('/'+menu+'/bet/'+id, modal);
					break;
				case 'deposit':
					getModal('/'+menu+'/deposit/'+id, modal);
					break;
				case 'withdraw':
					getModal('/'+menu+'/withdraw/'+id, modal);
					break;
				default:
					break;
			}
			$(this).val('');
		}
	});

    filterModal = () => {
        
        let filter = $("#filterModal").val();
        let value = $("#valueModal").val().toLowerCase();

        if(filter == '' || value == '')
        {
        	$('.modalFilter').filter(function(){
				return true;
			}).show();
        }else{
			$('.modalFilter').hide();

        	$('.modalFilter').filter(function(){
				return $(this).data(filter).match(value);
				// return $(this).data(filter) == value;
			}).show();
        }
		
    }

    filterCostModal = () => {
        
        let filter = $("#filterCostModal").val();
        let value = $("#valueCostModal").val().toLowerCase();

        if(filter == '' || value == '')
        {
        	$('.modalCostFilter').filter(function(){
				return true;
			}).show();
        }else{
			$('.modalCostFilter').hide();

        	$('.modalCostFilter').filter(function(){
				return $(this).data(filter).match(value);
				// return $(this).data(filter) == value;
			}).show();
        }
		
    }

    filterModalCustomer = () => {
        
        let filter = $("#filterModal_customer").val();
        let value = $("#valueModal_customer").val().toLowerCase();

        if(filter == '' || value == '')
        {
        	$('.modalFilterCustomer').filter(function(){
				return true;
			}).show();
        }else{
			$('.modalFilterCustomer').hide();

        	$('.modalFilterCustomer').filter(function(){
				return $(this).data(filter).match(value);
				// return $(this).data(filter) == value;
			}).show();
        }
		
    }

    filterModalSales = () => {
        
        let filter = $("#filterModal_sales").val();
        let value = $("#valueModal_sales").val().toLowerCase();

        if(filter == '' || value == '')
        {
        	$('.modalFilterSales').filter(function(){
				return true;
			}).show();
        }else{
			$('.modalFilterSales').hide();

        	$('.modalFilterSales').filter(function(){
				return $(this).data(filter).match(value);
				// return $(this).data(filter) == value;
			}).show();
        }
		
    }

    filterModalSelling = () => {
        
        let filter = $("#filterModal_selling").val();
        let value = $("#valueModal_selling").val().toLowerCase();

        if(filter == '' || value == '')
        {
        	$('.modalFilterSelling').filter(function(){
				return true;
			}).show();
        }else{
			$('.modalFilterSelling').hide();

        	$('.modalFilterSelling').filter(function(){
				return $(this).data(filter).match(value);
				// return $(this).data(filter) == value;
			}).show();
        }
		
    }

    filterData = () => {
		let url = window.location.origin + window.location.pathname;
		let page = urlParam('page') ? urlParam('page') : 1;
		let take = $('#take_data').val();
		let query = "";

    	var filter_arr = JSON.parse($("#filter_arr").val());

		if(page != '' && page != undefined){
			if(query != '') query += '&';
			query += 'page='+page;
		}
		if(take != '' && take != undefined){
			if(query != '') query += '&';
			query += 'take='+take;
		}

    	Object.keys(filter_arr).forEach(function(dt, index){
    		let filter = $("#"+dt.replace(".", "-")).val();

    		if(filter != '' && filter != undefined){
				if(query != '') query += '&';
				query += dt+'='+filter;
			}
    	});
    	
		location.replace(url+'?'+query);
    }

    countTotalStock = () => {
	    let total_all = 0;
	    for (let i = 0; i <= countAddStock; i++) {
	        let qty = $("#qty_"+i).val();
	        let price = $("#price_"+i).val();

	        if(!isNaN(qty) && !isNaN(price))
	        {
	            total_all += qty * price;
	        }
	    }
	    total_all = (total_all).toString(),
	        sisa_all    = total_all.length % 3,
	        rupiah_all  = total_all.substr(0, sisa_all),
	        ribuan_all  = total_all.substr(sisa_all).match(/\d{3}/g);
	            
	    if (ribuan_all) {
	        separator_all = sisa_all ? ',' : '';
	        rupiah_all += separator_all + ribuan_all.join(',');
	    }
	    
	    $("#total_all").val(rupiah_all);

        if(typeof countTotalCost === 'function'){
            countTotalCost();
        }
        if(typeof countGrandTotalCost === 'function'){
            countGrandTotalCost();
        }
    }

    strNumberFormat = (str) => {
        let total = str.toString(),
            sisa    = total.length % 3,
            rupiah  = total.substr(0, sisa),
            ribuan  = total.substr(sisa).match(/\d{3}/g);
                
        if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }
        return rupiah;
    }

</script>