@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Lap. Pituang</h1>
@stop

@php
    $total = 0;
    $current_customer = 0;
    $current_total = 0;
@endphp

@section('content')

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    
	<div class="card">

        <div class="card-header border-0">
            <h3 class="card-title">Lap. Pituang</h3>
            <div class="card-tools">
                <a href="{{URL::to('/lappiutang')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <select class="form-control" name="customer_id" id="customer_id">
                        <option value="">--Pilih Customer--</option>
                        @foreach($data_customer as $key => $customer)
                            <option value="{{$customer->id}}" @if($customer->id == $customer_id) selected @endif>
                                {{$customer->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" name="sales_id" id="sales_id">
                        <option value="">--Pilih Sales--</option>
                        @foreach($data_sales as $key => $sales)
                            <option value="{{$sales->id}}" @if($sales->id == $sales_id) selected @endif>
                                {{$sales->name}} ( {{$sales->code}} )
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" name="jto" id="jto">
                        <option value="">--Pilih Jenis--</option>
                        <option value="0" @if($jto == 0 && $jto != "") selected @endif>Belum Jatuh Tempo</option>
                        <option value="1" @if($jto == 1 && $jto != "") selected @endif>Sudah Jatuh Tempo</option>
                    </select>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-default" onclick="searchDataUnique()"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>#</th>
                        <th>Tanggal</th>
                        <th>Tanggal JTO</th>
                        <th>Nomor Faktur</th>
                        <th>Jumlah</th>
        			</tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
                            @if($current_customer != $dt->customer_id)
                                @if($current_customer > 0)
                                    <tr>
                                        <td colspan="4" align="center"><b>Total</b></td>
                                        <td>
                                            <b>{{number_format($current_total)}}</b>
                                        </td>
                                    </tr>
                                    @php
                                        $current_total = 0;
                                    @endphp
                                @endif
                                <tr>
                                    <td colspan="5">
                                        <b>{{$dt->data_customer->name ?? "-"}}</b>
                                    </td>
                                </tr>
                            @endif
                            @php
                                $jumlah = $dt->amount - $dt->data_payment()->where('type', 'buy')->where('status', 1)->sum('amount');
                                $total += $jumlah;
                                $current_total += $jumlah;
                            @endphp
                            <tr>
                                <td>{{($key + 1)}}</td>
                                <td>{{\DateTime::createFromFormat('Y-m-d', $dt->date)->format('d-M-Y')}}</td>
                                <td>
                                    @if($dt->due_date != null)
                                        {{\DateTime::createFromFormat('Y-m-d', $dt->due_date)->format('d-M-Y')}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{$dt->invoice}}</td>
                                <td>
                                    {{number_format($jumlah)}}
                                </td>
                            </tr>
                            @php
                                $current_customer = $dt->customer_id;
                            @endphp
        				@endforeach
                        <tr>
                            <td colspan="4" align="center"><b>Total</b></td>
                            <td>
                                <b>{{number_format($current_total)}}</b>
                            </td>
                        </tr>
        			@endif
        		</tbody>
                <tfoot>
                    <tr><td colspan="5">&nbsp;</td></tr>
                    <tr>
                        <td colspan="4" align="center"><b>Grand Total</b></td>
                        <td><b>{{number_format($total)}}</b></td>
                    </tr>
                </tfoot>
        	</table>
        </div>
	</div>

    <script type="text/javascript">
        
        searchDataUnique = () => {
            let url = window.location.origin + window.location.pathname;
            let customer_id = $('#customer_id').val();
            let sales_id = $('#sales_id').val();
            let jto = $('#jto').val();

            let query = '';

            if(customer_id != ''){
                if(query != '') query += '&';
                query += 'customer_id='+customer_id;
            }

            if(jto != ''){
                if(query != '') query += '&';
                query += 'jto='+jto;
            }

            if(sales_id != ''){
                if(query != '') query += '&';
                query += 'sales_id='+sales_id;
            }

            location.replace(url+'?'+query);
        }

    </script>
@stop