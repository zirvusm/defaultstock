@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Welcome to {{$data['full_name']}}!</h1>
@stop


@section('content')

<style type="text/css">
    .my-custom-scrollbar {
        position: relative;
        overflow: auto;
    }
    .table-wrapper-scroll-y {
        display: block;
    }
</style>

<div class="card">
	<div class="card-header border-0">
        <h3 class="card-title">{{$data['full_name']}}</h3>
    </div>
    <div class="card-body">
    	Email : <a href="mailto:{{$data['email']}}">{{$data['email']}}</a></br>
		Address : {{$data['address']}}</br>
		Hp : <a href="tel:{{$data['email']}}">{{$data['email']}}</a></br>
		Whatsapp : <a href="https://wa.me/{{$data['wa']}}">{{$data['wa']}}</a></br>
		Facebook : <a href="{{$data['fb']}}">{{$data['fb']}}</a></br>
		Instagram : <a href="{{$data['ig']}}">{{$data['ig']}}</a></br>
		{{-- Facebook : <a href="https://facebook.com/{{$data['fb']}}">{{$data['fb']}}</a></br> --}}
		{{-- Instagram : <a href="https://www.instagram.com/{{$data['ig']}}">{{$data['ig']}}</a></br> --}}
		Maps : <br>
		<div class="card-body col-md-12">
			<table class="table table-bordered table-hover">
				<tbody>
					<tr>
						<th>
							<iframe src="{{$data['iframe']}}" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
						</th>
					</tr>
				</tbody>
			</table>
		</div>
    </div>
		
</div>

<div id="for_inject"></div>
@stop