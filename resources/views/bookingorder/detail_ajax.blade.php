@php
    $total = 0;
    $count = 0;
@endphp
@foreach($data->data_detail as $key => $detail)
    @php
        $hs = $detail->qty * $detail->selling_price;
        $total += $hs;
        $count++;
    @endphp
    <tr id="tr_add_stock_{{$key}}">
        <td>
            <input type="hidden" name="stockcodes[]" value="{{$detail->data_product->code}}">
            {{$detail->data_product->code}}
        </td>
        <td>
            <input type="hidden" name="stocknames[]" value="{{$detail->data_product->name}}">
            <input type="hidden" name="ids[]" value="{{$detail->stock_id}}">
            {{$detail->data_product->name}}
        </td>
        <td>
            <input type="number" class="form-control" id="qty_{{$key}}" name="qtys[]" value="{{$detail->qty}}" min="0" onchange="countTotal({{$key}})">
        </td>
        <td>
            <input type="number" class="form-control" id="price_{{$key}}" name="prices[]" value="{{$detail->selling_price}}" min="0" step="0.1" onchange="countTotal({{$key}})">
        </td>
        <td>
            <input type="text" class="form-control" id="totals_{{$key}}" readonly="" value="{{number_format($hs)}}">
        </td>
        <td>
            <button type="button" class="btn btn-danger" onclick="removeAddProduct({{$key}})"><i class="fas fa-trash"></i></button>
        </td>
    </tr>
@endforeach

<script type="text/javascript">
    countAddStock = {{$count}};
    countTotalStock();
</script>