@php
    $total = 0;
    $total_cost = 0;
    $count_cost = 0;
    $grand_total = 0;
@endphp
@foreach($data->data_detail as $key => $detail)
    @php
        $hs = $detail->qty * $detail->selling_price;
        $total += $hs;
    @endphp
@endforeach

@foreach($data->data_cost()->where('gr', $gr)->get() as $key_cost => $cost)
    @php
        if($cost->type == 0)
        {
            $hs_cost = $cost->amount;
        }else{
            $hs_cost = $total * $cost->amount / 100;
        }
        $total_cost += $hs_cost;
        $grand_total += $hs_cost;
        $count_cost++;
    @endphp
    <tr id="tr_add_cost_{{$key_cost}}">
        <td>
            <input type="hidden" name="costcodes[]" value="{{$cost->code}}">
            {{$cost->code}}
        </td>
        <td>
            <input type="hidden" name="costnames[]" value="{{$cost->name}}">
            <input type="hidden" name="id_costs[]" value="{{$cost->data_parent->id}}">
            {{$cost->name}}
        </td>
        <td>
            @if($cost->type == 0)
                <input type="number" class="form-control" id="amount_cost_{{$key_cost}}" name="amounts[]" value="{{$cost->amount}}" data-type="0" min="0" step="1" onchange="countCost_total({{$cost->amount}}, {{$cost->type}})">
            @else
                <input type="number" class="form-control" id="amount_cost_{{$key_cost}}" name="amounts[]" value="{{$cost->amount}}" data-type="1" min="0" max="100" step="0.1" onchange="countCost_total({{$cost->amount}}, {{$cost->type}})">
            @endif
        </td>
        <td>
            <input type="text" class="form-control" id="totals_cost_{{$key_cost}}" readonly="" value="{{number_format($hs_cost)}}">
        </td>
        <td>
            <button type="button" class="btn btn-danger" onclick="removeCost({{$key_cost}})"><i class="fas fa-trash"></i></button>
        </td>
    </tr>
@endforeach

<script type="text/javascript">
    countCost = {{$count_cost}};

    if(typeof countTotalCost === 'function'){
        countTotalCost();
    }
    if(typeof countGrandTotalCost === 'function'){
        countGrandTotalCost();
    }
</script>