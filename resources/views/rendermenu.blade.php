@php
  use App\Menu;
  $current = url()->current();
  $baseurl = URL::to('/');
  $path = str_replace($baseurl.'/', '', $current);
  $path_arr = explode('/', $path); 

  $menus = Menu::where('status', 1)->where('is_menu', 1)
    ->where('parent_id', $parent_id)->orderBy('order')->get();

  foreach ($menus as $key => $menu) {
    $cek = $menu->data_child()->where('status', 1)->orderBy('order')->get();
    $menu->selected = false;
    foreach ($cek as $k => $v) {
      $v->selected = false;
      if($v->route == $path_arr[0]){
        $v->selected = true;
        $menu->selected = true;
      }
    }
    if($menu->route == $path_arr[0]){
      $menu->selected = true;
    }
    $menu->data_child = $cek;
  }

@endphp
@foreach($menus as $key => $menu)
  @if($menu->data_child()->where('is_menu', 1)->count() > 0)
    <li class="nav-header" style="padding-top: 0px !important"></li>
    <li class="nav-item has-treeview @if($menu->selected) menu-open @endif">
      <a href="#" class="nav-link @if($menu->selected) active @endif">
        {!!$menu->icon!!}
        <p>{{$menu->name}}<i class="fas fa-angle-left right"></i></p>
      </a>
      <ul class="nav nav-treeview" style="background-color: #6a7373">
        {!!view('rendermenu')->with('parent_id', $menu->id)!!}
      </ul>
    </li>
  @else
    @can('view '.$menu->route)
      <li class="nav-item">
        <a href="{{URL::to('/'.$menu->route)}}" class="nav-link @if($menu->selected) active @endif">
          {!!$menu->icon!!}
          <p>{{$menu->name}}</p>
        </a>
      </li>
    @endcan
  @endif
@endforeach