@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Kas</h1>
@stop


@section('content')

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

	<div class="card">

        <div class="card-header border-0">
            <h3 class="card-title">Kas</h3>
            <div class="card-tools">
                @can('add kas')
                    <a href="{{URL::to('/kas/add')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></a>
                @endcan
                <a href="{{URL::to('/kas')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <select class="form-control" name="filter" id="filter">
                        <option value="">--Pilih Filter--</option>
                        <option value="code" @if($filter == 'code') selected="" @endif>Kode Kas</option>
                        <option value="name" @if($filter == 'name') selected="" @endif>Nama</option>
                        <option value="type" @if($filter == 'type') selected="" @endif>Tipe</option>
                        <option value="account_number" @if($filter == 'account_number') selected="" @endif>Nomor Akun</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="value" placeholder="Cari . . ." name="value" id="value" value="{{$value}}">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-default" onclick="searchData()"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>
        					#
        				</th>
                        <th>
                            Kode
                        </th>
                        <th>
                            Nomor Akun
                        </th>
                        <th>
                            Nama
                        </th>
                        <th>
                            Tipe
                        </th>
                        <th>
                            Saldo
                        </th>
                        <th>
                            Keterangan
                        </th>
                        <th width="180px">
                            Action
                        </th>
        			</tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
        					<tr>
        						<td>
        							{{($key+1) + (($page - 1) * $take)}}
        						</td>
                                <td>
                                    {{$dt->code}}
                                </td>
                                <td>
                                    {{$dt->account_number}}
                                </td>
                                <td>
                                    {{$dt->name}}
                                </td>
                                <td>
                                    {{$dt->type}}
                                </td>
                                <td>
                                    @if($dt->type == "hutang" && $dt->amount > 0)
                                        <font color="red">{{"(".number_format($dt->amount).")"}}</font>
                                    @else
                                        {{number_format($dt->amount)}}
                                    @endif
                                </td>
                                <td>
                                    {{$dt->description}}
                                </td>
                                <td>
                                    <div class="row">
                                        @can('update kas')
                                            @if($dt->type == "kas/bank")
                                                <div class="col-sm-4">
                                                    <button type="button" class="btn btn-success" onclick="addBalance({{$dt->id}}, '{{$dt->name}}', '{{number_format($dt->amount)}}')">
                                                        <i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                            @endif
                                        @endcan
                                        @if($dt->main == 0)
                                            @can('update kas')
                                            <div class="col-sm-4">
                                                <a href="{{URL::to('/kas/'.$dt->id)}}" class="btn btn-warning">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            @endcan
                                            @can('delete kas')
                                                <div class="col-sm-4">
                                                    <button type="button" class="btn btn-danger" onclick="beforeDelete('{{URL::to('/kas/destroye/'.$dt->id)}}')">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </div>
                                            @endcan
                                        @endif
                                    </div>
                                </td>
        					</tr>
        				@endforeach
        			@endif
        		</tbody>
        	</table>
        </div>

        <div class="card-footer">
        	<div class="float-left">
                <select class="form-control" onchange="searchData()" id="take_data" name="take_data">
                    @foreach(HelperController::$take as $key => $jumlah)
                        <option value="{{$jumlah}}" @if($take == $jumlah) selected="" @endif>{{$jumlah}}</option>
                    @endforeach
                </select>
                <label class="label-control">Of : {{$count}} Data</label>
        	</div>
        	<div class="float-right">
        		{!!$data->appends(['take' => $take, 'filter' => $filter, 'value' => $value])->links()!!}
        	</div>
        </div>

	</div>
@stop
<div id="addBalanceModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Saldo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="/kas/add/balance" method="POST" id="addBalanceForm">
                {{csrf_field()}}
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" value="" readonly="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Saldo</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="balance" value="" readonly="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="amount" class="col-sm-2 col-form-label">Jumlah</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="amount" id="amount" value="0" min="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="description" name="description" rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

<script type="text/javascript">

    addBalance = (id, name, balance) =>
    {
        $("#id").val(id);
        $("#name").val(name);
        $("#balance").val(balance);
        $("#amount").val(0);
        $("#addBalanceModal").modal('show');
    }

    // submitAddBalance = () => {
    //     $.ajax({
    //         url: "/kas/addbalance",
    //         type: "POST",
    //         data: {
    //             _token: $("input[name=_token]").val(),
    //             id: $("#id").val(),
    //             amount: $("#amount").val()
    //         },
    //         success: function(response){
    //             if(response.success)
    //         }
    //     });
    // }

</script>
