@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Tambah Kas</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/kas/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Kode</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('code'))?'is-invalid':''}}" name="code" id="code" value="{{old('code')}}">
                        @if($errors->has('code'))
                            <span id="code-error" class="error invalid-feedback">{{$errors->first('code')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="account_number" class="col-sm-2 col-form-label">Nomor Akun</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('account_number'))?'is-invalid':''}}" name="account_number" id="account_number" value="{{old('account_number')}}">
                        @if($errors->has('account_number'))
                            <span id="account_number-error" class="error invalid-feedback">{{$errors->first('account_number')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" id="name" value="{{old('name')}}">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="type" class="col-sm-2 col-form-label">Tipe</label>
                    <div class="col-sm-10">

                        <select name="type" id="type" class="form-control {{($errors->has('type'))?'is-invalid':''}}">
                            @foreach($types as $key => $type)
                                <option value="{{$key}}">{{$type}}</option>
                            @endforeach
                        </select>

                        @if($errors->has('type'))
                            <span id="type-error" class="error invalid-feedback">{{$errors->first('type')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="amount" class="col-sm-2 col-form-label">Saldo</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control {{($errors->has('amount'))?'is-invalid':''}}" name="amount" id="amount" value="0">
                        @if($errors->has('amount'))
                            <span id="amount-error" class="error invalid-feedback">{{$errors->first('amount')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" id="description" name="description" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/kas')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
