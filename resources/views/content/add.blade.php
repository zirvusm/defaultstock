@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Add Content</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/content/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="parent_id" class="col-sm-2 col-form-label">Category</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('parent_id'))?'is-invalid':''}}" name="parent_id" id="parent_id">
                            @foreach($cat as $key => $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('parent_id'))
                            <span id="parent_id-error" class="error invalid-feedback">{{$errors->first('parent_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" id="name">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                @if(env('USE_LANG'))
                    <div class="form-group row">
                        <label for="lang" class="col-sm-2 col-form-label">Lang</label>
                        <div class="col-sm-10">
                            <select class="form-control {{($errors->has('lang'))?'is-invalid':''}}" name="lang" id="lang">
                                <option value="en">En</option>
                                <option value="id">ID</option>
                                <option value="cn">Cn</option>
                            </select>
                            @if($errors->has('lang'))
                                <span id="lang-error" class="error invalid-feedback">{{$errors->first('lang')}}</span>
                            @endif
                        </div>
                    </div>
                @endif
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Date</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date">
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="image" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" accept=".jpg,.jpeg,.png,.gif,.svg" class="custom-file-input" name="image" id="image" value="">
                                <label class="custom-file-label" for="image" id="textImage">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Upload</span>
                            </div>
                        </div>
                        @if($errors->has('image'))
                            <span id="image-error" class="error invalid-feedback">{{$errors->first('image')}}</span>
                        @endif
                    </div>
                </div>
                <img src="" id="preview" width="30%">
                <div class="col-md-12">
                    &nbsp;
                </div>
                <div class="form-group row">
                    <label for="simple" class="col-sm-2 col-form-label">Simple Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('simple'))?'is-invalid':''}}" name="simple" id="simple" rows="4"></textarea>
                        @if($errors->has('simple'))
                            <span id="simple-error" class="error invalid-feedback">{{$errors->first('simple')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control texteditor" name="description" id="description" rows="4"></textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
                
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/content')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        $('#image').change(function(){
            $('#textImage').html(this.value);
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop