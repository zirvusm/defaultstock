@extends('app')
@extends('modals.kas')
@extends('modals.addstock')
@extends('modals.addcost')



@section('module_name')
    <h1 class="m-0 text-dark">Ubah Penjualan</h1>
@stop
@php
    $total = 0;
    $count = 0;
    $total_cost = 0;
    $count_cost = 0;
    $grand_total = 0;
@endphp


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/selling/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label for="kas_id" class="col-sm-2 col-form-label">Kas</label>
                    <div class="input-group col-sm-10">
                        <input type="text" class="form-control {{($errors->has('kas_id'))?'is-invalid':''}}" name="kas_name" id="kas_name" readonly="" value="{{$data->data_kas->name ?? "-"}}">
                        @if($data->status == 0)
                            <input type="hidden" name="kas_id" id="kas_id" value="{{$data->kas_id}}">
                            <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#kasModal"><i class="fa fa-folder-open"></i></button>
                        @endif

                        @if($errors->has('kas_id'))
                            <span id="kas_id-error" class="error invalid-feedback">{{$errors->first('kas_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="customer_id" class="col-sm-2 col-form-label">Customer</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="customer_id" id="customer_id" value="{{$data->customer_id}}">
                        <input type="text" class="form-control {{($errors->has('customer_id'))?'is-invalid':''}}" name="customer_name" id="customer_name" readonly="" value="{{$data->data_customer->name ?? "-"}}">

                        @if($data->po_invoice == null)
                            <button class="btn btn-default btn-flat" id="btnMdlCustomer" type="button" data-toggle="modal" data-target="#customerModal"><i class="fa fa-folder-open"></i></button>
                        @endif

                        @if($errors->has('customer_id'))
                            <span id="customer_id-error" class="error invalid-feedback">{{$errors->first('customer_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sales_id" class="col-sm-2 col-form-label">Sales</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="sales_id" id="sales_id" value="{{$data->sales_id}}">
                        <input type="text" class="form-control {{($errors->has('sales_id'))?'is-invalid':''}}" name="sales_name" id="sales_name" readonly="" value="{{$data->data_sales->name ?? "-"}}">
                        @if($data->po_invoice == null)
                            <button class="btn btn-default btn-flat" id="btnMdlSales" type="button" data-toggle="modal" data-target="#salesModal"><i class="fa fa-folder-open"></i></button>
                        @endif
                        @if($errors->has('sales_id'))
                            <span id="sales_id-error" class="error invalid-feedback">{{$errors->first('sales_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nomor Pesanan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"value="{{$data->po_invoice}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nomor Faktur</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"value="{{$data->invoice}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{$data->date}}">
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        @if($data->status == 0)
                            <select name="status" id="status" class="form-control {{($errors->has('status'))?'is-invalid':''}}">
                                <option value="0">Menunggu</option>
                                @can('approve purchase')
                                    <option value="1" @if($data->status == 1) selected="" @endif>
                                        Di Setujui
                                    </option>
                                @endcan
                                @can('reject purchase')
                                    <option value="2" @if($data->status == 2) selected="" @endif>
                                        Di Batalkan
                                    </option>
                                @endcan
                            </select>
                        @else
                            <label_v1 class="form-control">
                                @if($data->status == 0)
                                    <b><font color="warning">Menunggu</font></b>
                                @elseif($data->status == 1)
                                    Di <b><font color="success">Setujui</font></b> Oleh <b>{{$data->audit_by}}</b> Pada <b>{{$data->audit_at}}</b>
                                @elseif($data->status == 2)
                                    Di <b><font color="danger">Batalkan</font></b> Oleh <b>{{$data->audit_by}}</b> Pada <b>{{$data->audit_at}}</b>
                                @endif
                            </label_v1>
                        @endif
                        @if($errors->has('status'))
                            <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                @if($data->status == 0)
                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addstockModal">
                        <i class="fas fa-plus"> Tambah Barang</i>
                    </button>
                @endif
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Total</th>
                        @if($data->status == 0)
                        <th>Action</th>
                        @endif
                    </thead>
                    <tbody id="tbl_products">
                        @foreach($data->data_detail as $key => $detail)
                            @php
                                $hs = $detail->qty * $detail->amount;
                                $total += $hs;
                                $count++;
                                $grand_total += $hs;
                            @endphp
                            <tr id="tr_add_product_{{$key}}">
                                <td>
                                    {{$detail->data_product->code}}
                                </td>
                                <td>
                                    <input type="hidden" name="ids[]" value="{{$detail->data_product->id}}">
                                    {{$detail->data_product->name}}
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="qty_{{$key}}" name="qtys[]" value="{{$detail->qty}}" min="0" onchange="countTotal({{$key}})" @if($data->status == 1) readonly="" @endif>
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="price_{{$key}}" name="prices[]" value="{{$detail->amount}}" min="0" step="0.1" onchange="countTotal({{$key}})" @if($data->status == 1) readonly="" @endif>
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="totals_{{$key}}" readonly="" value="{{number_format($hs)}}">
                                </td>
                                @if($data->status == 0)
                                <td>
                                    <button type="button" class="btn btn-danger" onclick="removeAddProduct({{$key}})"><i class="fas fa-trash"></i></button>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <td>
                                <input type="text" class="form-control" id="total_all" value="{{number_format($total)}}" readonly="">
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                @if($data->status == 0)
                <button type="button" class="btn btn-warning pull-right" onclick="loadCost()">
                    <i class="fas fa-plus"> Biaya Tambahan</i>
                </button>
                @endif
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        @if($data->status == 0)
                        <th>Action</th>
                        @endif
                    </thead>
                    <tbody id="tbl_cost">
                        @foreach($data->data_cost()->where('gr', $gr)->get() as $key_cost => $cost)
                            @php
                                if($cost->type == 0)
                                {
                                    $hs_cost = $cost->amount;
                                }else{
                                    $hs_cost = $total * $cost->amount / 100;
                                }
                                $total_cost += $hs_cost;
                                $grand_total += $hs_cost;
                                $count_cost++;
                            @endphp
                            <tr id="tr_add_cost_{{$key_cost}}">
                                <td>
                                    {{$cost->code}}
                                </td>
                                <td>
                                    <input type="hidden" name="id_costs[]" value="{{$cost->data_parent->id}}">
                                    {{$cost->name}}
                                </td>
                                <td>
                                    @if($cost->type == 0)
                                        <input type="number" class="form-control" id="amount_cost_{{$key_cost}}" name="amounts[]" value="{{$cost->amount}}" data-type="0" min="0" step="1" onchange="countCost_total({{$cost->amount}}, {{$cost->type}})" @if($data->status == 1) readonly="" @endif>
                                    @else
                                        <input type="number" class="form-control" id="amount_cost_{{$key_cost}}" name="amounts[]" value="{{$cost->amount}}" data-type="1" min="0" max="100" step="0.1" onchange="countCost_total({{$cost->amount}}, {{$cost->type}})" @if($data->status == 1) readonly="" @endif>
                                    @endif
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="totals_cost_{{$key_cost}}" readonly="" value="{{number_format($hs_cost)}}">
                                </td>
                                @if($data->status == 0)
                                <td>
                                    <button type="button" class="btn btn-danger" onclick="removeCost({{$key_cost}})"><i class="fas fa-trash"></i></button>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">Total</th>
                            <td>
                                <input type="text" class="form-control" name="total_cost" id="total_cost" value="{{number_format($total_cost)}}" readonly="">
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="grand_total" class="col-sm-2 col-form-label">Grand Total</label>
                    <div class="col-sm-10">
                        <input type="hidden" class="form-control" name="grand_totalfix" id="grand_totalfix" value="{{$grand_total}}" readonly="">
                        <input type="text" class="form-control" name="grand_total" id="grand_total" value="{{number_format($grand_total)}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" id="description" name="description" rows="4" @if($data->status == 1) disabled="" @endif>{{$data->description}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/selling')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">


        afterselectAddStock = (id, code, name, hpp, qty) => {
            let customer_id = $("#customer_id1").val();
            $.ajax({
                url: "{{URL::to("selling/history")}}/"+id+"/"+customer_id,
                type: "GET",
                success: function(response){
                    if(response.id != undefined){
                        hpp = response.amount;
                    }
                    let ht = '';
                    countAddStock += 1;
                    ht += '<tr id="tr_add_stock_'+countAddStock+'">';
                    ht += '<td><input type="hidden" name="stockcodes[]" value="'+code+'">'+code+'</td>';
                    ht += '<td><input type="hidden" name="ids[]" value="'+id+'"><input type="hidden" name="stocknames[]" value="'+name+'">'+name+'</td>';
                    ht += '<td>';
                    ht += '<input type="number" class="form-control" id="qty_'+countAddStock+'" name="qtys[]" value="0" min="0" onchange="countTotal('+countAddStock+')">';
                    ht += '</td>';
                    ht += '<td>';
                    ht += '<input type="number" class="form-control" id="price_'+countAddStock+'" name="prices[]" value="'+hpp+'" min="0" step="0.1" onchange="countTotal('+countAddStock+')">';
                    ht += '</td>';
                    ht += '<td>';
                    ht += '<input type="text" class="form-control" id="totals_'+countAddStock+'" readonly="">';
                    ht += '</td>';
                    ht += '<td><button type="button" class="btn btn-danger" onclick="removeAddProduct('+countAddStock+')"><i class="fas fa-trash"></i></button></td>';
                    ht += '</tr>';
                    $("#tbl_products").append(ht);
                    $("#addstockModal").modal('hide');
                }
            });
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
