@foreach($data_goodsin as $key => $detail)
	<tr class="modalFilter" data-code="{{strtolower($detail['code'])}}" data-name="{{strtolower($detail['name'])}}">
		<td>{{$detail['invoice']}}</td>
		<td>{{$detail['code']}}</td>
		<td>{{$detail['name']}}</td>
		<td>{{number_format($detail['qty'])}}</td>
		<td>{{number_format($detail['amount'])}}</td>
		<td>{{number_format($detail['qty'] * $detail['amount'])}}</td>
		<td id="btnGoods_{{$detail['product_id']}}">
			@if($detail['in_arr'])
				<button type="button" class="btn btn-warning" 
					onclick="unSelectGoods({{$detail['product_id']}}, '{{$detail['code']}}', '{{$detail['name']}}', {{$detail['qty']}}, {{$detail['amount']}})"
				>Buang</button>
			@else
				<button type="button" class="btn btn-default" 
					onclick="selectGoods({{$detail['product_id']}}, '{{$detail['code']}}', '{{$detail['name']}}', {{$detail['qty']}}, {{$detail['amount']}})"
				>Pilih</button>
			@endif
		</td>
	</tr>
@endforeach
