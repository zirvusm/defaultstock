@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Tanda Terima</h1>
@stop


@section('content')

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

	<div class="card">

        <div class="card-header border-0">
            <h3 class="card-title">Tanda Terima</h3>
            <div class="card-tools">
                @can('add receipt')
                    <a href="{{URL::to('/receipt/add')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></a>
                @endcan
                <a href="{{URL::to('/receipt')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <select class="form-control" name="filter" id="filter">
                        <option value="">--Pilih Filter--</option>
                        <option value="po_invoice" @if($filter == 'po_invoice') selected="" @endif>Nomor Pesanan</option>
                        <option value="invoice" @if($filter == 'invoice') selected="" @endif>Nomor Faktur</option>
                        <option value="date" @if($filter == 'date') selected="" @endif>Tanggal</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="value" placeholder="Cari . . ." name="value" id="value" value="{{$value}}">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-default" onclick="searchDatareceipt()"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>
        					#
        				</th>
                        <th>
                            Nomor Tanda Terima
                        </th>
                        <th>
                            Nama Sales
                        </th>
                        <th>
                            Tanggal
                        </th>
                        <th>
                            Grand Total
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Oleh / Pada
                        </th>
                        <th>
                            Dikirim Pada
                        </th>
                        <th width="160px">
                            Action
                        </th>
        			</tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
        					<tr>
                                <td>
                                    {{($key+1) + (($page - 1) * $take)}}
                                </td>
                                <td>
                                    {{$dt->receipt}}
                                </td>
                                <td>
                                    {{$dt->data_sales->name}}
                                </td>
                                <td>
                                    {{$dt->date}}
                                </td>
                                <td>
                                    {{number_format($dt->amount)}}
                                </td>
                                <td>
                                    @if($dt->status == 0)
                                        <font color="orange">Menunggu</font>
                                    @elseif($dt->status == 1)
                                        <font color="success">
                                            Di Setujui
                                        </font>
                                    @elseif($dt->status == 2)
                                        <font color="red">Di Batalkan</font>
                                    @endif
                                </td>
                                <td align="center">
                                    {{$dt->audit_by ?? '-'}}<br>{{$dt->audit_at ?? '-'}}
                                </td>
                                <td>
                                    {{$dt->delivery_at}}
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <a href="{{URL::to('/receipt/detail/'.$dt->id)}}" class="btn btn-primary">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </div>
                                        @if($dt->status == 0)
                                            @can('update receipt')
                                            <div class="col-sm-4">
                                                <a href="{{URL::to('/receipt/'.$dt->id)}}" class="btn btn-warning">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            @endcan
                                            @can('delete receipt')
                                            <div class="col-sm-4">
                                                <button type="button" class="btn btn-danger" onclick="beforeDelete('{{URL::to('/receipt/destroye/'.$dt->id)}}')">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </div>
                                            @endcan
                                        @elseif($dt->status == 1)
                                            <div class="col-sm-4">
                                                <a href="{{URL::to('/receipt/print/'.$dt->id)}}" target="_blank" class="btn btn-warning">
                                                    <i class="fas fa-print"></i>
                                                </a>
                                            </div>
                                            <div class="col-sm-4">
                                                <a href="{{URL::to('/receipt/undo/'.$dt->id)}}" class="btn btn-danger">
                                                    <i class="fas fa-undo"></i>
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                </td>
        					</tr>
        				@endforeach
        			@endif
        		</tbody>
        	</table>
        </div>

        <div class="card-footer">
        	<div class="float-left">
                <select class="form-control" onchange="searchData()" id="take_data" name="take_data">
                    @foreach(HelperController::$take as $key => $jumlah)
                        <option value="{{$jumlah}}" @if($take == $jumlah) selected="" @endif>{{$jumlah}}</option>
                    @endforeach
                </select>
                <label class="label-control">Of : {{$count}} Data</label>
        	</div>
        	<div class="float-right">
        		{!!$data->appends(['take' => $take, 'filter' => $filter, 'value' => $value])->links()!!}
        	</div>
        </div>

	</div>
@stop

<script type="text/javascript">


    searchDatareceipt = () => {

        let url = window.location.origin + window.location.pathname;
        let page = urlParam('page') ? urlParam('page') : 1;
        let take = $('#take_data').val();
        let filter = $('#filter').val();
        let value = $('#value').val();

        let query = '';

        if(page != ''){
            if(query != '') query += '&';
            query += 'page='+page;
        }
        if(take != ''){
            if(query != '') query += '&';
            query += 'take='+take;
        }
        if(filter != '' && filter != undefined){
            if(query != '') query += '&';
            query += 'filter='+filter;
        }
        if(value != '' && value != undefined){
            if(query != '') query += '&';
            query += 'value='+value;
        }

        location.replace(url+'?'+query);
    }
</script>
