@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Detail Tanda Terima</h1>
@stop
@php
    $total = 0;
    $count = 0;
    $total_cost = 0;
    $count_cost = 0;
    $grand_total = 0;
@endphp



@section('content')

<div class="col-md-12">
    <div class="card">
        <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label for="sales_id" class="col-sm-2 col-form-label">Sales</label>
                    <div class="input-group col-sm-10">
                        <input type="text" class="form-control {{($errors->has('sales_id'))?'is-invalid':''}}" name="sales_name" id="sales_name" readonly="" value="{{$data->data_sales->name ?? "-"}}">
                        @if($data->status == 0)
                        <input type="hidden" name="sales_id" id="sales_id" value="{{$data->sales_id}}">
                        @endif

                        @if($errors->has('sales_id'))
                        <span id="sales_id-error" class="error invalid-feedback">{{$errors->first('sales_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nomor Tanda Terima</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"value="{{$data->receipt}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{$data->date}}" readonly="">
                        @if($errors->has('date'))
                        <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <label_v1 class="form-control">
                            @if($data->status == 0)
                            <b><font color="warning">Menunggu</font></b>
                            @elseif($data->status == 1)
                            Di <b><font color="success">Setujui</font></b> Oleh <b>{{$data->audit_by}}</b> Pada <b>{{$data->audit_at}}</b>
                            @elseif($data->status == 2)
                            Di <b><font color="danger">Batalkan</font></b> Oleh <b>{{$data->audit_by}}</b> Pada <b>{{$data->audit_at}}</b>
                            @endif
                        </label_v1>
                        @if($errors->has('status'))
                        <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Nomor Faktur</th>
                        <th>Customer</th>
                        <th>Tanggal</th>
                        <th>Jatuh Tempo</th>
                        <th>Grand Total</th>
                    </thead>
                    <tbody id="tbl_transactions">
                        @foreach($data->data_detail as $key => $detail)
                            @php
                                $hs = $detail->data_transaction->amount;
                                $total += $hs;
                                $count++;
                            @endphp
                            <tr id="tr_add_product_{{$key}}">
                                <td>
                                    <input type="hidden" name="receipts[]" value="{{$detail->data_transaction->invoice}}">
                                    {{$detail->data_transaction->invoice}}
                                </td>
                                <td>
                                    <input type="hidden" name="customernames[]" value="{{$detail->data_transaction->data_customer->name}}">
                                    <input type="hidden" name="ids[]" value="{{$detail->id}}">
                                    {{$detail->data_transaction->data_customer->name}}
                                </td>
                                <td>
                                    <input type="hidden" class="form-control" id="date_{{$key}}" name="dates[]" value="{{$detail->data_transaction->date}}">{{$detail->data_transaction->date}}
                                </td>
                                <td>
                                    <input type="hidden" class="form-control" id="due_date_{{$key}}" name="due_dates[]" value="{{$detail->data_transaction->due_date}}">{{$detail->data_transaction->due_date}}
                                </td>
                                <td>
                                    <input type="hidden" class="form-control" id="grand_total_{{$key}}" name="grand_total[]" readonly="" value="{{$hs}}">{{$hs}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <td>
                                <input type="text" class="form-control" id="total_all" value="{{number_format($total)}}" readonly="">
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" id="description" name="description" rows="4" disabled="">{{$data->description}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
        <div class="card-footer">
            <div class="float-right">
                <a href="{{URL::to('/receipt')}}" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
