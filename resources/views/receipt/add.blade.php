@extends('app')
@extends('modals.addtransaction')
@extends('modals.sales')


@section('module_name')
    <h1 class="m-0 text-dark">Tambah Tanda Terima</h1>
@stop
@php
    $total = 0;
    $count = 0;
    $total_cost = 0;
    $count_cost = 0;
    $grand_total = 0;
    $receipt = "I-1";

    if($data_receipt != null){
        $temp = explode("-", $data_receipt->receipt ?? "");
        $next = (isset($temp[1])) ? $temp[1] + 1 : 1;
        $receipt = "I-".$next;
    }
@endphp


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/receipt/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="sales_id" class="col-sm-2 col-form-label">Sales</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="sales_id" id="sales_id" value="{{old('sales_id')}}">
                        <input type="text" class="form-control {{($errors->has('sales_id'))?'is-invalid':''}}" name="sales_name" id="sales_name" readonly="" value="{{old('sales_name')}}">

                        <button class="btn btn-default btn-flat" id="btnMdlSales" type="button" data-toggle="modal" data-target="#salesModal"><i class="fa fa-folder-open"></i></button>

                        @if($errors->has('sales_id'))
                            <span id="sales_id-error" class="error invalid-feedback">{{$errors->first('sales_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="receipt" class="col-sm-2 col-form-label">Nomor Tanda Terima</label>
                    <div class="col-sm-10">
                        @if($data_receipt != null)
                            <code>Nomor Tanda Terima sebelumnya : <b>{{$data_receipt->receipt}}</b></code>
                        @endif
                        <input type="text" class="form-control {{($errors->has('receipt'))?'is-invalid':''}}" name="receipt" id="receipt" value="{{$receipt}}" readonly>
                        @if($errors->has('receipt'))
                            <span id="receipt-error" class="error invalid-feedback">{{$errors->first('receipt')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{date('Y-m-d')}}">
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addtransactionModal">
                    <i class="fas fa-plus"> Tambah Transaksi</i>
                </button>
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Nomor Faktur</th>
                        <th>Customer</th>
                        <th>Tanggal</th>
                        <th>Jatuh Tempo</th>
                        <th>Grand Total</th>
                        <th>Action</th>
                    </thead>
                    <tbody id="tbl_transactions">
                        @if(old('ids') != null)
                            @foreach(old('ids') as $key => $value)
                                @php
                                    $hs = old('grand_total')[$key];
                                    $total += $hs;
                                    $count++;
                                @endphp
                                <tr id="tr_add_transaction_{{$key}}">
                                    <td>
                                        <input type="hidden" name="receipts[]" value="{{old('receipts')[$key]}}">
                                        {{old('receipts')[$key]}}
                                    </td>
                                    <td>
                                        <input type="hidden" name="customernames[]" value="{{old('customernames')[$key]}}">
                                        <input type="hidden" name="ids[]" value="{{$value}}">
                                        {{old('customernames')[$key]}}
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="date_{{$key}}" name="dates[]" value="{{old('dates')[$key]}}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="due_date_{{$key}}" name="due_dates[]" value="{{old('due_dates')[$key]}}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="grand_total_{{$key}}" name="grand_total[]" readonly="" value="{{old(number_format($hs))}}">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger" onclick="removeAddTransaction({{$key}})"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <td>
                                <input type="text" class="form-control" name="total_all" id="total_all" value="{{old(number_format($total))}}" readonly="">
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" id="description" name="description" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/receipt')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
