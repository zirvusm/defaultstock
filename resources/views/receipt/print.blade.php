<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body{
            font-family:'Gill Sans', 'Gill Sans MT', 'Calibri', 'Trebuchet MS', sans-serif;
            color:#333;
            text-align:left;
            font-size:18px;
            margin:0;
            width:100%;
            height:auto;
        }
        .container{
            margin:0 auto;
            margin-top:35px;
            padding:40px;
            width:1000px;
            height:auto;
            background-color:#fff;
        }
        #logo{
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: auto;
            height: 60px;
        }
        caption{
            font-size:28px;
            margin-bottom:18px;
            text-align:left;
        }
        #top{
            margin-top: 10px;
            border-collapse:collapse;
            margin:0 auto;
            width:990px;
        }
        #top tr{
            width:247.5px;
        }
        #top th{
            width:247.5px;
        }
        #middle{
            margin-top: 20px;
            border:1px solid #333;
            border-collapse:collapse;
            width:990px;
        }
        #middle td{
            padding:8px;
            border:1px solid #333;
        }
        #middle tr{
            padding:12px;
            border:1px solid #333;
        }
        #middle th{
            background-color: #f0f0f0;
            padding:12px;
            border:1px solid #333;
        }
        #bottom{
            margin-top: 100px;
            display: flex;
            justify-content: center;
            text-align: center;
        }
        #bottom td{
            padding:8px;
            width:247.5px;
            height: 200px;
        }
        #bottom tr{
            padding:8px;
            width:247.5px;
        }
        #bottom th{
            padding:8px;
            width:247.5px;
        }
        #intable{
            margin-top: 10px;
            border-collapse:collapse;
            width:500px;
        }
        h4, p{
            margin:0px;
        }
    </style>
</head>
<body>
    @php
        $total = 0;
        $total_cost = 0;
    @endphp
    <div class="container">
        <img id="logo" src="{{$setting['logo']}}" alt="Logo" /><br>
        <table id="top">
            <tbody>
                <tr>
                    <th colspan="3">RECEIPT</th>
                    <th colspan="1">{{$data->delivery_at ?? '-'}}</th>
                </tr>
            </tbody>
        </table>
        <table id="intable">
            <tbody>
                <tr>
                    <th>No. Tanda Terima</th>
                    <th>{{$data->invoice ?? '-'}}</th>
                </tr>
                <tr>
                    <th>Sales</th>
                    <th>{{$data->data_sales->name ?? '-'}}</th>
                </tr>
                <tr>
                    <th>Tanggal</th>
                    <th>{{$data->date ?? '-'}}</th>
                </tr>
                <tr>
                    <th>Keterangan</th>
                    <th>{{$data->description ?? '-'}}</th>
                </tr>
            </tbody>
        </table>
        <table id="middle">
            <tbody>
                <tr>
                    <th>No.</th>
                    <th>Nomor Faktur</th>
                    <th>Customer</th>
                    <th>Tanggal</th>
                    <th>Jatuh Tempo</th>
                    <th>Grand Total</th>
                </tr>
                @foreach($data->data_detail as $key => $detail)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$detail->data_transaction->invoice ?? '-'}}</td>
                        <td>{{$detail->data_transaction->data_customer->name ?? '-'}} ({{$detail->data_transaction->data_customer->phone_number ?? '-'}})</td>
                        <td>{{$detail->data_transaction->date ?? '-'}}</td>
                        <td>{{$detail->data_transaction->due_date ?? '-'}}</td>
                        <td>{{number_format($detail->data_transaction->amount) ?? '-'}}</td>
                    </tr>
                @endforeach
                <?php
                    for ($x = $data->data_detail->count()+1; $x <= 10; $x++) {
                        echo "<tr><td> $x </td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>";
                    }
                ?>
            </tbody>
        </table>
        <table id="intable" style="margin-right: 12px;margin-left: auto;text-align:right;">
            <tbody>
                <tr>
                    <th>Grand Total</th>
                    <th>{{number_format($data->amount) ?? '0'}}</th>
                </tr>
                <tr>
                    <th colspan="2">=================</th>
                </tr>
            </tbody>
        </table>
        <table id="bottom">
            <tr>
                <th>
                    Pengirim,
                </th>
                <th>
                    Penerima,
                </th>
                <th>
                    Yang Mengetahui,
                </th>
            </tr>
            <tr>
                <td>
                    _______________
                </td>
                <td>
                    _______________
                </td>
                <td>
                    _______________
                </td>
            </tr>
        </table>
        <p>NB:<br>Barang telah diterima dalam jumlah yang benar dan kondisi yang baik.<br>Pembayaran Cek/Giro dianggap sah apabila telah dicairkan/diuangkan.</p>
    </div>
</body>
</html>
<script src="{{asset('/template/plugins/jquery/jquery.min.js')}}"></script>
<script>

$( document ).ready(function() {
    window.print();
});

</script>
