@extends('app')
@extends('modals.kas')
@extends('modals.supplier')
@extends('modals.addproduct')
@extends('modals.addcost')


@section('module_name')
    <h1 class="m-0 text-dark">Tambah Pembelian</h1>
@stop
@php
    $total = 0;
    $count = 0;
    $total_cost = 0;
    $count_cost = 0;
    $grand_total = 0;
    $invoice = "B-1";

    if($data_purchase != null){
        $temp = explode("-", $data_purchase->invoice ?? "");
        $next = (isset($temp[1])) ? $temp[1] + 1 : 1;
        $invoice = "B-".$next;
    }

@endphp


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/purchase/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="kas_id" class="col-sm-2 col-form-label">Kas</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="kas_id" id="kas_id" value="{{old('kas_id')}}">
                        <input type="text" class="form-control {{($errors->has('kas_id'))?'is-invalid':''}}" name="kas_name" id="kas_name" readonly="" value="{{old('kas_name')}}">

                        <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#kasModal"><i class="fa fa-folder-open"></i></button>

                        @if($errors->has('kas_id'))
                            <span id="kas_id-error" class="error invalid-feedback">{{$errors->first('kas_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="supplier_id" class="col-sm-2 col-form-label">Supplier</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="supplier_id" id="supplier_id" value="{{old('supplier_id')}}">
                        <input type="text" class="form-control {{($errors->has('supplier_id'))?'is-invalid':''}}" name="supplier_name" id="supplier_name" readonly="" value="{{old('supplier_name')}}">

                        <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#supplierModal"><i class="fa fa-folder-open"></i></button>

                        @if($errors->has('supplier_id'))
                            <span id="supplier_id-error" class="error invalid-feedback">{{$errors->first('supplier_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gudang_id" class="col-sm-2 col-form-label">Gudang</label>
                    <div class="col-sm-10">
                        <select name="gudang_id" id="gudang_id" class="form-control {{($errors->has('gudang_id'))?'is-invalid':''}}">
                            @foreach($data_gudang as $key => $value)
                                <option value="{{$value->id}}"
                                    @if($value->id == old('gudang_id'))
                                        selected=""
                                    @endif
                                >{{$value->name}} ( {{$value->name}} )</option>
                            @endforeach
                        </select>
                        @if($errors->has('gudang_id'))
                            <span id="gudang_id-error" class="error invalid-feedback">{{$errors->first('gudang_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="invoice" class="col-sm-2 col-form-label">Nomor Faktur</label>
                    <div class="col-sm-10">
                        @if($data_purchase != null)
                            <code>Nomor Faktur sebelumnya : <b>{{$data_purchase->invoice}}</b></code>
                        @endif
                        <input type="text" class="form-control {{($errors->has('invoice'))?'is-invalid':''}}" name="invoice" id="invoice" value="{{$invoice}}" readonly>
                        @if($errors->has('invoice'))
                            <span id="invoice-error" class="error invalid-feedback">{{$errors->first('invoice')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{date('Y-m-d')}}">
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addproductModal">
                    <i class="fas fa-plus"> Tambah Barang</i>
                </button>
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Total</th>
                        <th>Action</th>
                    </thead>
                    <tbody id="tbl_products">
                        @if(old('ids') != null)
                            @foreach(old('ids') as $key => $value)
                                @php
                                    $hs = old('qtys')[$key] * old('prices')[$key];
                                    $total += $hs;
                                    $count++;
                                @endphp
                                <tr id="tr_add_product_{{$key}}">
                                    <td>
                                        <input type="hidden" name="productcodes[]" value="{{old('productcodes')[$key]}}">
                                        {{old('productcodes')[$key]}}
                                    </td>
                                    <td>
                                        <input type="hidden" name="productnames[]" value="{{old('productnames')[$key]}}">
                                        <input type="hidden" name="ids[]" value="{{$value}}">
                                        {{old('productnames')[$key]}}
                                    </td>
                                    <td>
                                        <input type="number" class="form-control" id="qty_{{$key}}" name="qtys[]" value="{{old('qtys')[$key]}}" min="0" onchange="countTotal({{$key}})">
                                    </td>
                                    <td>
                                        <input type="number" class="form-control" id="price_{{$key}}" name="prices[]" value="{{old('prices')[$key]}}" min="0" step="0.1" onchange="countTotal({{$key}})">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="totals_{{$key}}" readonly="" value="{{number_format($hs)}}">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger" onclick="removeAddProduct({{$key}})"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <td>
                                <input type="text" class="form-control" name="total_all" id="total_all" value="{{number_format($total)}}" readonly="">
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                <button type="button" class="btn btn-warning pull-right" onclick="loadCost()">
                    <i class="fas fa-plus"> Biaya Tambahan</i>
                </button>
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Action</th>
                    </thead>
                    <tbody id="tbl_cost">
                        @if(old('id_costs') != null)
                            @foreach(old('id_costs') as $key_cost => $value_cost)
                                @php
                                    if(old('types')[$key_cost] == 0)
                                    {
                                        $hs_cost = old('amounts')[$key_cost];
                                    }else{
                                        $hs_cost = $total * old('amounts')[$key_cost] / 100;
                                    }
                                    $total_cost += $hs_cost;
                                    $count_cost++;
                                @endphp
                                <tr id="tr_add_cost_{{$key_cost}}">
                                    <td>
                                        <input type="hidden" name="costcodes[]" value="{{old('costcodes')[$key_cost]}}">
                                        {{old('costcodes')[$key_cost]}}
                                    </td>
                                    <td>
                                        <input type="hidden" name="costnames[]" value="{{old('costnames')[$key_cost]}}">
                                        <input type="hidden" name="id_costs[]" value="{{$value_cost}}">
                                        {{old('costnames')[$key_cost]}}
                                    </td>
                                    <td>
                                        <input type="hidden" name="types[]" value="{{old('types')[$key_cost]}}">
                                        @if(old('types')[$key_cost] == 0)
                                            <input type="number" class="form-control" id="amount_cost_{{$key_cost}}" name="amounts[]" value="{{old('amounts')[$key_cost]}}" data-type="0" min="0" step="1" onchange="countCost_total({{old('amounts')[$key_cost]}}, {{old('types')[$key_cost]}})">
                                        @else
                                            <input type="number" class="form-control" id="amount_cost_{{$key_cost}}" name="amounts[]" value="{{old('amounts')[$key_cost]}}" data-type="1" min="0" max="100" step="0.1" onchange="countCost_total({{old('amounts')[$key_cost]}}, {{old('types')[$key_cost]}})">
                                        @endif
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="totals_cost_{{$key_cost}}" readonly="" value="{{number_format($hs_cost)}}">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger" onclick="removeCost({{$key_cost}})"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">Total</th>
                            <td>
                                <input type="text" class="form-control" name="total_cost" id="total_cost" value="{{number_format($total_cost)}}" readonly="">
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="grand_total" class="col-sm-2 col-form-label">Grand Total</label>
                    <div class="col-sm-10">
                        <input type="hidden" name="grand_totalfix" id="grand_totalfix" value="{{number_format($grand_total)}}" readonly="">
                        <input type="text" class="form-control" name="grand_total" id="grand_total" value="{{number_format($grand_total)}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" id="description" name="description" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/purchase')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
