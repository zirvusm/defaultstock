@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Detail Penjualan</h1>
@stop
@php
    $total = 0;
    $count = 0;
    $total_cost = 0;
    $count_cost = 0;
    $grand_total = 0;
@endphp



@section('content')

<div class="col-md-12">
    <div class="card">
        <input type="hidden" name="id" id="id" value="{{$data->id}}">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kas</label>
                <div class="input-group col-sm-10">
                    <input type="text" class="form-control" readonly="" value="{{$data->data_kas->name ?? "-"}}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Supplier</label>
                <div class="input-group col-sm-10">
                    <input type="text" class="form-control" readonly="" value="{{$data->data_supplier->name ?? "-"}}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Gudang</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$data->data_gudang->name ?? "-"}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nomor Faktur</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$data->invoice}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Tanggal</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" value="{{$data->date}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    <label_v1 class="form-control">
                        @if($data->status == 0)
                            <b><font color="warning">Menunggu</font></b>
                        @elseif($data->status == 1)
                            Di <b><font color="success">Setujui</font></b> Oleh <b>{{$data->audit_by}}</b> Pada <b>{{$data->audit_at}}</b>
                        @elseif($data->status == 2)
                            Di <b><font color="danger">Batalkan</font></b> Oleh <b>{{$data->audit_by}}</b> Pada <b>{{$data->audit_at}}</b>
                        @endif
                    </label_v1>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
            <table class="table table-bordered table-hover">
                <thead>
                    <th>Kode Produk</th>
                    <th>Nama Produk</th>
                    <th>Qty</th>
                    <th>Harga</th>
                    <th>Total</th>
                </thead>
                <tbody id="tbl_products">
                    @foreach($data->data_detail as $key => $detail)
                        @php
                            $hs = $detail->qty * $detail->amount;
                            $total += $hs;
                            $grand_total += $hs;
                            $count++;
                        @endphp
                        <tr id="tr_add_product_{{$key}}">
                            <td>
                                {{$detail->data_product->code}}
                            </td>
                            <td>
                                <input type="hidden" name="ids[]" value="{{$detail->data_product->id}}">
                                {{$detail->data_product->name}}
                            </td>
                            <td>
                                <input type="text" class="form-control" id="qty_{{$key}}" name="qtys[]" value="{{number_format($detail->qty)}}" readonly="">
                            </td>
                            <td>
                                <input type="text" class="form-control" id="amount_{{$key}}" name="amounts[]" value="{{number_format($detail->amount)}}" readonly="">
                            </td>
                            <td>
                                <input type="text" class="form-control" id="totals_{{$key}}" readonly="" value="{{number_format($hs)}}">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4">Total</th>
                        <td>
                            <input type="text" class="form-control" id="total_all" value="{{number_format($total)}}" readonly="">
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
            <table class="table table-bordered table-hover">
                <thead>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th>Total</th>
                </thead>
                <tbody id="tbl_cost">
                    @foreach($data->data_cost()->where('gr', $gr)->get() as $key_cost => $cost)
                        @php
                            if($cost->type == 0)
                            {
                                $hs_cost = $cost->amount;
                            }else{
                                $hs_cost = $total * $cost->amount / 100;
                            }
                            $total_cost += $hs_cost;
                            $grand_total += $hs_cost;
                            $count_cost++;
                        @endphp
                        <tr id="tr_add_cost_{{$key_cost}}">
                            <td>
                                {{$cost->code}}
                            </td>
                            <td>
                                {{$cost->name}}
                            </td>
                            <td>
                                <input type="text" class="form-control" value="{{number_format($cost->amount)}}@if($cost->type == 1)%@endif" readonly="">
                            </td>
                            <td>
                                <input type="text" class="form-control" readonly="" value="{{number_format($hs_cost)}}">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="3">Total</th>
                        <td>
                            <input type="text" class="form-control" name="total_cost" id="total_cost" value="{{number_format($total_cost)}}" readonly="">
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label for="grand_total" class="col-sm-2 col-form-label">Grand Total</label>
                <div class="col-sm-10">
                    <input type="hidden" value="{{$grand_total}}" readonly="">
                    <input type="text" class="form-control" value="{{number_format($grand_total)}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Keterangan</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="4" readonly="">{{$data->description}}</textarea>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="float-right">
                <a href="{{URL::to('/purchase')}}" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
