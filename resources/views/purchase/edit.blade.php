@extends('app')
@extends('modals.kas')
@extends('modals.supplier')
@extends('modals.addproduct')
@extends('modals.addcost')


@section('module_name')
    <h1 class="m-0 text-dark">Ubah Penjualan</h1>
@stop
@php
    $total = 0;
    $count = 0;
    $total_cost = 0;
    $count_cost = 0;
    $grand_total = 0;
@endphp


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/purchase/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label for="kas_id" class="col-sm-2 col-form-label">Kas</label>
                    <div class="input-group col-sm-10">
                        <input type="text" class="form-control {{($errors->has('kas_id'))?'is-invalid':''}}" name="kas_name" id="kas_name" readonly="" value="{{$data->data_kas->name ?? "-"}}">
                        @if($data->status == 0)
                            <input type="hidden" name="kas_id" id="kas_id" value="{{$data->kas_id}}">
                            <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#kasModal"><i class="fa fa-folder-open"></i></button>
                        @endif

                        @if($errors->has('kas_id'))
                            <span id="kas_id-error" class="error invalid-feedback">{{$errors->first('kas_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="supplier_id" class="col-sm-2 col-form-label">Supplier</label>
                    <div class="input-group col-sm-10">
                        <input type="text" class="form-control {{($errors->has('supplier_id'))?'is-invalid':''}}" name="supplier_name" id="supplier_name" readonly="" value="{{$data->data_supplier->name ?? "-"}}">
                        @if($data->status == 0)
                            <input type="hidden" name="supplier_id" id="supplier_id" value="{{$data->supplier_id}}">
                            <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#supplierModal"><i class="fa fa-folder-open"></i></button>
                        @endif

                        @if($errors->has('supplier_id'))
                            <span id="supplier_id-error" class="error invalid-feedback">{{$errors->first('supplier_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gudang_id" class="col-sm-2 col-form-label">Gudang</label>
                    <div class="col-sm-10">
                        @if($data->status == 0)
                            <select name="gudang_id" id="gudang_id" class="form-control {{($errors->has('gudang_id'))?'is-invalid':''}}">
                                @foreach($data_gudang as $key => $value)
                                    <option value="{{$value->id}}"
                                        @if($data->gudang_id == $value->id)
                                            selected=""
                                        @endif
                                    >{{$value->name}}</option>
                                @endforeach
                            </select>
                        @else
                            <input type="text" class="form-control" value="{{$data->data_gudang->name}}" readonly="">
                        @endif
                        @if($errors->has('gudang_id'))
                            <span id="gudang_id-error" class="error invalid-feedback">{{$errors->first('gudang_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="invoice" class="col-sm-2 col-form-label">Nomor Faktur</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('invoice'))?'is-invalid':''}}" name="invoice" id="invoice" value="{{$data->invoice}}" readonly="">
                        @if($errors->has('invoice'))
                            <span id="invoice-error" class="error invalid-feedback">{{$errors->first('invoice')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{$data->date}}" @if($data->status == 1) readonly="" @endif>
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        @if($data->status == 0)
                            <select name="status" id="status" class="form-control {{($errors->has('status'))?'is-invalid':''}}">
                                <option value="0">Menunggu</option>
                                @can('approve purchase')
                                    <option value="1" @if($data->status == 1) selected="" @endif>
                                        Di Setujui
                                    </option>
                                @endcan
                                @can('reject purchase')
                                    <option value="2" @if($data->status == 2) selected="" @endif>
                                        Di Batalkan
                                    </option>
                                @endcan
                            </select>
                        @else
                            <label_v1 class="form-control">
                                @if($data->status == 0)
                                    <b><font color="warning">Menunggu</font></b>
                                @elseif($data->status == 1)
                                    Di <b><font color="success">Setujui</font></b> Oleh <b>{{$data->audit_by}}</b> Pada <b>{{$data->audit_at}}</b>
                                @elseif($data->status == 2)
                                    Di <b><font color="danger">Batalkan</font></b> Oleh <b>{{$data->audit_by}}</b> Pada <b>{{$data->audit_at}}</b>
                                @endif
                            </label_v1>
                        @endif
                        @if($errors->has('status'))
                            <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                @if($data->status == 0)
                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addproductModal">
                        <i class="fas fa-plus"> Tambah Barang</i>
                    </button>
                @endif
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Total</th>
                        @if($data->status == 0)
                        <th>Action</th>
                        @endif
                    </thead>
                    <tbody id="tbl_products">
                        @foreach($data->data_detail as $key => $detail)
                            @php
                                $hs = $detail->qty * $detail->amount;
                                $total += $hs;
                                $count++;
                                $grand_total += $hs;
                            @endphp
                            <tr id="tr_add_product_{{$key}}">
                                <td>
                                    {{$detail->data_product->code}}
                                </td>
                                <td>
                                    <input type="hidden" name="ids[]" value="{{$detail->data_product->id}}">
                                    {{$detail->data_product->name}}
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="qty_{{$key}}" name="qtys[]" value="{{$detail->qty}}" min="0" onchange="countTotal({{$key}})" @if($data->status == 1) readonly="" @endif>
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="price_{{$key}}" name="prices[]" value="{{$detail->amount}}" min="0" step="0.1" onchange="countTotal({{$key}})" @if($data->status == 1) readonly="" @endif>
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="totals_{{$key}}" readonly="" value="{{number_format($hs)}}">
                                </td>
                                @if($data->status == 0)
                                <td>
                                    <button type="button" class="btn btn-danger" onclick="removeAddProduct({{$key}})"><i class="fas fa-trash"></i></button>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <td>
                                <input type="text" class="form-control" id="total_all" value="{{number_format($total)}}" readonly="">
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                @if($data->status == 0)
                <button type="button" class="btn btn-warning pull-right" onclick="loadCost()">
                    <i class="fas fa-plus"> Biaya Tambahan</i>
                </button>
                @endif
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        @if($data->status == 0)
                        <th>Action</th>
                        @endif
                    </thead>
                    <tbody id="tbl_cost">
                        @foreach($data->data_cost()->where('gr', $gr)->get() as $key_cost => $cost)
                            @php
                                if($cost->type == 0)
                                {
                                    $hs_cost = $cost->amount;
                                }else{
                                    $hs_cost = $total * $cost->amount / 100;
                                }
                                $total_cost += $hs_cost;
                                $grand_total += $hs_cost;
                                $count_cost++;
                            @endphp
                            <tr id="tr_add_cost_{{$key_cost}}">
                                <td>
                                    {{$cost->code}}
                                </td>
                                <td>
                                    <input type="hidden" name="id_costs[]" value="{{$cost->data_parent->id}}">
                                    {{$cost->name}}
                                </td>
                                <td>
                                    @if($cost->type == 0)
                                        <input type="number" class="form-control" id="amount_cost_{{$key_cost}}" name="amounts[]" value="{{$cost->amount}}" data-type="0" min="0" step="1" onchange="countCost_total({{$cost->amount}}, {{$cost->type}})" @if($data->status == 1) readonly="" @endif>
                                    @else
                                        <input type="number" class="form-control" id="amount_cost_{{$key_cost}}" name="amounts[]" value="{{$cost->amount}}" data-type="1" min="0" max="100" step="0.1" onchange="countCost_total({{$cost->amount}}, {{$cost->type}})" @if($data->status == 1) readonly="" @endif>
                                    @endif
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="totals_cost_{{$key_cost}}" readonly="" value="{{number_format($hs_cost)}}">
                                </td>
                                @if($data->status == 0)
                                <td>
                                    <button type="button" class="btn btn-danger" onclick="removeCost({{$key_cost}})"><i class="fas fa-trash"></i></button>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">Total</th>
                            <td>
                                <input type="text" class="form-control" name="total_cost" id="total_cost" value="{{number_format($total_cost)}}" readonly="">
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="grand_total" class="col-sm-2 col-form-label">Grand Total</label>
                    <div class="col-sm-10">
                        <input type="hidden" name="grand_totalfix" id="grand_totalfix" value="{{$grand_total}}" readonly="">
                        <input type="text" class="form-control" name="grand_total" id="grand_total" value="{{number_format($grand_total)}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" id="description" name="description" rows="4" @if($data->status == 1) disabled="" @endif>{{$data->description}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/purchase')}}" class="btn btn-default">Back</a>
                    @if($data->status == 0)
                        <button type="submit" class="btn btn-warning">Update</button>
                    @elseif($data->status == 1 && $data->pay_off == 0)
                        <a href="/purchase/pay/{{$data->id}}" class="btn btn-warning">Bayar</a>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
