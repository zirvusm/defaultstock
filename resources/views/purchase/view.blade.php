@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Pembelian</h1>
@stop


@section('content')

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

	<div class="card">

        <div class="card-header border-0">
            <h3 class="card-title">Pembelian</h3>
            <div class="card-tools">
                @can('add purchase')
                    <a href="{{URL::to('/purchase/add')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></a>
                @endcan
                <a href="{{URL::to('/purchase')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <select class="form-control" name="filter" id="filter">
                        <option value="">--Pilih Filter--</option>
                        <option value="invoice" @if($filter == 'invoice') selected="" @endif>Nomor Faktur</option>
                        <option value="date" @if($filter == 'date') selected="" @endif>Tanggal</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" name="filter_jt" id="filter_jt">
                        <option value="">--Jatuh Tempo--</option>
                        <option value="Y" @if($filter_jt == 'Y') selected="" @endif>Lewat Jatuh Tempo</option>
                        <option value="N" @if($filter_jt == 'N') selected="" @endif>Belum Jatuh Tempo</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="value" placeholder="Cari . . ." name="value" id="value" value="{{$value}}">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-default" onclick="searchDataPurchase()"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>
        					#
        				</th>
                        <th>
                            Nomor Faktur
                        </th>
                        <th>
                            Tanggal
                        </th>
                        <th>
                            Jatuh Tempo
                        </th>
                        <th>
                            Grand Total
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Pembayaran
                        </th>
                        <th>
                            Oleh / Pada
                        </th>
                        <th width="160px">
                            Action
                        </th>
        			</tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
                            @php
                                $total = $dt->data_detail()->sum(\DB::raw('qty * amount'));
                                $total_cost = $dt->data_cost->where('gr', $gr)->sum('total');
                                $grand_total = $total + $total_cost;
                                $total_paid = $dt->data_payment()->where('status', 1)->sum('amount');
                                $total_unpaid = $grand_total - $total_paid;
                            @endphp
        					<tr>
        						<td>
        							{{($key+1) + (($page - 1) * $take)}}
        						</td>
                                <td>
                                    {{$dt->invoice}}
                                </td>
                                <td>
                                    {{$dt->date}}
                                </td>
                                <td>
                                    @if(date('Y-m-d') >= $dt->due_date)
                                        <font color="red">{{$dt->due_date}}</font>
                                    @else
                                        {{$dt->due_date}}
                                    @endif
                                </td>
                                <td>
                                    {{number_format($total + $total_cost)}}
                                </td>
                                <td>
                                    @if($dt->status == 0)
                                        <font color="orange">Menunggu</font>
                                    @elseif($dt->status == 1)
                                        <font color="success">
                                            Di Setujui
                                        </font>
                                    @elseif($dt->status == 2)
                                        <font color="red">Di Batalkan</font>
                                    @endif
                                </td>
                                <td>
                                    @if($dt->status == 0)
                                        <font color="orange">Menunggu</font>
                                    @elseif($dt->status == 1 || $dt->goods_in == 1)
                                        @if($dt->pay_off == 0)
                                            <font color="orange">Belum Lunas</font><br>
                                            <font color="red">Sisa = {{number_format($total_unpaid)}}</font>
                                        @elseif($dt->pay_off == 1)
                                            <font color="success">Lunas</font>
                                        @endif
                                    @elseif($dt->status == 2 || $dt->goods_in == 2)
                                        <font color="red">Di Batalkan</font>
                                    @endif
                                </td>
                                <td align="center">
                                    {{$dt->audit_by ?? '-'}}<br>{{$dt->audit_at ?? '-'}}
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <a href="{{URL::to('/purchase/detail/'.$dt->id)}}" class="btn btn-primary">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </div>
                                        @if($dt->status == 0)
                                            @can('update purchase')
                                            <div class="col-sm-4">
                                                <a href="{{URL::to('/purchase/'.$dt->id)}}" class="btn btn-warning">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            @endcan
                                            @can('delete purchase')
                                            <div class="col-sm-4">
                                                <button type="button" class="btn btn-danger" onclick="beforeDelete('{{URL::to('/purchase/destroye/'.$dt->id)}}')">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </div>
                                            @endcan
                                        @elseif($dt->status == 1)
                                            <div class="col-sm-4">
                                                <a href="{{URL::to('/purchase/print/'.$dt->id)}}" target="_blank" class="btn btn-warning">
                                                    <i class="fas fa-print"></i>
                                                </a>
                                            </div>
                                            <div class="col-sm-4">
                                                <a href="{{URL::to('/purchase/undo/'.$dt->id)}}" class="btn btn-danger">
                                                    <i class="fas fa-undo"></i>
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                </td>
        					</tr>
        				@endforeach
        			@endif
        		</tbody>
        	</table>
        </div>

        <div class="card-footer">
        	<div class="float-left">
                <select class="form-control" onchange="searchDataPurchase()" id="take_data" name="take_data">
                    @foreach(HelperController::$take as $key => $jumlah)
                        <option value="{{$jumlah}}" @if($take == $jumlah) selected="" @endif>{{$jumlah}}</option>
                    @endforeach
                </select>
                <label class="label-control">Of : {{$count}} Data</label>
        	</div>
        	<div class="float-right">
        		{!!$data->appends(['take' => $take, 'filter' => $filter, 'filter_jt' => $filter_jt, 'value' => $value])->links()!!}
        	</div>
        </div>

	</div>
@stop

<script type="text/javascript">


    searchDataPurchase = () => {

        let url = window.location.origin + window.location.pathname;
        let page = urlParam('page') ? urlParam('page') : 1;
        let take = $('#take_data').val();
        let filter = $('#filter').val();
        let filter_jt = $('#filter_jt').val();
        let value = $('#value').val();

        let query = '';

        if(page != ''){
            if(query != '') query += '&';
            query += 'page='+page;
        }
        if(take != ''){
            if(query != '') query += '&';
            query += 'take='+take;
        }
        if(filter != '' && filter != undefined){
            if(query != '') query += '&';
            query += 'filter='+filter;
        }
        if(filter_jt != '' && filter_jt != undefined){
            if(query != '') query += '&';
            query += 'filter_jt='+filter_jt;
        }
        if(value != '' && value != undefined){
            if(query != '') query += '&';
            query += 'value='+value;
        }

        location.replace(url+'?'+query);
    }
</script>
