@extends('app')
@extends('modals.goodsin')

@section('module_name')
    <h1 class="m-0 text-dark">Ubah Retur Pembelian</h1>
@stop
@php
    $total = 0;
    $arr = [];
@endphp
@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/returnpurchase/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Supplier</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{$data->data_supplier->name}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Gudang</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{$data->data_gudang->name}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nomor Retur</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{$data->invoice}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{$data->date}}" @if($data->status == 1) readonly="" @endif>
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
                @if(Auth::user()->can('approve returnpurchase') || Auth::user()->can('reject returnpurchase'))
                    <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control {{($errors->has('status'))?'is-invalid':''}}" id="status" name="status" @if($data->status == 1) disabled @endif>
                                <option value="0">Menunggu</option>
                                <option value="1" @if($data->status == 1) selected="" @endif>Di Setujui</option>
                                <option value="2" @if($data->status == 2) selected="" @endif>Di Batalkan</option>
                            </select>
                            @if($errors->has('status'))
                                <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                @if($data->status != 1)
                    <button type="button" class="btn btn-primary pull-right" onclick="getGoods()">
                        <i class="fas fa-plus"> Tambah Barang</i>
                    </button>
                @endif
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Total</th>
                    </thead>
                    <tbody id="tbl_goodsin">
                        @foreach($data->data_detail as $key => $detail)
                            @php $arr[] = $detail->product_id; @endphp
                            <tr id="goodsIn_tr_{{$detail->product_id}}">
                                <td>
                                    <input type="hidden" name="ids[]" value="{{$detail->product_id}}">
                                    <label_v1 class="form-control" @if($data->status == 1) readonly="" @endif>{{$detail->data_product->code}}</label_v1>
                                </td>
                                <td>
                                    <input class="form-control" value="{{$detail->data_product->name}}" readonly="">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="qty_{{$detail->product_id}}" name="qtys[]" value="{{$detail->qty}}" min="1" onchange="reCount({{$detail->product_id}})" @if($data->status == 1) readonly="" @endif>
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="amount_{{$detail->product_id}}" name="amounts[]" value="{{$detail->amount}}" min="0" onchange="reCount({{$detail->product_id}})" @if($data->status == 1) readonly="" @endif>
                                </td>
                                <td><label_v1 class="form-control" name="totals[]" id="total_{{$detail->product_id}}" @if($data->status == 1) readonly="" @endif>{{number_format($detail->qty * $detail->amount)}}</label_v1></td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <td>
                                <input type="hidden" class="form-control" name="total_allfix" id="total_allfix" value="{{$total}}" readonly="">
                                <input type="text" class="form-control" name="total_all" id="total_all" value="{{number_format($total)}}" readonly="">
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" id="description" rows="4" @if($data->status == 1) readonly="" @endif>{{$data->description}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/returnpurchase')}}" class="btn btn-default">Back</a>
                    @if($data->status != 1)
                        <button type="submit" class="btn btn-warning">Update</button>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        var newSearch = true;
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function(){
            reCount(0);
        });

        getGoods = () => {
            if(newSearch)
            {
                newSearch = false;
                $.ajax({
                    url: "/returnpurchase/get/goods",
                    type: "POST",
                    data: {
                        _token: $("input[name=_token]").val(),
                        supplier_id: {{$data->supplier_id}},
                        gudang_id: {{$data->gudang_id}},
                        arr: '{{json_encode($arr)}}'
                    },
                    success: function(response){
                        $("#data_goodsin").html(response.view);
                        $("#goodsInModal").modal("show");
                    }
                });
            }else{
                $("#goodsInModal").modal("show");
            }
        }

    </script>

@stop
