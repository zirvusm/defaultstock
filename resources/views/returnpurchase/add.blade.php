@extends('app')
@extends('modals.goodsin')

@section('module_name')
    <h1 class="m-0 text-dark">Tambah Retur Pembelian</h1>
@stop
@php
    $total = 0;
    $invoice = "R-1";

    if($data_old != null){
        $temp = explode("-", $data_old->invoice ?? "");
        $next = (isset($temp[1])) ? $temp[1] + 1 : 1;
        $invoice = "R-".$next;
    }
@endphp
@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/returnpurchase/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="supplier_id" class="col-sm-2 col-form-label">Supplier</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('supplier_id'))?'is-invalid':''}}" name="supplier_id" id="supplier_id" onchange="onChangeData()">
                            @foreach($data_supplier as $key => $supplier)
                                <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('supplier_id'))
                            <span id="supplier_id-error" class="error invalid-feedback">{{$errors->first('supplier_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gudang_id" class="col-sm-2 col-form-label">Gudang</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('gudang_id'))?'is-invalid':''}}" name="gudang_id" id="gudang_id" onchange="onChangeData()">
                            @foreach($data_gudang as $key => $gudang)
                                <option value="{{$gudang->id}}">{{$gudang->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('gudang_id'))
                            <span id="gudang_id-error" class="error invalid-feedback">{{$errors->first('gudang_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="invoice" class="col-sm-2 col-form-label">Nomor Retur</label>
                    <div class="col-sm-10">
                        @if($data_old != null)
                            <code>Nomor Retur sebelumnya : <b>{{$data_old->invoice}}</b></code>
                        @endif
                        <input type="text" class="form-control {{($errors->has('invoice'))?'is-invalid':''}}" name="invoice" id="invoice" value="{{$invoice}}" readonly>
                        @if($errors->has('invoice'))
                            <span id="invoice-error" class="error invalid-feedback">{{$errors->first('invoice')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('date'))?'is-invalid':''}}" name="date" id="date" value="{{date('Y-m-d')}}">
                        @if($errors->has('date'))
                            <span id="date-error" class="error invalid-feedback">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                <button type="button" class="btn btn-primary pull-right" onclick="getGoods()">
                    <i class="fas fa-plus"> Tambah Barang</i>
                </button>
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Total</th>
                    </thead>
                    <tbody id="tbl_goodsin">
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <td>
                                <input type="hidden" class="form-control" name="total_allfix" id="total_allfix" value="{{$total}}" readonly="">
                                <input type="text" class="form-control" name="total_all" id="total_all" value="{{number_format($total)}}" readonly="">
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" id="description" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/returnpurchase')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        var newSearch = true;
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        onChangeData = () => {
            $("#tbl_goodsin").html("");
            $("#data_goodsin").html("");
            $("#total_all").val("");
            newSearch = true;
        }

        getGoods = () => {
            if(newSearch)
            {
                newSearch = false;
                $.ajax({
                    url: "/returnpurchase/get/goods",
                    type: "POST",
                    data: {
                        _token: $("input[name=_token]").val(),
                        supplier_id: $("#supplier_id").val(),
                        gudang_id: $("#gudang_id").val()
                    },
                    success: function(response){
                        $("#data_goodsin").html(response.view);
                        $("#goodsInModal").modal("show");
                    }
                });
            }else{
                $("#goodsInModal").modal("show");
            }
        }

    </script>

@stop
