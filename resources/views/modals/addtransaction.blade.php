<div id="addtransactionModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Tanda Terima</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal" id="filterModal">
                            <option value="">--Pilih Filter--</option>
                            <option value="invoice">Nomor Faktur</option>
                            <option value="date">Tanggal</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal" id="valueModal" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModal()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Nomor Faktur</th>
                            <th>Customer</th>
                            <th>Tanggal</th>
                            <th>Jatuh Tempo</th>
                            <th>Grand Total</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($data_transaction as $key => $transaction)
                                <tr class="modalFilter" data-invoice="{{strtolower($transaction->invoice)}}" data-date="{{strtolower($transaction->date)}}">
                                    <td>
                                        {{$transaction->invoice ?? '-'}}
                                    </td>
                                    <td>
                                        {{$transaction->data_customer->name ?? '-'}}
                                    </td>
                                    <td>
                                        {{$transaction->date}}
                                    </td>
                                    <td>
                                        {{$transaction->due_date}}
                                    </td>
                                    <td>
                                        {{number_format($transaction->amount)}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectAddTransaction({{$transaction->id}}, '{{$transaction->invoice}}', '{{$transaction->data_customer->name}}', '{{$transaction->date}}', '{{$transaction->due_date}}', {{$transaction->amount}})">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    var countAddTransaction = {{$count}};

    selectAddTransaction = (id, invoice, customername, date, due_date, grand_total) => {
        let ht = '';
        countAddTransaction += 1;

        ht += '<tr id="tr_add_transaction_'+countAddTransaction+'">';

        ht += '<td><input type="hidden" name="receipts[]" value="'+invoice+'">'+invoice+'</td>';
        ht += '<td><input type="hidden" name="customernames[]" value="'+customername+'">'+customername+'<input type="hidden" name="ids[]" value="'+id+'"></td>';
        ht += '<td>';
        ht += '<input type="hidden" class="form-control" id="date_'+countAddTransaction+'" name="dates[]" value="'+date+'" readonly="">'+date;
        ht += '</td>';
        ht += '<td>';
        ht += '<input type="hidden" class="form-control" id="due_date_'+countAddTransaction+'" name="due_dates[]" value="'+due_date+'" readonly="">'+due_date;
        ht += '</td>';
        ht += '<td>';
        ht += '<input type="hidden" class="form-control" id="grand_total_'+countAddTransaction+'" name="grand_total[]" value="'+grand_total+'" readonly="">'+grand_total;
        ht += '</td>';
        ht += '<td><button type="button" class="btn btn-danger" onclick="removeAddTransaction('+countAddTransaction+','+grand_total+')"><i class="fas fa-trash"></i></button></td>';

        ht += '</tr>';
        let totalawal = document.getElementById("total_all").value;
        let totalakhir = 0;
        totalakhir = +totalawal + +grand_total;
        $("#total_all").val(totalakhir);
        $("#tbl_transactions").append(ht);
        $("#addtransactionModal").modal('hide');
    }

    removeAddTransaction = (id, grand_total) => {
        let totalawal = document.getElementById("total_all").value;
        let totalakhir = 0;
        totalakhir = +totalawal - +grand_total;
        $("#total_all").val(totalakhir);
        $("#tr_add_transaction_"+id).remove();
    }

</script>
