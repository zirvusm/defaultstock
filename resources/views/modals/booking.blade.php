<div id="bookingModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Pesanan Penjualan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal_selling" id="filterModal_selling">
                            <option value="">--Pilih Filter--</option>
                            <option value="invoice">Nomor Pesanan</option>
                            <option value="sales">Sales</option>
                            <option value="customer">Pelanggan</option>
                            <option value="date">Tanggal</option>
                            <option value="description">Keterangan</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal_selling" id="valueModal_selling" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModalSelling()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Nomor Pesanan</th>
                            <th>Sales</th>
                            <th>Pelanggan</th>
                            <th>Tanggal</th>
                            <th>Total</th>
                            <th>Keterangan</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6" align="center">
                                    Tidak Ada
                                </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectSelling(0,'Tidak Ada',0,'Tidak Ada',0,'Tidak Ada')">Pilih</button>
                                    </td>
                            </tr>
                            @foreach($data_booking as $key => $booking)
                                <tr class="modalFilterSelling" 
                                    data-sales="{{strtolower($booking->sales)}}"
                                    data-customer="{{strtolower($booking->customer)}}"
                                    data-invoice="{{strtolower($booking->invoice)}}"
                                    data-date="{{strtolower($booking->date)}}"
                                    data-description="{{strtolower($booking->description)}}"
                                >
                                    <td>
                                        {{$booking->invoice}}
                                    </td>
                                    <td>
                                        {{$booking->data_sales->name ?? '-'}}
                                    </td>
                                    <td>
                                        {{$booking->data_customer->name ?? '-'}}
                                    </td>
                                    <td>
                                        {{$booking->date}}
                                    </td>
                                    <td>
                                        {{number_format($booking->data_detail()->sum('selling_price'))}}
                                    </td>
                                    <td>
                                        {!!str_replace("\n", "<br>", $booking->description)!!}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectSelling(
                                            {{$booking->id}},
                                            '{{$booking->invoice}}',
                                            {{$booking->sales_id}},
                                            '{{$booking->data_sales->name ?? '-'}}',
                                            '{{$booking->data_sales->code ?? '-'}}',
                                            {{$booking->customer_id}},
                                            '{{$booking->data_customer->name ?? '-'}}',
                                            '{{$booking->data_customer->code ?? '-'}}'
                                        )">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
    
<script type="text/javascript">
    
    selectSelling = (id, invoice, sales_id, sales_name, sales_code, customer_id, customer_name, customer_code) => {
        $("#booking_id").val(id);
        $("#booking_invoice").val(invoice);
        $("#sales_id").val(sales_id);
        $("#customer_id").val(customer_id);
        if(sales_code != ""){
            $("#sales_name").val(sales_name+" ( "+sales_code+" )");
            if(customer_code != null){
                $("#customer_name").val(customer_name+" ( "+customer_code+" )");
            }else{
                $("#customer_name").val("Tidak Ada");
            }
        }else{
            $("#sales_name").val(sales_name);
            if(customer_code == null){
                $("#customer_name").val("Tidak Ada");
            }
        }

        if(id == 0){
            $("#btnMdlCustomer").attr('disabled', false);
            $("#btnMdlSales").attr('disabled', false);
        }else{
            $("#btnMdlCustomer").attr('disabled', true);
            $("#btnMdlSales").attr('disabled', true);
        }

        $("#bookingModal").modal('hide');
        $("#tbl_products").html('<tr><td colspan="6" align="center">Loading . . .</td></tr>');

        $.ajax({
            url : "{{URL::to('/bookingorder/detail_ajax/')}}/"+id,
            success : function(response){
                $("#tbl_products").html(response.view);
                $("#tbl_cost").html(response.view_cost);
                $("#description").val(response.description);
            },error : function(request, status, error){
                $("#tbl_products").html('');
            }
        });   
    }

</script>