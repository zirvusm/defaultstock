<div id="transactionModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal" id="filterModal">
                            <option value="">--Pilih Filter--</option>
                            <option value="invoice">Nomor Faktur</option>
                            <option value="date">Tanggal</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal" id="valueModal" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModal()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Nomor Faktur</th>
                            <th>Tanggal</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($transactions as $key => $transaction)
                                @php
                                    $total = $transaction->data_detail()->sum(\DB::raw('qty * amount'));
                                    $total_cost = $transaction->data_cost->where('gr', $gr)->sum('total');
                                    $grand_total = $total + $total_cost;
                                    $total_paid = $transaction->data_payment()->where('status', 1)->sum('amount');
                                    $transaction_name = $transaction->invoice;
                                    $total_unpaid = $grand_total - $total_paid;
                                @endphp
                                <tr class="modalFilter" data-invoice="{{strtolower($transaction->invoice)}}" data-date="{{strtolower($transaction->date)}}">
                                    <td>
                                        {{$transaction->invoice}}
                                    </td>
                                    <td>
                                        {{$transaction->date}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectKas({{$transaction->id}}, '{{$transaction_name}}')">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
    
<script type="text/javascript">
    
    selectKas = (id, name) => {
        $("#transaction_id").val(id);
        $("#transaction_name").val(name);
        $("#transactionModal").modal('hide');
        $("#transaction_id").trigger("change");
    }

</script>