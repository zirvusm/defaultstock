<div id="kasModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Kas</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Nomor Akun</th>
                            <th>Nama</th>
                            <th>Saldo</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($data_kas as $key => $kas)
                                @php
                                    $kas_name = $kas->name;
                                    if($kas->main == 0) $kas_name = $kas->name." ( ".$kas->account_number." )";
                                @endphp
                                <tr>
                                    <td>
                                        {{$kas->account_number}}
                                    </td>
                                    <td>
                                        {{$kas->name}}
                                    </td>
                                    <td>
                                        {{number_format($kas->amount)}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectKas({{$kas->id}}, '{{$kas_name}}', '{{number_format($kas->amount)}}')">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    selectKas = (id, name, amount) => {
        $("#kas_id").val(id);
        $("#kas_name").val(name+" ( "+amount+" )");
        $("#kasModal").modal('hide');
    }

</script>
