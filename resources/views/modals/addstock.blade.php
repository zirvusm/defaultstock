<div id="addstockModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Stock</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal" id="filterModal">
                            <option value="">--Pilih Filter--</option>
                            <option value="gudang">Gudang</option>
                            <option value="code">Kode</option>
                            <option value="name">Nama</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal" id="valueModal" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModal()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Gudang</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Satuan</th>
                            <th>Qty</th>
                            <th>HPP</th>
                            <th>Harga Penjualan Sebelumnya</th>
                            <th>Action</th>
                        </thead>
                        <tbody id="tbodyCatProduct">
                            @foreach($data_stock as $key => $stock)
                                <tr class="modalFilter" data-code="{{strtolower($stock->data_product->code)}}" data-name="{{strtolower($stock->data_product->name)}}" data-gudang="{{strtolower($stock->data_gudang->name)}}">
                                    <td>
                                        {{$stock->data_gudang->name ?? '-'}}
                                    </td>
                                    <td>
                                        {{$stock->data_product->code ?? '-'}}
                                    </td>
                                    <td>
                                        {{$stock->data_product->name ?? '-'}}
                                    </td>
                                    <td>
                                        {{$stock->data_product->unit}}
                                    </td>
                                    <td>
                                        {{$stock->qty}}
                                    </td>
                                    <td>
                                        {{number_format($stock->hpp)}}
                                    </td>
                                    <td>
                                        {{number_format($stock->data_transaction()->orderBy('id', 'desc')->first()->amount ?? 0)}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectAddStock({{$stock->id}}, '{{$stock->data_product->code}}', '{{$stock->data_product->name}}', {{$stock->hpp}}, {{$stock->qty}})">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    var countAddStock = {{$count}};

    selectAddStock = (id, code, name, hpp, qty) => {
        if(typeof afterselectAddStock !== 'undefined') afterselectAddStock(id, code, name, hpp, qty);
        else $("#addstockModal").modal('hide');
    }

    removeAddProduct = (id) => {
        $("#tr_add_stock_"+id).remove();
        countTotalStock();
    }

    countTotal = (id) => {
        let total = ($("#qty_"+id).val() * $("#price_"+id).val()).toString(),
            sisa    = total.length % 3,
            rupiah  = total.substr(0, sisa),
            ribuan  = total.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }

        $("#totals_"+id).val( rupiah );

        countTotalStock();
    }

</script>
