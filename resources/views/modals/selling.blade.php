<div id="sellingModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Penjualan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal" id="filterModal">
                            <option value="">--Pilih Filter--</option>
                            <option value="invoice">Nomor Faktur</option>
                            <option value="date">Tanggal</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal" id="valueModal" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModal()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Nomor Faktur</th>
                            <th>Tanggal</th>
                            <th>Jatuh Tempo</th>
                            <th>Grand Total</th>
                            <th>Total Bayar</th>
                            <th>Sisa</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($data_selling as $key => $selling)
                                @php
                                    $total = $selling->data_detail()->sum(\DB::raw('qty * amount'));
                                    $total_cost = $selling->data_cost->where('gr', $gr)->sum('total');
                                    $grand_total = $total + $total_cost;
                                    $total_paid = $selling->data_payment()->where('status', 1)->sum('amount');
                                    $selling_name = $selling->invoice;
                                    $total_unpaid = $grand_total - $total_paid;
                                @endphp
                                <tr class="modalFilter" data-invoice="{{strtolower($selling->invoice)}}" data-date="{{strtolower($selling->date)}}">
                                    <td>
                                        {{$selling->invoice}}
                                    </td>
                                    <td>
                                        {{$selling->date}}
                                    </td>
                                    <td>
                                        {{$selling->due_date}}
                                    </td>
                                    <td>
                                        {{number_format($grand_total)}}
                                    </td>
                                    <td>
                                        {{number_format($total_paid)}}
                                    </td>
                                    <td>
                                        {{number_format($total_unpaid)}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectKas({{$selling->id}}, '{{$selling_name}}', '{{$total_unpaid}}', '{{$total_paid}}', '{{$grand_total}}')">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    selectKas = (id, name, unpaid, paid, grandtotal) => {
        $("#selling_id").val(id);
        $("#selling_name").val(name+" ( Grand Total = "+grandtotal+", Total Bayar = "+paid+", Sisa = "+unpaid+" )");
        $("#amount").attr('max', unpaid);
        $("#sellingModal").modal('hide');
    }

</script>
