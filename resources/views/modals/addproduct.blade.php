<div id="addproductModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Produk</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal" id="filterModal">
                            <option value="">--Pilih Filter--</option>
                            <option value="code">Kode</option>
                            <option value="name">Nama</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal" id="valueModal" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModal()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Qty</th>
                            <th>Action</th>
                        </thead>
                        <tbody id="tbodyCatProduct">
                            @foreach($data_product as $key => $product)
                                <tr class="modalFilter" data-code="{{strtolower($product->code)}}" data-name="{{strtolower($product->name)}}">
                                    <td>
                                        {{$product->code}}
                                    </td>
                                    <td>
                                        {{$product->name}}
                                    </td>
                                    <td>
                                        {{$product->data_stock()->sum('qty')}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectAddProduct({{$product->id}}, '{{$product->code}}', '{{$product->name}}')">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
    
<script type="text/javascript">
    var countAddProduct = {{$count}};
    
    selectAddProduct = (id, code, name) => {
        let ht = '';
        countAddProduct += 1;

        ht += '<tr id="tr_add_product_'+countAddProduct+'">';

        ht += '<td><input type="hidden" name="productcodes[]" value="'+code+'">'+code+'</td>';
        ht += '<td><input type="hidden" name="ids[]" value="'+id+'"><input type="hidden" name="productnames[]" value="'+name+'">'+name+'</td>';
        ht += '<td>';
        ht += '<input type="number" class="form-control" id="qty_'+countAddProduct+'" name="qtys[]" value="0" min="0" onchange="countTotal('+countAddProduct+')">';
        ht += '</td>';
        ht += '<td>';
        ht += '<input type="number" class="form-control" id="price_'+countAddProduct+'" name="prices[]" value="0" min="0" step="0.1" onchange="countTotal('+countAddProduct+')">';
        ht += '</td>';
        ht += '<td>';
        ht += '<input type="text" class="form-control" id="totals_'+countAddProduct+'" readonly="">';
        ht += '</td>';
        ht += '<td><button type="button" class="btn btn-danger" onclick="removeAddProduct('+countAddProduct+')"><i class="fas fa-trash"></i></button></td>';

        ht += '</tr>';

        $("#tbl_products").append(ht);
        $("#addproductModal").modal('hide');
    }

    removeAddProduct = (id) => {
        $("#tr_add_product_"+id).remove();
    }

    countTotal = (id) => {
        let total = ($("#qty_"+id).val() * $("#price_"+id).val()).toString(),
            sisa    = total.length % 3,
            rupiah  = total.substr(0, sisa),
            ribuan  = total.substr(sisa).match(/\d{3}/g);
                
        if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }

        $("#totals_"+id).val( rupiah );

        let total_all = 0;

        for (let i = 0; i <= countAddProduct; i++) {
            let qty = $("#qty_"+i).val();
            let price = $("#price_"+i).val();

            if(!isNaN(qty) && !isNaN(price))
            {
                total_all += qty * price;
            }
        }

        total_all = (total_all).toString(),
            sisa_all    = total_all.length % 3,
            rupiah_all  = total_all.substr(0, sisa_all),
            ribuan_all  = total_all.substr(sisa_all).match(/\d{3}/g);
                
        if (ribuan_all) {
            separator_all = sisa_all ? ',' : '';
            rupiah_all += separator_all + ribuan_all.join(',');
        }
        
        $("#total_all").val(rupiah_all);

        if(typeof countTotalCost === 'function'){
            countTotalCost();
        }
        if(typeof countGrandTotalCost === 'function'){
            countGrandTotalCost();
        }
    }

</script>