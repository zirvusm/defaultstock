<div id="supplierModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Supplier</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Code</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($data_supplier as $key => $supplier)
                                <tr>
                                    <td>
                                        {{$supplier->code}}
                                    </td>
                                    <td>
                                        {{$supplier->name}}
                                    </td>
                                    <td>
                                        {{$supplier->address}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectSupplier({{$supplier->id}}, '{{$supplier->name}}', '{{$supplier->code}}')">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    selectSupplier = (id, name, code) => {
        $("#supplier_id").val(id);
        $("#supplier_name").val(name+" ( "+code+" )");
        $("#supplierModal").modal('hide');

        // $.ajax({
        //     url : "/catproduct/getcattegory/"+id,
        //     success : function(data)
        //     {
        //         $("#supplierModal").modal('hide');
        //         $("#tbodyCatProduct").html('');
        //         let ht = "";

        //         ht += '<tr class="modalFilter" data-code="-" data-name="-">';

        //         ht += '<td>-</td>';
        //         ht += '<td>Tidak Ada</td>';
        //         ht += '<td><button type="button" class="btn btn-default btn-flat" onclick="selectCatProduct(0, `Tidak Ada`)">Pilih</button></td>';

        //         ht += '</tr>';

        //         data.forEach(function(dt, index){
        //             ht += '<tr class="modalFilter" data-code="'+dt.code+'" data-name="'+dt.name+'">';

        //             ht += '<td>'+dt.code+'</td>';
        //             ht += '<td>'+dt.name+'</td>';
        //             ht += '<td><button type="button" class="btn btn-default btn-flat" onclick="selectCatProduct('+dt.id+', `'+dt.name+'`)">Pilih</button></td>';

        //             ht += '</tr>';
        //         });
        //         $("#tbodyCatProduct").html(ht);
        //     }
        // });

    }

</script>
