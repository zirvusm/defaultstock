<div id="salesModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Sales</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal_sales" id="filterModal_sales">
                            <option value="">--Pilih Filter--</option>
                            <option value="code">Kode</option>
                            <option value="name">Nama</option>
                            <option value="nik">Nik</option>
                            <option value="address">Address</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal_sales" id="valueModal_sales" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModalSales()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Nik</th>
                            <th>Alamat</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($data_sales as $key => $sales)
                                <tr class="modalFilterSales" data-code="{{strtolower($sales->code)}}" data-name="{{strtolower($sales->name)}}" data-nik="{{strtolower($sales->nik)}}" data-address="{{strtolower($sales->address)}}">
                                    <td>
                                        {{$sales->code}}
                                    </td>
                                    <td>
                                        {{$sales->name}}
                                    </td>
                                    <td>
                                        {{$sales->nik}}
                                    </td>
                                    <td>
                                        {{$sales->address}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectSales({{$sales->id}}, '{{$sales->name}}', '{{$sales->code}}')">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    selectSales = (id, name, code) => {
        $("#sales_id").val(id);
        $("#sales_name").val(name+" ( "+code+" )");
        $("#salesModal").modal('hide');
    }

</script>
