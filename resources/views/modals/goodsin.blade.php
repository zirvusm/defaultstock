<div id="goodsInModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Barang</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal" id="filterModal">
                            <option value="">--Pilih Filter--</option>
                            <option value="code">Kode</option>
                            <option value="name">Nama</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal" id="valueModal" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModal()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Faktur</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th>Total</th>
                            <th>Action</th>
                        </thead>
                        <tbody id="data_goodsin">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
    
<script type="text/javascript">
    
    selectGoods = (id, code, name, qty, amount) => {
        let btn = '<button type="button" class="btn btn-warning" onclick="unSelectGoods('+id+",'"+code+"','"+name+"',"+qty+","+amount+')">Buang</button>';
        $("#btnGoods_"+id).html(btn);

        let ht = '';
        ht += '<tr id="goodsIn_tr_'+id+'">';
        ht += '<td><input type="hidden" name="ids[]" value="'+id+'"><label_v1 class="form-control">'+code+'</label_v1></td>';
        ht += '<td><label_v1 class="form-control">'+name+'</label_v1></td>';
        ht += '<td><input type="number" class="form-control" id="qty_'+id+'" name="qtys[]" value="1" min="1" max="'+qty+'" onchange="reCount('+id+')"></td>';
        ht += '<td><input type="number" class="form-control" id="amount_'+id+'" name="amounts[]" value="'+amount+'" min="0" onchange="reCount('+id+')"></td>';
        ht += '<td><label_v1 class="form-control" name="totals[]" id="total_'+id+'">'+numberWithCommas(1*amount)+'</label_v1></td>';
        ht += '</tr>';

        $("#tbl_goodsin").append(ht);
        reCount(id);
    }

    unSelectGoods = (id, code, name, qty, amount) => {
        let btn = '<button type="button" class="btn btn-default" onclick="selectGoods('+id+",'"+code+"','"+name+"',"+qty+","+amount+')">Pilih</button>';
        $("#btnGoods_"+id).html(btn);

        $("#goodsIn_tr_"+id).remove();
        reCount(0);
    }

    numberWithCommas = (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    reCount = (id) => {
        if(id != 0)
        {
            let qty = $("#qty_"+id).val();
            let amount = $("#amount_"+id).val();
            $("#total_"+id).html(numberWithCommas(qty * amount));
        }

        let values = $("label_v1[name='totals[]']").map(function(){
            return $(this).html();
        }).get();

        let total_all = 0;
        values.forEach(function(dt, index){
            dt = dt.replaceAll(",", "") * 1;
            total_all += dt;
        });
        $("#total_all").val(numberWithCommas(total_all));
    }

</script>