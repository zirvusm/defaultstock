<div id="catProductModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Kategori Produk</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal" id="filterModal">
                            <option value="">--Pilih Filter--</option>
                            <option value="code">Kode</option>
                            <option value="name">Nama</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal" id="valueModal" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModal()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Action</th>
                        </thead>
                        <tbody id="tbodyCatProduct">
                            @foreach($data_category as $key => $category)
                                <tr class="modalFilter" data-code="{{strtolower($category->code)}}" data-name="{{strtolower($category->name)}}">
                                    <td>
                                        {{$category->code}}
                                    </td>
                                    <td>
                                        {{$category->name}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectCatProduct({{$category->id}}, '{{$category->name}}', '{{$category->code}}')">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    selectCatProduct = (id, name, code) => {
        $("#category_id").val(id);
        $("#category_name").val(name+" ( "+code+" )");
        $("#catProductModal").modal('hide');
    }

</script>
