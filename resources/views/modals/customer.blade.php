<div id="customerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Customer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterModal_customer" id="filterModal_customer">
                            <option value="">--Pilih Filter--</option>
                            <option value="code">Kode</option>
                            <option value="name">Nama</option>
                            <option value="nik">Nik</option>
                            <option value="address">Address</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueModal_customer" id="valueModal_customer" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterModalCustomer()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>NIK</th>
                            <th>Alamat</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($data_customer as $key => $customer)
                                <tr class="modalFilterCustomer" data-code="{{strtolower($customer->code)}}" data-name="{{strtolower($customer->name)}}" data-nik="{{strtolower($customer->nik)}}" data-address="{{strtolower($customer->address)}}">
                                    <td>
                                        {{$customer->code}}
                                    </td>
                                    <td>
                                        {{$customer->name}}
                                    </td>
                                    <td>
                                        {{$customer->nik}}
                                    </td>
                                    <td>
                                        {{$customer->address}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat" onclick="selectCustomer(
                                            {{$customer->id}}, 
                                            '{{$customer->name}}',
                                            '{{$customer->code}}',
                                            '{{$customer->data_sales->id ?? 0}}',
                                            '{{$customer->data_sales->name ?? 'None'}}',
                                            '{{$customer->data_sales->code ?? 'None'}}'
                                        )">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
    
<script type="text/javascript">
    
    selectCustomer = (id, name, code, sales_id, sales_name, sales_code) => {
        $("#customer_id").val(id);
        $("#customer_name").val(name+" ( "+code+" )");
        $("#sales_id").val(sales_id);
        $("#sales_name").val(sales_name+" ( "+sales_code+" )");
        $("#customerModal").modal('hide');
    }

</script>