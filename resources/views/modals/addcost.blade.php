<div id="addcostModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">List Cost</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-4">
                        <select class="form-control" name="filterCostModal" id="filterCostModal">
                            <option value="">--Pilih Filter--</option>
                            <option value="code">Kode</option>
                            <option value="name">Nama</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Cari . . ." name="valueCostModal" id="valueCostModal" value="">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-default" onclick="filterCostModal()"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Jumlah</th>
                            <th>Action</th>
                        </thead>
                        <tbody id="tbodyCost">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    var countCost = {{$count_cost}};

    loadCost = () => {
        $("#tbodyCost").html('<tr><td colspan="4" align="center">Loading . . .</td></tr>');
        $("#addcostModal").modal('show');

        $.ajax({
            url : "{{URL::to('/additionalcost/getAll')}}",
            success : function(response){
                let data = response;
                let ht = "";

                data.forEach(function(dt, index){

                    ht += '<tr class="modalCostFilter" data-code="'+dt.code.toLowerCase()+'" data-name="'+dt.name.toLowerCase()+'">';
                    ht += '<td>'+dt.code+'</td>';
                    ht += '<td>'+dt.name+'</td>';

                    if(dt.type == 0)
                        ht += '<td>'+strNumberFormat(dt.amount)+'</td>';
                    else
                        ht += '<td>'+dt.amount+'%</td>';

                    ht += '<td>';
                    ht += '<button type="button" class="btn btn-default btn-flat" onclick="selectCost('+dt.id+', `'+dt.code+'`, `'+dt.name+'`, `'+dt.type+'`, `'+dt.amount+'`)">Pilih</button>';
                    ht += '</td>';

                    ht += '</tr>';

                });
                $("#tbodyCost").html(ht);
            },
            error : function(a, b){
                $("#tbodyCost").html('<tr><td colspan="3" align="center">No Data</td></tr>');
            }
        });
    }

    selectCost = (id, code, name, type, amount) => {
        let ht = '';
        countCost += 1;

        ht += '<tr id="tr_add_cost_'+countCost+'">';

        ht += '<td><input type="hidden" name="costcodes[]" value="'+code+'">'+code+'</td>';
        ht += '<td><input type="hidden" name="id_costs[]" value="'+id+'"><input type="hidden" name="costnames[]" value="'+name+'">'+name+'</td>';
        ht += '<td>';
        ht += '<input type="hidden" name="types[]" value="'+type+'">';
        if(type == 0)
        {
            ht += '<input type="number" class="form-control" id="amount_cost_'+countCost+'" name="amounts[]" value="'+amount+'" data-type="0" min="0" step="1" onchange="countCost_total('+countCost+', '+type+')">';
        }else{
            ht += '<input type="number" class="form-control" id="amount_cost_'+countCost+'" name="amounts[]" value="'+amount+'" data-type="1" min="0" max="100" step="0.1" onchange="countCost_total('+countCost+', '+type+')">';
        }
        ht += '</td>';
        ht += '<td>';
        ht += '<input type="text" class="form-control" id="totals_cost_'+countCost+'" readonly="">';
        ht += '</td>';
        ht += '<td><button type="button" class="btn btn-danger" onclick="removeCost('+countCost+')"><i class="fas fa-trash"></i></button></td>';

        ht += '</tr>';

        $("#tbl_cost").append(ht);
        $("#amount_cost_"+countCost).trigger('change');
        $("#addcostModal").modal('hide');
    }

    removeCost = (id) => {
        $("#tr_add_cost_"+id).remove();
        countTotalCost();
        countGrandTotalCost();
    }

    countCost_total = (id, type) => {
        let amount = $("#amount_cost_"+id).val();
        let total = $("#total_all").val();

        if(type == 0){
            $("#totals_cost_"+id).val(strNumberFormat(amount));
            countTotalCost();
        }else{
            countTotalCost();
            // let total_cost = $("#total_cost").val();

            // total_cost = total_cost.replaceAll(",", "");
            // total_cost = total_cost.replaceAll(".", "");
            // total_cost = total_cost * 1;

            // amount = amount.replaceAll(",", "");
            // amount = amount.replaceAll(".", "");
            // amount = amount * 1;

            // total = total.replaceAll(",", "");
            // total = total.replaceAll(".", "");
            // total = total * 1;

            // let result = total * amount / 100;
            // $("#totals_cost_"+id).val(strNumberFormat(result));
            // $("#total_cost").val(strNumberFormat(result + total_cost));
        }
        countGrandTotalCost();
    }

    countGrandTotalCost = () => {
        let total = $("#total_all").val();
        let total_cost = $("#total_cost").val();

        total = total.replaceAll(",", "");
        total = total.replaceAll(".", "");
        total = total * 1;
        if(isNaN(total)){
            total = 0;
        }

        total_cost = total_cost.replaceAll(",", "");
        total_cost = total_cost.replaceAll(".", "");
        total_cost = total_cost * 1;
        if(isNaN(total_cost)){
            total = 0;
        }

        $("#grand_totalfix").val(total + total_cost);
        $("#grand_total").val(strNumberFormat(total + total_cost));
    }

    countTotalCost = () => {
        let total_all = 0;
        for (let i = 0; i <= countCost; i++) {
            let amount = $("#totals_cost_"+i).val();
            let type = $("#amount_cost_"+i).data('type');

            if(amount != undefined)
            {
                amount = amount.replaceAll(",", "");
                amount = amount.replaceAll(".", "");
                amount = amount * 1;

                if(!isNaN(amount))
                {
                    if(type == 0)
                    {
                        total_all += (amount * 1);
                    }else{
                        let total = $("#total_all").val();
                        let amount_cost = $("#amount_cost_"+i).val();

                        if(amount_cost == undefined){
                            amount_cost = "0";
                        }

                        total = total.replaceAll(",", "");
                        total = total.replaceAll(".", "");
                        total = total * 1;
                        if(isNaN(total)){
                            total = 0;
                        }

                        amount_cost = amount_cost.replaceAll(",", "");
                        amount_cost = amount_cost.replaceAll(".", "");
                        amount_cost = amount_cost * 1;
                        if(isNaN(amount_cost)){
                            amount_cost = 0;
                        }
                        $("#totals_cost_"+i).val(strNumberFormat(total * amount_cost / 100));

                        total_all += (total * amount_cost / 100);
                    }
                }
            }

        }
        // total_all = (total_all).toString(),
        //     sisa_all    = total_all.length % 3,
        //     rupiah_all  = total_all.substr(0, sisa_all),
        //     ribuan_all  = total_all.substr(sisa_all).match(/\d{3}/g);

        // if (ribuan_all) {
        //     separator_all = sisa_all ? ',' : '';
        //     rupiah_all += separator_all + ribuan_all.join(',');
        // }

        $("#total_cost").val(strNumberFormat(total_all));
    }

</script>
