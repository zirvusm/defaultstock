@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Edit Menu</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/menu/update')}}" method="post">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label for="parent_id" class="col-sm-2 col-form-label">Parent</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('parent_id'))?'is-invalid':''}}" name="parent_id" id="parent_id">
                            <option value="0">None</option>
                            @foreach($parent as $id => $name)
                                <option value="{{$id}}" @if($data->parent_id == $id) selected="" @endif>{{$name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('parent_id'))
                            <span id="parent_id-error" class="error invalid-feedback">{{$errors->first('parent_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" id="name" name="name" id="name" value="{{$data->name}}">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="route" class="col-sm-2 col-form-label">Route</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('route'))?'is-invalid':''}}" id="route" name="route" id="route" value="{{$data->route}}" readonly="">
                        @if($errors->has('route'))
                            <span id="route-error" class="error invalid-feedback">{{$errors->first('route')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="icon" class="col-sm-2 col-form-label">Icon</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('icon'))?'is-invalid':''}}" id="icon" name="icon" id="icon" value="{{$data->icon}}">
                        @if($errors->has('icon'))
                            <span id="icon-error" class="error invalid-feedback">{{$errors->first('icon')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="order" class="col-sm-2 col-form-label">Order</label>
                    <div class="col-sm-10">
                        <input type="number" min="0" class="form-control {{($errors->has('order'))?'is-invalid':''}}" id="order" name="order" id="order" value="{{$data->order}}">
                        @if($errors->has('order'))
                            <span id="order-error" class="error invalid-feedback">{{$errors->first('order')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="is_menu" class="col-sm-2 col-form-label">Is Menu</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('is_menu'))?'is-invalid':''}}" name="is_menu" id="is_menu">
                            @foreach(HelperController::$bool as $index => $value)
                                <option value="{{$index}}" @if($data->is_menu == $index) selected="" @endif>{!!$value!!}</option>
                            @endforeach
                        </select>
                        @if($errors->has('is_menu'))
                            <span id="is_menu-error" class="error invalid-feedback">{{$errors->first('is_menu')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('status'))?'is-invalid':''}}" name="status" id="status">
                            @foreach(HelperController::$status as $index => $value)
                                <option value="{{$index}}" @if($data->status == $index) selected="" @endif>{!!$value!!}</option>
                            @endforeach
                        </select>
                        @if($errors->has('status'))
                            <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/menu')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop