@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Lap. Hutang</h1>
@stop

@php
    $total = 0;
    $current_supplier = 0;
    $current_total = 0;
@endphp

@section('content')

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    
	<div class="card">

        <div class="card-header border-0">
            <h3 class="card-title">Lap. Hutang</h3>
            <div class="card-tools">
                <a href="{{URL::to('/laphutang')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <select class="form-control" name="supplier_id" id="supplier_id">
                        <option value="">--Pilih Supplier--</option>
                        @foreach($data_supplier as $key => $supplier)
                            <option value="{{$supplier->id}}" @if($supplier->id == $supplier_id) selected @endif>
                                {{$supplier->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" name="jto" id="jto">
                        <option value="">--Pilih Jenis--</option>
                        <option value="0" @if($jto == 0 && $jto != "") selected @endif>Belum Jatuh Tempo</option>
                        <option value="1" @if($jto == 1 && $jto != "") selected @endif>Sudah Jatuh Tempo</option>
                    </select>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-default" onclick="searchDataUnique()"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>#</th>
                        <th>Tanggal</th>
                        <th>Tanggal JTO</th>
                        <th>Nomor Faktur</th>
                        <th>Jumlah</th>
        			</tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
                            @if($current_supplier != $dt->supplier_id)
                                @if($current_supplier > 0)
                                    <tr>
                                        <td colspan="4" align="center"><b>Total</b></td>
                                        <td>
                                            <b>{{number_format($current_total)}}</b>
                                        </td>
                                    </tr>
                                    @php
                                        $current_total = 0;
                                    @endphp
                                @endif
                                <tr>
                                    <td colspan="5">
                                        <b>{{$dt->data_supplier->name ?? "-"}}</b>
                                    </td>
                                </tr>
                            @endif
                            @php
                                $jumlah = $dt->amount - $dt->data_payment()->where('type', 'buy')->where('status', 1)->sum('amount');
                                $total += $jumlah;
                                $current_total += $jumlah;
                            @endphp
                            <tr>
                                <td>{{($key + 1)}}</td>
                                <td>{{\DateTime::createFromFormat('Y-m-d', $dt->date)->format('d-M-Y')}}</td>
                                <td>{{\DateTime::createFromFormat('Y-m-d', $dt->due_date)->format('d-M-Y')}}</td>
                                <td>{{$dt->invoice}}</td>
                                <td>
                                    {{number_format($jumlah)}}
                                </td>
                            </tr>
                            @php
                                $current_supplier = $dt->supplier_id;
                            @endphp
        				@endforeach
                        <tr>
                            <td colspan="4" align="center"><b>Total</b></td>
                            <td>
                                <b>{{number_format($current_total)}}</b>
                            </td>
                        </tr>
        			@endif
        		</tbody>
                <tfoot>
                    <tr><td colspan="5">&nbsp;</td></tr>
                    <tr>
                        <td colspan="4" align="center"><b>Grand Total</b></td>
                        <td><b>{{number_format($total)}}</b></td>
                    </tr>
                </tfoot>
        	</table>
        </div>
	</div>

    <script type="text/javascript">
        
        searchDataUnique = () => {
            let url = window.location.origin + window.location.pathname;
            let supplier_id = $('#supplier_id').val();
            let jto = $('#jto').val();

            let query = '';

            if(supplier_id != ''){
                if(query != '') query += '&';
                query += 'supplier_id='+supplier_id;
            }

            if(jto != ''){
                if(query != '') query += '&';
                query += 'jto='+jto;
            }

            location.replace(url+'?'+query);
        }

    </script>
@stop