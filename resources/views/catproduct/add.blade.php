@extends('app')
{{-- @extends('modals.supplier')
@extends('modals.catproduct') --}}


@section('module_name')
    <h1 class="m-0 text-dark">Tambah Kategori Produk</h1>
@stop


@section('content')

<style type="text/css">
    .my-custom-scrollbar {
        position: relative;
        /*height: 300px;*/
        overflow: auto;
    }
    .table-wrapper-scroll-y {
        display: block;
    }
</style>

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/catproduct/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Kode</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('code'))?'is-invalid':''}}" name="code" id="code" value="{{old('code')}}">
                        @if($errors->has('code'))
                            <span id="code-error" class="error invalid-feedback">{{$errors->first('code')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" id="name" value="{{old('name')}}">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" id="description" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
                
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/catproduct')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

@stop