@extends('app')
{{-- @extends('modals.supplier') --}}
@extends('modals.catproduct')


@section('module_name')
    <h1 class="m-0 text-dark">Tambah Produk</h1>
@stop


@section('content')

<style type="text/css">
    .my-custom-scrollbar {
        position: relative;
        /*height: 300px;*/
        overflow: auto;
    }
    .table-wrapper-scroll-y {
        display: block;
    }
</style>

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/product/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                {{-- <div class="form-group row">
                    <label for="supplier_id" class="col-sm-2 col-form-label">Supplier</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="supplier_id" id="supplier_id">
                        <input type="text" class="form-control {{($errors->has('supplier_id'))?'is-invalid':''}}" name="supplier_name" id="supplier_name" readonly="">

                        <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#supplierModal"><i class="fa fa-folder-open"></i></button>

                        @if($errors->has('supplier_id'))
                            <span id="supplier_id-error" class="error invalid-feedback">{{$errors->first('supplier_id')}}</span>
                        @endif
                    </div>
                </div> --}}
                <div class="form-group row">
                    <label for="category_id" class="col-sm-2 col-form-label">Kategori</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="category_id" id="category_id" value="{{old('category_id')}}">
                        <input type="text" class="form-control {{($errors->has('category_id'))?'is-invalid':''}}" name="category_name" id="category_name" readonly="" value="{{old('category_name')}}">

                        <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#catProductModal"><i class="fa fa-folder-open"></i></button>

                        @if($errors->has('category_id'))
                            <span id="category_id-error" class="error invalid-feedback">{{$errors->first('category_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Kode</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('code'))?'is-invalid':''}}" name="code" id="code" value="{{old('code')}}">
                        @if($errors->has('code'))
                            <span id="code-error" class="error invalid-feedback">{{$errors->first('code')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" id="name" value="{{old('name')}}">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="unit" class="col-sm-2 col-form-label">Satuan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('unit'))?'is-invalid':''}}" name="unit" id="unit" value="{{old('unit')}}">
                        @if($errors->has('unit'))
                            <span id="unit-error" class="error invalid-feedback">{{$errors->first('unit')}}</span>
                        @endif
                    </div>
                </div>
                {{-- <div class="form-group row">
                    <label for="selling_price" class="col-sm-2 col-form-label">Harga Jual</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control {{($errors->has('selling_price'))?'is-invalid':''}}" name="selling_price" id="selling_price" min="0" value="{{old('selling_price') ?? 0}}">
                        @if($errors->has('selling_price'))
                            <span id="selling_price-error" class="error invalid-feedback">{{$errors->first('selling_price')}}</span>
                        @endif
                    </div>
                </div> --}}
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" id="description" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/product')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

@stop
