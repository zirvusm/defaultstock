<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{env('APP_NAME')}} | Log in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('/template/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{asset('/template/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/template/dist/css/adminlte.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    @toastr_css
</head>
{{-- toastr()->warning('My name is Inigo Montoya. You killed my father, prepare to die!')
toastr()->success('Have fun storming the castle!', 'Miracle Max Says')
toastr()->error('I do not think that word means what you think it means.', 'Inconceivable!') --}}

@php
    // dd($errors->has('email'));
    // dd($errors->first('email'));
@endphp

<body class="hold-transition login-page">

<div class="login-box">
    <div class="login-logo">
        <strong>{{env('APP_NAME')}}</strong>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="{{URL::to('/login')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control {{($errors->has('email')) ? 'is-invalid' : ''}}" id="email" name="email" placeholder="Enter email" aria-invalid="">
                    @if($errors->has('email'))
                        <span id="email-error" class="error invalid-feedback">{{$errors->first('email')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control {{($errors->has('password')) ? 'is-invalid' : ''}}" id="password" name="password" placeholder="Enter password" aria-invalid="">
                    @if($errors->has('password'))
                        <span id="password-error" class="error invalid-feedback">{{$errors->first('password')}}</span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{asset('/template/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('/template/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/template/dist/js/adminlte.min.js')}}"></script>
<link href="{{asset('/toastr.min.css')}}" rel="stylesheet"/>
<script src="{{asset('/toastr.min.js')}}"></script>
</body>
@toastr_js
@toastr_render
</html>
