<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{env('APP_NAME')}} | Dashboard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{asset('/template/plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{asset('/template/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstra')}}p-4.min.css">
  <link rel="stylesheet" href="{{asset('/template/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('/template/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('/template/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <link rel="stylesheet" href="{{asset('/template/plugins/daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{asset('/template/plugins/summernote/summernote-bs4.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  @toastr_css
</head>
<style type="text/css">
    .my-custom-scrollbar {
        position: relative;
        /*height: 300px;*/
        overflow: auto;
    }
    .table-wrapper-scroll-y {
        display: block;
    }
</style>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">


  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
  
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
    
      <li class="nav-item dropdown">
        <blink id="notif" style="display: none" data-blink="false">
            <a href="/dashboard" class="btn btn-tool btn-danger" style="background-color: red !important">
              <font color="white" id="notifText">0 Transaction</font>
            </a>
        </blink>
        <button class="btn btn-tool" name="btn_profile_setting" id="btn_profile_setting">
          <i class="fas fa-cogs"></i>
        </button>
      </li>
    </ul>
  </nav>



  <aside class="main-sidebar sidebar-dark-primary elevation-4">
  
    <a href="/" class="brand-link">
      <img src="{{asset('/img/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{env('APP_NAME')}}</span>
    </a>

  
    <div class="sidebar">
    
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{Auth::user()->img}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

    
      <nav class="mt-2">
        {!!view('listmenu')!!}
      </nav>
    
    </div>
  
  </aside>


  <div class="content-wrapper">
  
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            @yield('module_name')
          </div>
        </div>
      </div>
    </div>
  

  
    <section class="content">
      @yield('content')
    </section>
  
  </div>

  <footer class="main-footer">
    {{-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.2
    </div> --}}
  </footer>


  <aside class="control-sidebar control-sidebar-dark">
  
  </aside>


</div>

<script src="{{asset('/template/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('/template/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('/template/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/template/plugins/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('/template/plugins/sparklines/sparkline.js')}}"></script>
<script src="{{asset('/template/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{asset('/template/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('/template/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('/template/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('/template/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{asset('/template/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<script src="{{asset('/template/dist/js/adminlte.js')}}"></script>
<script src="{{asset('/template/dist/js/demo.js')}}"></script>
{!!view('script')!!}
</body>
@yield('script')
@toastr_js
@toastr_render
</html>
