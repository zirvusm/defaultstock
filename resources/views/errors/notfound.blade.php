@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Not Found</h1>
@stop


@section('content')

<div class="flex-center position-ref full-height">
    <code>404</code>
</div>

@stop