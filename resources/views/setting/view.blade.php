@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Company Setting</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/setting/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="full_name" class="col-sm-2 col-form-label">Full Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('full_name')) ? 'is-invalid' : ''}}" name="full_name" id="full_name" value="{{$data['full_name']}}">
                        @if($errors->has('full_name'))
                            <span id="full_name-error" class="error invalid-feedback">{{$errors->first('full_name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="short_name" class="col-sm-2 col-form-label">Short Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('short_name')) ? 'is-invalid' : ''}}" name="short_name" id="short_name" value="{{$data['short_name']}}">
                        @if($errors->has('short_name'))
                            <span id="short_name-error" class="error invalid-feedback">{{$errors->first('short_name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('email')) ? 'is-invalid' : ''}}" name="email" id="email" value="{{$data['email']}}">
                        @if($errors->has('email'))
                            <span id="email-error" class="error invalid-feedback">{{$errors->first('email')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('address')) ? 'is-invalid' : ''}}" name="address" id="address" rows="4">{{$data['address']}}</textarea>
                        @if($errors->has('address'))
                            <span id="address-error" class="error invalid-feedback">{{$errors->first('address')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="hp" class="col-sm-2 col-form-label">Telp</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('hp')) ? 'is-invalid' : ''}}" name="hp" id="hp" value="{{$data['hp']}}">
                        @if($errors->has('hp'))
                            <span id="hp-error" class="error invalid-feedback">{{$errors->first('hp')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="wa" class="col-sm-2 col-form-label">WhatsApp</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('wa')) ? 'is-invalid' : ''}}" name="wa" id="wa" value="{{$data['wa']}}">
                        @if($errors->has('wa'))
                            <span id="wa-error" class="error invalid-feedback">{{$errors->first('wa')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="fb" class="col-sm-2 col-form-label">FaceBook</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('fb')) ? 'is-invalid' : ''}}" name="fb" id="fb" value="{{$data['fb']}}">
                        @if($errors->has('fb'))
                            <span id="fb-error" class="error invalid-feedback">{{$errors->first('fb')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ig" class="col-sm-2 col-form-label">Instagram</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('ig')) ? 'is-invalid' : ''}}" name="ig" id="ig" value="{{$data['ig']}}">
                        @if($errors->has('ig'))
                            <span id="ig-error" class="error invalid-feedback">{{$errors->first('ig')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="map_link" class="col-sm-2 col-form-label">Google Map Link</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('map_link')) ? 'is-invalid' : ''}}" name="map_link" id="map_link" value="{{$data['map_link']}}">
                        @if($errors->has('map_link'))
                            <span id="map_link-error" class="error invalid-feedback">{{$errors->first('map_link')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="iframe" class="col-sm-2 col-form-label">Iframe</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('iframe')) ? 'is-invalid' : ''}}" id="iframe" name="iframe">{{$data['iframe']}}</textarea>
                        @if($errors->has('iframe'))
                            <span id="iframe-error" class="error invalid-feedback">{{$errors->first('iframe')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="favicon" class="col-sm-2 col-form-label">Fav Icon</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" accept=".jpg,.jpeg,.png,.gif,.svg,.ico" class="custom-file-input" name="favicon" id="favicon" value="">
                                <label class="custom-file-label" for="favicon" id="textLogo">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Upload</span>
                            </div>
                        </div>
                        @if($errors->has('favicon'))
                            <span id="favicon-error" class="error invalid-feedback">{{$errors->first('favicon')}}</span>
                        @endif
                    </div>
                </div>
                <img src="{{$data['favicon']}}" id="previewfav" width="30%">
                <div class="form-group row">
                    <label for="logo" class="col-sm-2 col-form-label">Logo</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" accept=".jpg,.jpeg,.png,.gif,.svg" class="custom-file-input" name="logo" id="logo" value="">
                                <label class="custom-file-label" for="logo" id="textLogo">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Upload</span>
                            </div>
                        </div>
                        @if($errors->has('logo'))
                            <span id="logo-error" class="error invalid-feedback">{{$errors->first('logo')}}</span>
                        @endif
                    </div>
                </div>
                <img src="{{$data['logo']}}" id="previewlogo" width="30%">
            </div>
            <div class="card-footer">
                <div class="float-right">
                    @can('update setting')
                        <button type="submit" class="btn btn-primary">Save</button>
                    @endcan
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        $('#logo').change(function(){
            $('#textLogo').html(this.value);
            readURL(this, '#previewlogo');
        });

        $('#favicon').change(function(){
            $('#textFav').html(this.value);
            readURL(this, '#previewfav');
        });

        function readURL(input, target) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(target).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop