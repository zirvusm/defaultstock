@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Tambah Customer</h1>
@stop

@php
    $code = "C-1";

    if($data_customer != null){
        $temp = explode("-", $data_customer->code ?? "");
        $next = (isset($temp[1])) ? $temp[1] + 1 : 1;
        $code = "C-".$next;
    }
@endphp

@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/customer/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="sales_id" class="col-sm-2 col-form-label">Sales</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('sales_id'))?'is-invalid':''}}" name="sales_id" id="sales_id">
                            <option value="0">None</option>
                            @foreach($data_sales as $key => $sales)
                                <option value="{{$sales->id}}"
                                    @if(old('sales_id') == $sales->id)
                                        selected=""
                                    @endif
                                >{{$sales->name}} ({{$sales->code}})</option>
                            @endforeach
                        </select>
                        @if($errors->has('sales_id'))
                            <span id="sales_id-error" class="error invalid-feedback">{{$errors->first('sales_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Kode</label>
                    <div class="col-sm-10">
                        @if($data_customer != null)
                            <code>Kode Pelanggan sebelumnya : <b>{{$data_customer->code}}</b></code>
                        @endif
                        <input type="text" class="form-control {{($errors->has('code'))?'is-invalid':''}}" name="code" id="code" value="{{$code}}" readonly>
                        @if($errors->has('code'))
                            <span id="code-error" class="error invalid-feedback">{{$errors->first('code')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nik" class="col-sm-2 col-form-label">NIK</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('nik'))?'is-invalid':''}}" name="nik" id="nik" placeholder="kosongkan jika tidak ada" value="{{old('nik')}}">
                        @if($errors->has('nik'))
                            <span id="nik-error" class="error invalid-feedback">{{$errors->first('nik')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" id="name" value="{{old('name')}}">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="bdate" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control {{($errors->has('bdate'))?'is-invalid':''}}" name="bdate" id="bdate" value="{{old('bdate')}}">
                        <code>kosongkan jika tidak ada</code>
                        @if($errors->has('bdate'))
                            <span id="bdate-error" class="error invalid-feedback">{{$errors->first('bdate')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone_number" class="col-sm-2 col-form-label">Nomor Telpon</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('phone_number'))?'is-invalid':''}}" name="phone_number" id="phone_number" placeholder="kosongkan jika tidak ada" value="{{old('phone_number')}}">
                        @if($errors->has('phone_number'))
                            <span id="phone_number-error" class="error invalid-feedback">{{$errors->first('phone_number')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('email'))?'is-invalid':''}}" name="email" id="email" placeholder="kosongkan jika tidak ada" value="{{old('email')}}">
                        @if($errors->has('email'))
                            <span id="email-error" class="error invalid-feedback">{{$errors->first('email')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('status'))?'is-invalid':''}}" id="status" name="status">
                            <option value="1">Aktif</option>
                            <option value="0" @if(old('status') == 0) selected="" @endif>Tidak Aktif</option>
                        </select>
                        @if($errors->has('status'))
                            <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('address'))?'is-invalid':''}}" id="address" name="address">{{old('address')}}</textarea>
                        @if($errors->has('address'))
                            <span id="address-error" class="error invalid-feedback">{{$errors->first('address')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" id="description" name="description" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/customer')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
