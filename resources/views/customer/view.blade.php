@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Customer</h1>
@stop


@section('content')

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    
	<div class="card">

        <div class="card-header border-0">
            <h3 class="card-title">Customer</h3>
            <div class="card-tools">
                @can('add customer')
                    <a href="{{URL::to('/customer/add')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></a>
                @endcan
                <a href="{{URL::to('/customer')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <select class="form-control" name="filter" id="filter">
                        <option value="">--Pilih Filter--</option>
                        <option value="data_sales.code" @if($filter == 'data_sales.code') selected="" @endif>Kode Sales</option>
                        <option value="data_sales.name" @if($filter == 'data_sales.name') selected="" @endif>Nama Sales</option>
                        <option value="code" @if($filter == 'code') selected="" @endif>Kode</option>
                        <option value="name" @if($filter == 'name') selected="" @endif>Nama</option>
                        <option value="nik" @if($filter == 'nik') selected="" @endif>NIK</option>
                        <option value="phone_number" @if($filter == 'phone_number') selected="" @endif>Nomor Telpon</option>
                        <option value="email" @if($filter == 'email') selected="" @endif>Email</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="value" placeholder="Cari . . ." name="value" id="value" value="{{$value}}">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-default" onclick="searchData()"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>
        					#
        				</th>
                        <th>
                            Sales
                        </th>
                        <th>
                            Kode
                        </th>
                        <th>
                            Nama
                        </th>
                        <th>
                            NIK
                        </th>
                        <th>
                            Nomor Telpon
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Status
                        </th>
                        <th width="120px">
                            Action
                        </th>
        			</tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
        					<tr>
        						<td>
        							{{($key+1) + (($page - 1) * $take)}}
        						</td>
                                <td>
                                    @if($dt->data_sales != null)
                                        {{$dt->data_sales->name}} ({{$dt->data_sales->code}})
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    {{$dt->code}}
                                </td>
                                <td>
                                    {{$dt->name}}
                                </td>
                                <td>
                                    {{$dt->nik ?? "-"}}
                                </td>
                                <td>
                                    {{$dt->phone_number ?? "-"}}
                                </td>
                                <td>
                                    {{$dt->email ?? "-"}}
                                </td>
                                <td>
                                    @if($dt->status == 1)
                                        <font color="green">Aktif</font>
                                    @else
                                        <font color="red">Tidak Aktif</font>
                                    @endif
                                </td>
                                <td>
                                    <div class="row">
                                        @can('update customer')
                                        <div class="col-sm-6">
                                            <a href="{{URL::to('/customer/'.$dt->id)}}" class="btn btn-warning">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        </div>
                                        @endcan
                                        @can('delete customer')
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-danger" onclick="beforeDelete('{{URL::to('/customer/destroye/'.$dt->id)}}')">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </div>
                                        @endcan
                                    </div>
                                </td>
        					</tr>
        				@endforeach
        			@endif
        		</tbody>
        	</table>
        </div>

        <div class="card-footer">
        	<div class="float-left">
                <select class="form-control" onchange="searchData()" id="take_data" name="take_data">
                    @foreach(HelperController::$take as $key => $jumlah)
                        <option value="{{$jumlah}}" @if($take == $jumlah) selected="" @endif>{{$jumlah}}</option>
                    @endforeach
                </select>
                <label class="label-control">Of : {{$count}} Data</label>
        	</div>
        	<div class="float-right">
        		{!!$data->appends(['take' => $take, 'filter' => $filter, 'value' => $value])->links()!!}
        	</div>
        </div>

	</div>
@stop