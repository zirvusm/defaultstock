@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Stok</h1>
@stop


@section('content')

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

	<div class="card">

        <div class="card-header border-0">
            <h3 class="card-title">Stok</h3>
            <div class="card-tools">
                @can('add stock')
                    <a href="{{URL::to('/stock/add')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></a>
                @endcan
                <a href="{{URL::to('/stock')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <select class="form-control" name="filter" id="filter">
                        <option value="">--Pilih Filter--</option>
                        <option value="data_gudang.name" @if($filter == 'data_gudang.name') selected="" @endif>Nama Gudang</option>
                        <option value="data_product.name" @if($filter == 'data_product.name') selected="" @endif>Nama Produk</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="value" placeholder="Cari . . ." name="value" id="value" value="{{$value}}">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-default" onclick="searchData()"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>#</th>
                        <th>Supplier</th>
                        <th>Gudang</th>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Qty</th>
                        <th>HPP</th>
                        <th width="120px">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
        					<tr>
        						<td>
        							{{($key+1) + (($page - 1) * $take)}}
        						</td>
                                <td>
                                    {{$dt->data_supplier->name}}
                                </td>
                                <td>
                                    {{$dt->data_gudang->name}}
                                </td>
                                <td>
                                    {{$dt->data_product->code}}
                                </td>
                                <td>
                                    {{$dt->data_product->name}}
                                </td>
                                <td>
                                    {{number_format($dt->qty)}} {{$dt->data_product->unit}}
                                </td>
                                <td>
                                    {{number_format($dt->hpp)}}
                                </td>
                                <td>
                                    <div class="row">
                                        @can('update stock')
                                        <div class="col-sm-6">
                                            <a href="{{URL::to('/stock/'.$dt->id)}}" class="btn btn-warning">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        </div>
                                        @endcan
                                        @can('delete stock')
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-danger" onclick="beforeDelete('{{URL::to('/stock/destroye/'.$dt->id)}}')">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </div>
                                        @endcan
                                    </div>
                                </td>
        					</tr>
        				@endforeach
        			@endif
        		</tbody>
        	</table>
        </div>

        <div class="card-footer">
        	<div class="float-left">
                <select class="form-control" onchange="searchData()" id="take_data" name="take_data">
                    @foreach(HelperController::$take as $key => $jumlah)
                        <option value="{{$jumlah}}" @if($take == $jumlah) selected="" @endif>{{$jumlah}}</option>
                    @endforeach
                </select>
                <label class="label-control">Of : {{$count}} Data</label>
        	</div>
        	<div class="float-right">
        		{!!$data->appends(['take' => $take, 'filter' => $filter, 'value' => $value])->links()!!}
        	</div>
        </div>

	</div>
@stop
