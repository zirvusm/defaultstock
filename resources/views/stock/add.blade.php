@extends('app')
@extends('modals.product')

@section('module_name')
    <h1 class="m-0 text-dark">Tambah Stok</h1>
@stop


@section('content')

<style type="text/css">
    .my-custom-scrollbar {
        position: relative;
        /*height: 300px;*/
        overflow: auto;
    }
    .table-wrapper-scroll-y {
        display: block;
    }
</style>

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/stock/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="supplier_id" class="col-sm-2 col-form-label">Supplier</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('supplier_id'))?'is-invalid':''}}" name="supplier_id" id="supplier_id">
                            @foreach($data_supplier as $key => $value)
                                <option value="{{$value->id}}">{{$value->name}} ( {{$value->code}} )</option>
                            @endforeach
                        </select>
                        @if($errors->has('supplier_id'))
                            <span id="supplier_id-error" class="error invalid-feedback">{{$errors->first('supplier_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gudang_id" class="col-sm-2 col-form-label">Gudang</label>
                    <div class="col-sm-10">
                        <select class="form-control {{($errors->has('gudang_id'))?'is-invalid':''}}" name="gudang_id" id="gudang_id">
                            @foreach($data_gudang as $key => $value)
                                <option value="{{$value->id}}">{{$value->name}} ( {{$value->code}} )</option>
                            @endforeach
                        </select>
                        @if($errors->has('gudang_id'))
                            <span id="gudang_id-error" class="error invalid-feedback">{{$errors->first('gudang_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="product_id" class="col-sm-2 col-form-label">Product</label>
                    <div class="input-group col-sm-10">
                        <input type="hidden" name="product_id" id="product_id" value="{{old('product_id')}}">
                        <input type="text" class="form-control {{($errors->has('product_id'))?'is-invalid':''}}" name="product_name" id="product_name" readonly="" value="{{old('product_name')}}">

                        <button class="btn btn-default btn-flat" type="button" data-toggle="modal" data-target="#productModal"><i class="fa fa-folder-open"></i></button>

                        @if($errors->has('product_id'))
                            <span id="product_id-error" class="error invalid-feedback">{{$errors->first('product_id')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="qty" class="col-sm-2 col-form-label">Qty</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control {{($errors->has('qty'))?'is-invalid':''}}" name="qty" id="qty" value="{{old('qty')}}" min="0" step="1">
                        @if($errors->has('qty'))
                            <span id="qty-error" class="error invalid-feedback">{{$errors->first('qty')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="hpp" class="col-sm-2 col-form-label">HPP</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control {{($errors->has('hpp'))?'is-invalid':''}}" name="hpp" id="hpp" value="{{old('hpp')}}" min="0">
                        @if($errors->has('hpp'))
                            <span id="hpp-error" class="error invalid-feedback">{{$errors->first('hpp')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" id="description" rows="4">{{old('description')}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/stock')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

@stop
