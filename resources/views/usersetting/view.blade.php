@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">User Setting</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/usersetting/update')}}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control {{($errors->has('email')) ? 'is-invalid' : ''}}" id="email" placeholder="Email" name="email" id="email" readonly="" value="{{Auth::user()->email}}">
                        @if($errors->has('email'))
                            <span id="email-error" class="error invalid-feedback">{{$errors->first('email')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control {{($errors->has('password')) ? 'is-invalid' : ''}}" id="password" placeholder="Let Empty if no Change" name="password" id="password" value="">
                        @if($errors->has('password'))
                            <span id="password-error" class="error invalid-feedback">{{$errors->first('password')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cpassword" class="col-sm-2 col-form-label">Confirm Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control {{($errors->has('cpassword')) ? 'is-invalid' : ''}}" id="cpassword" placeholder="Let Empty if no Change" name="cpassword" id="cpassword" value="">
                        @if($errors->has('cpassword'))
                            <span id="cpassword-error" class="error invalid-feedback">{{$errors->first('cpassword')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="img" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('img')) ? 'is-invalid' : ''}}" id="img" placeholder="Let Empty if no Change" name="img" id="img" value="{{Auth::user()->img}}">
                        @if($errors->has('img'))
                            <span id="img-error" class="error invalid-feedback">{{$errors->first('cpassword')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    @can('update usersetting')
                        <button type="submit" class="btn btn-primary">Save</button>
                    @endcan
                </div>
            </div>
        </form>
    </div>
</div>

@stop