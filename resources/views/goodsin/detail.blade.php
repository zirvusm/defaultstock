@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Detail Terima Barang</h1>
@stop
@php
    $total = 0;
    $count = 0;
    $total_cost = 0;
    $count_cost = 0;
    $grand_total = 0;
@endphp



@section('content')

<div class="col-md-12">
    <div class="card">
        <input type="hidden" name="id" id="id" value="{{$data->id}}">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Gudang</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$data->data_transaction->data_gudang->name ?? "-"}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nomor Faktur</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$data->data_transaction->invoice}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Supplier</label>
                <div class="input-group col-sm-10">
                    <input type="text" class="form-control" readonly="" value="{{$data->data_transaction->data_supplier->name ?? "-"}}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Tanggal</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" value="{{$data->date}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label for="status" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    <label_v1 class="form-control">
                        @if($data->status == 0) 
                            <b><font color="warning">Menunggu</font></b>
                        @elseif($data->status == 1) 
                            Di <b><font color="success">Setujui</font></b>
                        @elseif($data->status == 2) 
                            Di <b><font color="danger">Batalkan</font></b>
                        @endif 
                    </label_v1>
                    @if($errors->has('status'))
                        <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
            <table class="table table-bordered table-hover">
                <thead>
                    <th>Kode Produk</th>
                    <th>Nama Produk</th>
                    <th>Qty</th>
                </thead>
                <tbody id="tbl_products">
                    @foreach($data->data_detail as $key => $detail)
                        <tr id="tr_add_product_{{$key}}">
                            <td>
                                {{$detail->data_product->code}}
                            </td>
                            <td>
                                <input type="hidden" name="ids[]" value="{{$detail->data_product->id}}">
                                {{$detail->data_product->name}}
                            </td>
                            <td>
                                <input type="text" class="form-control" id="qty_{{$key}}" name="qtys[]" value="{{number_format($detail->qty)}}" readonly="">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Keterangan</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="4" readonly="">{{$data->description}}</textarea>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="float-right">
                <a href="{{URL::to('/goodsin')}}" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop