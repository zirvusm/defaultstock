@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Ubah Terima Barang</h1>
@stop
@php
    $total = 0;
    $count = 0;
    $total_cost = 0;
    $count_cost = 0;
    $grand_total = 0;
@endphp


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/goodsin/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nomor Faktur</label>
                    <div class="input-group col-sm-10">
                        <input type="text" class="form-control" readonly="" value="{{$data->data_transaction->invoice ?? "-"}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" value="{{$data->date}}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select class="form-control {{$errors->has('status') ? 'is-invalid' : ''}}" name="status" id="status">
                            <option value="0" @if($data->status == 0) selected @endif>Menunggu</option>
                            <option value="1" @if($data->status == 1) selected @endif>Di Sejutui</option>
                            <option value="2" @if($data->status == 2) selected @endif>Di Tolak</option>
                        </select>
                        @if($errors->has('status'))
                            <span id="status-error" class="error invalid-feedback">{{$errors->first('status')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="4" id="description" name="description">{{$data->description}}</textarea>
                    </div>
                </div>
            </div>

            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Qty</th>
                    </thead>
                    <tbody>
                        @foreach($data->data_detail as $key => $detail)
                            <tr>
                                <td>{{$detail->data_product->code ?? "-"}}</td>
                                <td>{{$detail->data_product->name ?? "-"}}</td>
                                <td><input type="text" class="form-control" value="{{$detail->qty}}" readonly></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/goodsin')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop