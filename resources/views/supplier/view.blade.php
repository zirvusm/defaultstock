@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Supplier</h1>
@stop


@section('content')

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    
	<div class="card">

        <div class="card-header border-0">
            <h3 class="card-title">Supplier</h3>
            <div class="card-tools">
                @can('add supplier')
                    <a href="{{URL::to('/supplier/add')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-plus"></i></a>
                @endcan
                <a href="{{URL::to('/supplier')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-2">
                    <select class="form-control" name="filter" id="filter">
                        <option value="">--Pilih Filter--</option>
                        <option value="code" @if($filter == 'code') selected="" @endif>Kode</option>
                        <option value="name" @if($filter == 'name') selected="" @endif>Nama</option>
                        <option value="phone_number" @if($filter == 'phone_number') selected="" @endif>Nomor Telpon</option>
                        <option value="email" @if($filter == 'email') selected="" @endif>Email</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="value" placeholder="Cari . . ." name="value" id="value" value="{{$value}}">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-default" onclick="searchData()"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>
        					#
        				</th>
                        <th>
                            Kode
                        </th>
                        <th>
                            Nama
                        </th>
                        <th>
                            Nomor Telpon
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Alamat
                        </th>
                        <th>
                            Status
                        </th>
                        <th width="120px">
                            Action
                        </th>
        			</tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
        					<tr>
        						<td>
        							{{($key+1) + (($page - 1) * $take)}}
        						</td>
                                <td>
                                    {{$dt->code}}
                                </td>
                                <td>
                                    {{$dt->name}}
                                </td>
                                <td>
                                    {{$dt->phone_number ?? "-"}}
                                </td>
                                <td>
                                    {{$dt->email ?? "-"}}
                                </td>
                                <td>
                                    {{$dt->address}}
                                </td>
                                <td>
                                    @if($dt->status == 1)
                                        <font color="green">Aktif</font>
                                    @else
                                        <font color="red">Tidak Aktif</font>
                                    @endif
                                </td>
                                <td>
                                    <div class="row">
                                        @can('update supplier')
                                        <div class="col-sm-6">
                                            <a href="{{URL::to('/supplier/'.$dt->id)}}" class="btn btn-warning">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        </div>
                                        @endcan
                                        @can('delete supplier')
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-danger" onclick="beforeDelete('{{URL::to('/supplier/destroye/'.$dt->id)}}')">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </div>
                                        @endcan
                                    </div>
                                </td>
        					</tr>
        				@endforeach
        			@endif
        		</tbody>
        	</table>
        </div>

        <div class="card-footer">
        	<div class="float-left">
                <select class="form-control" onchange="searchData()" id="take_data" name="take_data">
                    @foreach(HelperController::$take as $key => $jumlah)
                        <option value="{{$jumlah}}" @if($take == $jumlah) selected="" @endif>{{$jumlah}}</option>
                    @endforeach
                </select>
                <label class="label-control">Of : {{$count}} Data</label>
        	</div>
        	<div class="float-right">
        		{!!$data->appends(['take' => $take, 'filter' => $filter, 'value' => $value])->links()!!}
        	</div>
        </div>

	</div>
@stop