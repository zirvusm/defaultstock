@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Authorization</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/authorization/update')}}" method="post">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{($errors->has('name'))?'is-invalid':''}}" id="name" name="name" id="name" value="{{$data->name}}" readonly="">
                        @if($errors->has('name'))
                            <span id="name-error" class="error invalid-feedback">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Menu</th>
                            @foreach($menu_action as $key => $action)
                                <th>{{$action}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($menus as $index => $menu)
                            <tr>
                                <td>{{$menu->name}}</td>
                                @foreach($menu_action as $key => $action)
                                    <td>
                                        <input 
                                            type="checkbox" 
                                            name="{{$key}} {{$menu->route}}"
                                            @php
                                                try {
                                                    if($data->hasPermissionTo($key.' '.$menu->route))
                                                        echo 'checked=""';
                                                } catch (Exception $e) {
                                                    
                                                }
                                            @endphp
                                        >
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/authorization')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop