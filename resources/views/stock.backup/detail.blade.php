@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Detail Stock</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <input type="hidden" name="id" id="id" value="{{$data->id}}">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Supplier</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$data->data_supplier->name ?? "-"}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Gudang</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$data->data_gudang->name ?? "-"}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kode Produk</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$data->data_product->code ?? "-"}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Produk</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$data->data_product->name ?? "-"}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Qty</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{number_format($data->qty)}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Modal Rata - Rata</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{number_format($modal)}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Harga Jual</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{number_format($data->selling_price)}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Keterangan</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="4" readonly="">{{$data->description}}</textarea>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="float-right">
                <a href="{{URL::to('/stock')}}" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop