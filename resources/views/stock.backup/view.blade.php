@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Stock</h1>
@stop


@section('content')

    <input type="hidden" name="filter_arr" id="filter_arr" value="{{json_encode($filter_arr)}}">

    <style type="text/css">
        .my-custom-scrollbar {
            position: relative;
            /*height: 300px;*/
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    
	<div class="card">

        <div class="card-header">
            <div class="card-tools">
                <a href="{{URL::to('/stock')}}" class="btn btn-tool" data-widget="collapse"><i class="fas fa-sync"></i></a>
            </div>
        </div>

        <div class="card-body">
            <div class="float-left input-group col-sm-3">
                <select class="form-control" onchange="filterData()" id="take_data" name="take_data">
                    @foreach(HelperController::$take as $key => $jumlah)
                        <option value="{{$jumlah}}" @if($take == $jumlah) selected="" @endif>{{$jumlah}}</option>
                    @endforeach
                    <option value="All" @if($take == "All") selected="" @endif>Semua Data</option>
                </select>
                <label class="label-control"> &nbsp; Of : {{$count}} Data</label>
            </div>
            <div class="float-right">
                @if($take != "All")
                  {!!$data->appends($query)->links()!!}
                @endif
            </div>
        </div>

        <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th rowspan="2">
        					#
        				</th>
                        <th>
                            <input type="text" class="form-control" id="data_supplier-name" placeholder="Cari . . ." value="{{$filter_arr['data_supplier.name'] ?? ""}}">
                        </th>
                        <th>
                            <input type="text" class="form-control" id="data_gudang-name" placeholder="Cari . . ." value="{{$filter_arr['data_gudang.name'] ?? ""}}">
                        </th>
                        <th>
                            <input type="text" class="form-control" id="data_product-code" placeholder="Cari . . ." value="{{$filter_arr['data_product.code'] ?? ""}}">
                        </th>
                        <th>
                            <input type="text" class="form-control" id="data_product-name" placeholder="Cari . . ." value="{{$filter_arr['data_product.name'] ?? ""}}">
                        </th>
                        <th colspan="4">
                            <button class="btn btn-default btn-flat col-sm-12" type="button" onclick="filterData()">
                                Filter <i class="fas fa-search"></i>
                            </button>
                        </th>
        			</tr>
                    <tr>
                        <th>
                            Supplier
                        </th>
                        <th>
                            Gudang
                        </th>
                        <th>
                            Kode Produk
                        </th>
                        <th>
                            Produk
                        </th>
                        <th>
                            Qty
                        </th>
                        <th>
                            Modal Rata - Rata
                        </th>
                        <th>
                            Harga Jual
                        </th>
                        <th width="120px">
                            Action
                        </th>
                    </tr>
        		</thead>
        		<tbody>
        			@if($data->count() == 0)
        				<tr>
        					<td colspan="100" align="center">No Data</td>
        				</tr>
        			@else
        				@foreach($data as $key => $dt)
        					<tr>
        						<td>
                                    @if($take != "All")
                                        {{($key+1) + (($page - 1) * $take)}}
                                    @else
                                        {{($key+1)}}
                                    @endif
        						</td>
                                <td>
                                    @if($dt->data_supplier != null)
                                        {{$dt->data_supplier->name}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if($dt->data_gudang != null)
                                        {{$dt->data_gudang->name}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if($dt->data_product != null)
                                        {{$dt->data_product->code}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if($dt->data_product != null)
                                        {{$dt->data_product->name}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    {{number_format($dt->qty)}}
                                </td>
                                <td>
                                    {{number_format($dt->modal)}}
                                </td>
                                <td>
                                    {{number_format($dt->selling_price)}}
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="{{URL::to('/stock/detail/'.$dt->id)}}" class="btn btn-primary">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </div>
                                        @can('update stock')
                                        <div class="col-sm-4">
                                            <a href="{{URL::to('/stock/'.$dt->id)}}" class="btn btn-warning">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        </div>
                                        @endcan
                                    </div>
                                </td>
        					</tr>
        				@endforeach
        			@endif
        		</tbody>
        	</table>
        </div>

	</div>
@stop