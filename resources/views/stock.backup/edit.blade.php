@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Ubah Stok</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="{{URL::to('/stock/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data->id}}">
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Supplier</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{$data->data_supplier->name}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Gudang</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{$data->data_gudang->name}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kode Produk</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{$data->data_product->code}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama Produk</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{$data->data_product->name}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Qty</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{number_format($data->qty)}}" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="selling_price" class="col-sm-2 col-form-label">Harga Jual</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control {{($errors->has('selling_price'))?'is-invalid':''}}" name="selling_price" id="selling_price" value="{{$data->selling_price}}" min="0">
                        @if($errors->has('selling_price'))
                            <span id="selling_price-error" class="error invalid-feedback">{{$errors->first('selling_price')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" id="description" rows="4">{{$data->description}}</textarea>
                        @if($errors->has('description'))
                            <span id="description-error" class="error invalid-feedback">{{$errors->first('description')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="{{URL::to('/stock')}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('script')

    <script type="text/javascript">
        // $('#image').change(function(){
        //     $('#textImage').html(this.value);
        //     readURL(this);
        // });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop