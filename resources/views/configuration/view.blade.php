@extends('app')


@section('module_name')
    <h1 class="m-0 text-dark">Configuration</h1>
@stop


@section('content')

<div class="col-md-12">
    <div class="card">
        <form class="form-horizontal" action="/configuration/update" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label for="pin" class="col-sm-2 col-form-label">Change Pin</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control {{($errors->has('pin')) ? 'is-invalid' : ''}}" id="pin" placeholder="Empty If no Change" name="pin" id="pin" value="">
                        @if($errors->has('pin'))
                            <span id="pin-error" class="error invalid-feedback">{{$errors->first('pin')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cpin" class="col-sm-2 col-form-label">Confirmation Pin</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control {{($errors->has('cpin')) ? 'is-invalid' : ''}}" id="cpin" placeholder="Empty If no Change" name="cpin" id="cpin" value="">
                        @if($errors->has('cpin'))
                            <span id="cpin-error" class="error invalid-feedback">{{$errors->first('cpin')}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    @can('update configuration')
                        <button type="submit" class="btn btn-primary">Save</button>
                    @endcan
                </div>
            </div>
        </form>
    </div>
</div>

@stop